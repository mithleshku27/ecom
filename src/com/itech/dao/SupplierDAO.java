

package com.itech.dao;

import java.util.List;

import com.itech.model.Supplier;

public interface SupplierDAO {

	public Supplier saveSupplierDetails(Supplier dto);
	public List<Supplier> findSupplierDetails();
	
}
