package com.itech.dao;

import java.util.*;
//import javax.mail.Session;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import com.itech.model.ClientSignUp;


public class ClientSignUpDAOImpl implements  ClientSignUpDAO {
	private SessionFactory sessionFactory;
	 
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<ClientSignUp> findClientSignUp(ClientSignUp dto) {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ClientSignUp saveClientSignUp(ClientSignUp dto) {
		 	
		Session session=sessionFactory.openSession();
		try {
			Transaction tx=session.beginTransaction();
			dto.setRegistration_date(new Date());
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId=DetachedCriteria.forClass(ClientSignUp.class).setProjection(Projections.max("id"));
			List<ClientSignUp> udList=session.createCriteria(ClientSignUp.class).add(Property.forName("id").eq(maxId)).list();
		    session.clear();
			session.close();
			return udList.get(0);
		} catch (Exception er) {
		} 
		
		return null;
	}

}
