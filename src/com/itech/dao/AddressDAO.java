package com.itech.dao;

import java.util.List;

import com.itech.model.Address;

public interface AddressDAO {
	public Address saveAuthorDetails(Address dto);

	public List<Address> findAddressDetails(Address dto);


}
