package com.itech.dao;


import java.util.List;

import com.itech.model.Product;


public interface ProductDAO
{
	public Product saveProduct(Product dto);	
	public List<Product> productList();
	public List<Product> productList(int proudctId);
	
}
