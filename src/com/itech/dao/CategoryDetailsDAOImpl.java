package com.itech.dao;


import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.CategoryDetails; 


public class CategoryDetailsDAOImpl implements CategoryDetailsDAO
{
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public CategoryDetails saveCategoryDetails(CategoryDetails dto)
	{
		try
		{
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			dto.setCreatedDate(new Date());
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(CategoryDetails.class).setProjection(Projections.max("ctId"));
			List<CategoryDetails> cidList = session.createCriteria(CategoryDetails.class).add(Property.forName("ctId").eq(maxId)).list();
			session.clear();
			session.close();
			return cidList.get(0);
		}
		catch (Exception er)
		{
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CategoryDetails> getCategoryDetailsList()
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<CategoryDetails> list = session.createQuery("from CategoryDetails").list();
			if (list != null && list.size() > 0)
			{
				return list;
			}
		}
		catch (Exception ER)
		{
		}
		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
}
