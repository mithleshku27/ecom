package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.itech.model.City;
import com.itech.model.State;


public class CityDAOImpl implements CityDAO
{
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<City> getCity(int cityId)
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<City> cityList = session.createQuery("From City c where c.cityId=" + cityId).list();
			session.close();
			if (cityList != null && cityList.size() > 0)
			{
				return cityList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<City> getCityByState(int stateId)
	{try
	{
		Session session = sessionFactory.openSession();
		List<City> cityList = session.createQuery("From City c where c.stateId=" + stateId).list();
		session.close();
		if (cityList != null && cityList.size() > 0)
		{
			return cityList;
		}
	}
	catch (Exception er)
	{
		er.printStackTrace();
	}
		return null;
	}
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<State> getStateId(String stateName)
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<State> cityList = session.createQuery("From State c where c.stateName=" + stateName).list();
			session.close();
			if (cityList != null && cityList.size() > 0)
			{
				return cityList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<State> getStateName(String stateName)
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<State> cityList = session.createQuery("From State c where c.stateName='" + stateName + "'").list();
			session.close();
			if (cityList != null && cityList.size() > 0)
			{
				return cityList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}
}
