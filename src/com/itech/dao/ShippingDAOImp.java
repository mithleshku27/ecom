package com.itech.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.Shipping;




public class ShippingDAOImp implements ShippingDAO
{
	
private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Shipping saveShippingDetails(Shipping dto)
	{
		try
		{
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(Shipping.class).setProjection(Projections.max("id"));
			List<Shipping> scdList = session.createCriteria(Shipping.class).add(Property.forName("id").eq(maxId)).list();
			session.clear();
			session.close();			
			return scdList.get(0);
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Shipping> findShippingDetails()
	{
		try
		{
			Session session = sessionFactory.openSession();
			//List<SubCategoryDetails> list = session.createQuery("From SubCategoryDetails").list();
			List<Shipping> list = session.createQuery("From tbl_shipping_details").list();
			if (list != null && list.size() > 0)
			{
				return list;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
}
	