package com.itech.dao;

import java.util.List;
import com.itech.model.Cart;

public interface CartDAO {
	
	public Cart saveCartDetails(Cart dto);
	
	public List<Cart> findCartDetails(Cart dto);
	
	public String findUpdateCartDetails(Cart dto);
	
	public String DeleteCartDetails(Cart dto);

}
