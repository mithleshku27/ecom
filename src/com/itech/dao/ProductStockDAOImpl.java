package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.ProductStock;


public class ProductStockDAOImpl implements ProductStockDAO
{
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	 
	@SuppressWarnings("unchecked")
	@Override
	public ProductStock saveProductStock(ProductStock dto)
	{
		try
		{
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxid = DetachedCriteria.forClass(ProductStock.class).setProjection(Projections.max("psId"));
			List<ProductStock> psList = session.createCriteria(ProductStock.class).add(Property.forName("psId").eq(maxid)).list();
			session.close();
			if (psList != null && psList.size() > 0)
			{
				return psList.get(0);
				
			}
		}
		catch (Exception er)
		{
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductStock> getProductStockList()
	{
		try{
			Session session=sessionFactory.openSession();
			List<ProductStock> psList=session.createQuery("FROM ProductStock").list();
			session.close();
			if(psList!=null && psList.size()>0){
				return psList;
			}
			
		}catch(Exception er){er.printStackTrace();}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductStock> getProductStockByPSId(int pstockId)
	{
		try{
			Session session=sessionFactory.openSession();
			List<ProductStock> psList=session.createQuery("FROM ProductStock p where p.productId="+pstockId).list();
			session.close();
			if(psList!=null && psList.size()>0){
				return psList;
			}
			
		}catch(Exception er){er.printStackTrace();}
		return null;
	}
}
