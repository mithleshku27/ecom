package com.itech.dao;


import java.util.List;

import com.itech.model.CategoryDetails;


public interface CategoryDetailsDAO
{
	public CategoryDetails saveCategoryDetails(CategoryDetails dto);
	
	public List<CategoryDetails> getCategoryDetailsList();
	
}
