package com.itech.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.Author;

public class AuthorDaoImpl implements AuthorDAO {
	
	private SessionFactory sessionFactory;

	

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	



	@SuppressWarnings("unchecked")
	@Override
	public Author saveAuthorDetails(Author dto) {
		Session session=sessionFactory.openSession();
		try {
			Transaction tx=session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId=DetachedCriteria.forClass(Author.class).setProjection(Projections.max("id"));
			List<Author> udList=session.createCriteria(Author.class).add(Property.forName("id").eq(maxId)).list();
		    session.clear();
			session.close();
			return udList.get(0);
		} catch (Exception er) {
		} 
		return null;
	}

	@Override
	public List<Author> findAuthorDetails(Author dto) {
	 
		return null;
	}

}
