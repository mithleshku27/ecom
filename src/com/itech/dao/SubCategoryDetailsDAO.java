package com.itech.dao;


import java.util.List;

import com.itech.model.SubCategoryDetails;


public interface SubCategoryDetailsDAO
{
	public SubCategoryDetails saveSubCategory(SubCategoryDetails dto);
	
	public List<SubCategoryDetails> getSubCategoryDetailsList();
}
