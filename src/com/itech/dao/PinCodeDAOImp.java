

package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.PinCodeModel;;


public class PinCodeDAOImp implements PinCodeDAO
{
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public PinCodeModel savePinDetails(PinCodeModel dto)
	{
		try
		{
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(PinCodeModel.class).setProjection(Projections.max("pid"));
			List<PinCodeModel> scdList = session.createCriteria(PinCodeModel.class).add(Property.forName("pid").eq(maxId)).list();
			session.clear();
			session.close();			
			return scdList.get(0);
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PinCodeModel> getPinDetailsList()
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<PinCodeModel> list = session.createQuery("From tbl_pin_code_details").list();
			if (list != null && list.size() > 0)
			{
				return list;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
}
