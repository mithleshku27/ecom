package com.itech.dao;

import java.util.List;

import com.itech.model.ClientSignUp;

public interface ClientSignUpDAO {
	
	public ClientSignUp saveClientSignUp(ClientSignUp dto);

	public List<ClientSignUp> findClientSignUp(ClientSignUp dto);

}
