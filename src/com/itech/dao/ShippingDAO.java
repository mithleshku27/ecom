
package com.itech.dao;

import java.util.List;

import com.itech.model.Shipping;;

public interface ShippingDAO {

	public Shipping saveShippingDetails(Shipping dto);
	public List<Shipping> findShippingDetails();
	
}
