package com.itech.dao;

import java.util.List;

import com.itech.model.ProductStock;

public interface ProductStockDAO 

{
	public ProductStock saveProductStock(ProductStock dto );	
	public List<ProductStock> getProductStockList();
	public List<ProductStock> getProductStockByPSId(int pstockId);
	
}
