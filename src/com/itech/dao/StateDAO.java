package com.itech.dao;

import java.util.List;

import com.itech.model.Country;
import com.itech.model.State;

public interface StateDAO
{
	public List<State> getState(int stateId);
	public List<State> getStateByCountryId(int countryId);
	public List<Country> getCountry(String countryName);
}
