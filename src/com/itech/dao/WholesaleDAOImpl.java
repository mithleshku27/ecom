package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.WholeSaleProductDetails;
import com.itech.model.WholesaleProduct;


public class WholesaleDAOImpl implements WholesaleDAO
{
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public WholesaleProduct saveWholesale(WholesaleProduct dto)
	{
		try
		{
			List<WholeSaleProductDetails> slist = dto.getWholeSaleProductDetails();
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			dto.setWholeSaleProductDetails(null);
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(WholesaleProduct.class).setProjection(Projections.max("hid"));
			List<WholesaleProduct> list = session.createCriteria(WholesaleProduct.class).add(Property.forName("hid").eq(maxId)).list();
			session.close();
		if (list != null && list.size() > 0)
			{
				int wholeSaleProductDetails = list.get(0).getHid();
				for (WholeSaleProductDetails dts : slist)
				{
					dts.setWholeSaleProductDetails(wholeSaleProductDetails);
					Session sessions = sessionFactory.openSession();
					Transaction txs = sessions.beginTransaction();
					sessions.save(dts);
					txs.commit();
				//	DetachedCriteria maxIds = DetachedCriteria.forClass(WholeSaleProductDetails.class).setProjection(Projections.max("wholesaleId"));
				//	List<WholeSaleProductDetails> lists = sessions.createCriteria(WholeSaleProductDetails.class).add(Property.forName("wholesaleId").eq(maxIds)).list();
					sessions.close();
				}
				
			}
			return list.get(0);
			
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WholesaleProduct> getWholesale(WholesaleProduct dto)
	{
		Session session = sessionFactory.openSession();
		List<WholesaleProduct> pdList = session.createQuery("From WholesaleProduct wp where wp.email='" + dto.getEmail() + "' OR wp.mobile='" + dto.getMobile() + "' OR wp.orderDate='" + dto.getOrderDate() + "' OR names='" + dto.getNames() + "'").list();
		session.clear();
		session.flush();
		session.close();
		
		if (pdList != null && pdList.size() > 0)
		{
			return pdList;
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WholesaleProduct> getWholesaleList()
	{
		Session session = sessionFactory.openSession();
		List<WholesaleProduct> pdList = session.createQuery("From WholesaleProduct").list();
		session.clear();
		session.flush();
		session.close();
		
		if (pdList != null && pdList.size() > 0)
		{
			return pdList;
		}
		
		return null;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
}
