package com.itech.dao;

import java.util.List;

import com.itech.model.ProductReview;

public interface ProductReviewDAO
{
	public ProductReview saveProductReviewDetails(ProductReview dto);

	public List<ProductReview> findProductReviewDetails(ProductReview dto);
}
