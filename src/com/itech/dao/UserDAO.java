package com.itech.dao;

import java.util.List;

import com.itech.model.ClientSignUp;
import com.itech.model.UserDetails;

public interface UserDAO {

	public UserDetails saveUserDetails(UserDetails dto);
	public List<UserDetails> findUserDetails(UserDetails dto);
	public List<ClientSignUp> findAdminUserDetails(ClientSignUp dto);
	
}
