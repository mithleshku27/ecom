package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.itech.model.Country;


public class CountryDAOImpl implements CountryDAO
{
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Country> getContry()
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<Country> countryList = session.createQuery("From Country").list();
			if (countryList != null && countryList.size() > 0)
			{
				return countryList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
}
