package com.itech.dao;

import java.util.List;

import com.itech.model.City;
import com.itech.model.State;

public interface CityDAO
{
	public List<City> getCity(int cityId);
	public List<City> getCityByState(int stateId);
	
	public List<State> getStateId(String stateName);
	
	public List<State> getStateName(String stateName);
}
