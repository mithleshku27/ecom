package com.itech.dao;

import java.util.List;

import com.itech.model.WholesaleProduct;

public interface WholesaleDAO
{
	public WholesaleProduct saveWholesale(WholesaleProduct dto);
	
	public List<WholesaleProduct> getWholesale(WholesaleProduct dto);
	
	public List<WholesaleProduct> getWholesaleList();
}
