package com.itech.dao;


import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.Cart;


public class CartDAOImpl implements CartDAO
{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	public Cart saveCartDetails(Cart dto)
	{
		Session session = sessionFactory.openSession();
		try
		{
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(Cart.class).setProjection(Projections.max("id"));
			List<Cart> udList = session.createCriteria(Cart.class).add(Property.forName("id").eq(maxId)).list();
			session.clear();
			session.close();
			return udList.get(0);
		}
		catch (Exception er)
		{
		}
		return null;
	}
	
	
	@Override
	@SuppressWarnings({"unchecked", "deprecation"})
	public List<Cart> findCartDetails(Cart dto)
	{
		Session session = sessionFactory.openSession();
		/*
		 * Session session =sessionFactory.openSession();
		 * session.beginTransaction();
		 * String select = "Select e FROM Cart e,ProductDetails t ON e.productID=t.prid  WHERE e.userID='" + dto.getUserID() +"'";
		 * Query query = session.createQuery(select);
		 * List<Cart> cartList = query.list();
		 * session.getTransaction().commit();
		 * session.close();
		 */
		
		// String sql="select u.*,p.productImage from tbl_user_cart u inner join tbl_productdetails p on u.productid=p.prid  where u.userid='"
		// +dto.getUserID() + "'";
		String sql = "select u.id, u.userid, u.productid, u.productname,  u.productPrice, u.quantity, u.ecocost, u.shippingcost,  u.totalamount, u.cartdate, u.billdate, u.paymentdate,  u.paidstatus, u.shippingid, p.productImage  from tbl_user_cart u inner join tbl_productdetails p on u.productid=p.prid  where u.userid='" + dto.getUserID() + "'";
		
		System.out.println(sql);
		List<Object[]> objList = session.createSQLQuery(sql).list();
		session.clear();
		session.flush();
		session.close();
		List<Cart> cartList = new ArrayList<Cart>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date;// = formatter.parse(dateInString);
		if (objList != null && objList.size() > 0)
		{
			for (Object[] obj : objList)
			{
				Cart dtos = new Cart();
				if (obj[0].toString() != null && !obj[0].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setId(Integer.parseInt(obj[0].toString()));
				}
				if (obj[1].toString() != null && !obj[1].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setUserID(Integer.parseInt(obj[1].toString()));
				}
				if (obj[2].toString() != null && !obj[2].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setProductID(Integer.parseInt(obj[2].toString()));
				}
				if (obj[3].toString() != null && !obj[3].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setProductName(obj[3].toString());
				}
				// 5000
				if (obj[4].toString() != null && !obj[4].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setProductPrice(Double.parseDouble(obj[4].toString()));
				}
				if (obj[5].toString() != null && !obj[5].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setQuantity(Double.parseDouble(obj[5].toString()));
				}
				if (obj[6].toString() != null && !obj[6].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setEcocost(Double.parseDouble(obj[6].toString()));
				}
				if (obj[7].toString() != null && !obj[7].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setShippingcost(Double.parseDouble(obj[7].toString()));
				}
				if (obj[8].toString() != null && !obj[8].toString().trim().equalsIgnoreCase(""))
				{
					dtos.setTotalamount(Double.parseDouble(obj[8].toString()));
				}
				byte[] imagefiles = serialize(obj[14]);
				dtos.setImageFile(imagefiles);
				
				try
				{
					try
					{
						if (obj[10] != null)
						{
							String dates = obj[9].toString().replace(".0", "");
							dtos.setCartDates(dates);
						}
						else
						{
							dtos.setCartDates("");
						}
					}
					catch (Exception er)
					{
						er.printStackTrace();
					}
					try
					{
						if (obj[10] != null)
						{
							dtos.setBillDates((new SimpleDateFormat("dd-MM-yyyy").format(obj[10])).toString());
							// dtos.setBillDates(obj[10].toString().replace(".0", ""));
						}
						else
						{
							dtos.setBillDates("");
						}
					}
					catch (Exception er)
					{
						er.printStackTrace();
					}
					try
					{
						if (obj[11] != null)
						{
							dtos.setPaymentDates(obj[11].toString().replace(".0", ""));
						}
						else
						{
							dtos.setPaymentDates("");
						}
					}
					catch (Exception er)
					{
						er.printStackTrace();
					}
					try
					{
						if (obj[12] != null && !obj[12].toString().equalsIgnoreCase("0") || !obj[12].toString().equalsIgnoreCase(""))
						{
							dtos.setPaidstatus(Integer.parseInt(obj[12].toString()));
						}
					}
					catch (Exception er)
					{
						er.printStackTrace();
					}
					try
					{
						if (obj[13] != null )
						{
							if(obj[13].toString().equalsIgnoreCase("0") || !obj[13].toString().equalsIgnoreCase("")){
							dtos.setShippingid(Integer.parseInt(obj[13].toString()));
							}
						}
					}
					catch (Exception er)
					{
						er.printStackTrace();
					}
				}
				catch (Exception e)
				{
					
					e.printStackTrace();
				}
				
				// dtos.setImageFile(obj[13].toString());
				cartList.add(dtos);
			}
			return cartList;
		}
		return cartList;
	}
	
	public static byte[] serialize(Object obj)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		try
		{
			
			ObjectOutputStream os = new ObjectOutputStream(out);
			os.writeObject(obj);
			return out.toByteArray();
		}
		catch (Exception er)
		{
			return out.toByteArray();
		}
		
	}
	
	
	@Override
	// @SuppressWarnings({"unchecked", "deprecation"})
	public String findUpdateCartDetails(Cart dto)
	{
		Session session = sessionFactory.openSession();
		try
		{
			Cart dto1 = null;
			Object obj = session.get(Cart.class, dto.getId());
			dto1 = (Cart)obj;
			session.close();
			dto1.setQuantity(dto.getQuantity());
			
			Session session1 = this.sessionFactory.openSession();
			session1.get(Cart.class, dto.getId());
			Transaction tx = session1.beginTransaction();
			session1.merge(dto1);
			// session1.delete(dto1);
			tx.commit();
			session1.clear();
			session1.close();
			return dto1.getProductName();
		}
		catch (Exception er)
		{
			er.printStackTrace();
			session.close();
			return "failed";
		}
		
		
	}
	
	@Override
	// @SuppressWarnings({"unchecked", "deprecation"})
	public String DeleteCartDetails(Cart dto)
	{
		Session session = sessionFactory.openSession();
		try
		{
			Session session1 = this.sessionFactory.openSession();
			Transaction tx = session1.beginTransaction();
			Query query = session.createQuery("delete Cart c where c.id = :ID");
			query.setParameter("ID", new Integer(dto.getId()));
			query.executeUpdate();
			tx.commit();
			session1.clear();
			session1.close();
			return null;
		}
		catch (Exception er)
		{
			er.printStackTrace();
			session.close();
			return "failed";
		}
		
	}
}
