package com.itech.dao;


import java.util.List;

import com.itech.model.ProductDetails;


public interface ProductDetailsDAO
{
	public ProductDetails saveProductDetails(ProductDetails dto);
	
	public List<ProductDetails> getProductDetails();
	
	public List<ProductDetails> getProductDetailsByProductCode(String productCode);
	

	public List<ProductDetails> getProductDetailsByCategory(String category);

	public List<ProductDetails> getItemDetailsWithProductID(String productID);
}
