package com.itech.dao;

import java.util.List;

import com.itech.model.Cart;
import com.itech.model.CartShipping;

public interface CartShippingDAO
{
	public CartShipping saveCartShippingDetails(CartShipping dto, Cart cartdto);
	public List<CartShipping> findCartShippingDetails(CartShipping dto);
}
