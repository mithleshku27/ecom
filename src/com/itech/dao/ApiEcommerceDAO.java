package com.itech.dao;

import java.util.List;

import com.itech.model.Ecommerce;;

public interface ApiEcommerceDAO {
	public Ecommerce saveApiEcommerceDetails(Ecommerce dto);

	public List<Ecommerce> findApiEcommerceDetails(Ecommerce dto);


}
