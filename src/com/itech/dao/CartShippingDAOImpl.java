package com.itech.dao;


import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.Cart;
import com.itech.model.CartShipping;


public class CartShippingDAOImpl implements CartShippingDAO
{
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	
	@SuppressWarnings("unchecked")
	public CartShipping saveCartShippingDetails(CartShipping dto, Cart cartdto)
	{
		Session session = sessionFactory.openSession();
		try
		{
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(CartShipping.class).setProjection(Projections.max("id"));
			List<CartShipping> udList = session.createCriteria(CartShipping.class).add(Property.forName("id").eq(maxId)).list();
			Integer shippingID = 0;
			session.clear();
			session.close();
			if (udList != null && udList.size() > 0)
			{
				shippingID = udList.get(0).getId();
			}
			UpdateCartDetails(cartdto, shippingID);
			
			return udList.get(0);
		}
		catch (Exception er)
		{
		}
		return null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CartShipping> findCartShippingDetails(CartShipping dto)
	{
		Session session2 = sessionFactory.openSession();
		List<CartShipping> cList = session2.createQuery("From CartShipping c where c.userid=" + dto.getUserid() + "").list();
		
		return cList;
	}
	
	@SuppressWarnings("unchecked")
	public void UpdateCartDetails(Cart dtoCart, Integer shippingID)
	{
		Session session2 = sessionFactory.openSession();
		try
		{
			List<Cart> cList = session2.createQuery("From Cart c where c.userID=" + dtoCart.getUserID() + "").list();
			
			session2.close();
			if (cList != null && cList.size() > 0)
			{
				for (Cart c : cList)
				{
					c.setShippingid(shippingID);
					c.setBillDate(new Date());
					Session session1 = this.sessionFactory.openSession();
					session1.get(Cart.class, dtoCart.getUserID());
					Transaction tx2 = session1.beginTransaction();
					session1.merge(c);
					tx2.commit();
					session1.clear();
					session1.close();
				}
			}
			
		}
		catch (Exception er)
		{
			er.printStackTrace();
			session2.close();
			
		}
		
	}
	
}
