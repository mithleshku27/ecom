package com.itech.dao;

import java.util.List;

import com.itech.model.Author;

public interface AuthorDAO {

	public Author saveAuthorDetails(Author dto);

	public List<Author> findAuthorDetails(Author dto);

}
