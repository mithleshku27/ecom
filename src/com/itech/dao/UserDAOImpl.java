package com.itech.dao;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.Address;
import com.itech.model.ClientSignUp;
import com.itech.model.UserDetails;


public class UserDAOImpl implements UserDAO
{
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings({"unchecked", "unused"})
	public static void main(String[] args)
	{
		UserDAOImpl impl = new UserDAOImpl();
		SessionFactory sessionFactory = impl.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query1 = session.getNamedQuery("findStockByStockCode").setString("stockCode", "7277");
		// HQL Named Query Example HQL_GET_ALL_ADDRESS
		Query query = session.getNamedQuery("HQL_GET_ALL_ADDRESS");
		List<UserDetails> empList = query.list();
		for (UserDetails emp : empList)
		{
			// System.out.println("List of userDetails::" + emp.getId() + "," +
			// emp.getAddress().getCity());
		}
		
		query = session.getNamedQuery("HQL_GET_userDetails_BY_ID");
		query.setInteger("id", 2);
		UserDetails emp = (UserDetails)query.uniqueResult();
		// System.out.println("userDetails Name=" + emp.getName() + ", City="+
		// emp.getAddress().getCity());
		
		query = session.getNamedQuery("HQL_GET_userDetails_BY_SALARY");
		query.setInteger("salary", 200);
		empList = query.list();
		for (UserDetails emp1 : empList)
		{
			// System.out.println("List of userDetailss::" + emp1.getId() + ","+
			// emp1.getSalary());
		}
		
		query = session.getNamedQuery("@HQL_GET_ALL_ADDRESS");
		List<Address> addressList = query.list();
		for (Address addr : addressList)
		{
			// System.out.println("List of Address::" + addr.getId() + "::"+
			// addr.getZipcode() + "::"+ addr.getUserDetails().getName());
		}
		
		// Native SQL Named Query Example
		query = session.getNamedQuery("@SQL_GET_ALL_ADDRESS");
		List<Object[]> addressObjArray = query.list();
		for (Object[] row : addressObjArray)
		{
			for (Object obj : row)
			{
				System.out.print(obj + "::");
			}
			System.out.println("\n");
		}
		
		query = session.getNamedQuery("SQL_GET_ALL_EMP_ADDRESS");
		addressObjArray = query.list();
		for (Object[] row : addressObjArray)
		{
			UserDetails e = (UserDetails)row[0];
			System.out.println("userDetails Info::" + e);
			Address a = (Address)row[1];
			System.out.println("Address Info::" + a);
		}
		tx.commit();
		sessionFactory.close();
	}
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public UserDetails saveUserDetails(UserDetails dto)
	{
		Session session = sessionFactory.openSession();
		try
		{
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(UserDetails.class).setProjection(Projections.max("id"));
			List<UserDetails> udList = session.createCriteria(UserDetails.class).add(Property.forName("id").eq(maxId)).list();
			session.clear();
			session.close();
			return udList.get(0);
		}
		catch (Exception er)
		{
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserDetails> findUserDetails(UserDetails dto)
	{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("From UserDetails u where u.email='" + dto.getUsers() + "' and u.uPassword='" + dto.getuPassword() + "' ");
		List<UserDetails> ulist = query.list();
		if (ulist != null && ulist.size() > 0)
		{
			return ulist;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ClientSignUp> findAdminUserDetails(ClientSignUp dto)
	{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("From ClientSignUp u where u.email='" + dto.getEmail() + "' and u.password='" + dto.getPassword() + "' ");
		List<ClientSignUp> ulist = query.list();
		if (ulist != null && ulist.size() > 0)
		{
			return ulist;
		}
		return null;
	}
	
	
}