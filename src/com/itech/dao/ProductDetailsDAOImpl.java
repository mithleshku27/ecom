package com.itech.dao;


import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.ProductDetails;


public class ProductDetailsDAOImpl implements ProductDetailsDAO
{
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public ProductDetails saveProductDetails(ProductDetails dto)
	{
		try
		{
			
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			dto.setCreatedDate(new Date());
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(ProductDetails.class).setProjection(Projections.max("prid"));
			List<ProductDetails> pdList = session.createCriteria(ProductDetails.class).add(Property.forName("prid").eq(maxId)).list();
			session.clear();
			session.flush();
			session.close();
			if (pdList != null && pdList.size() > 0)
			{
				return pdList.get(0);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductDetails> getProductDetails()
	{
		Session session = sessionFactory.openSession();
		List<ProductDetails> pdList = session.createQuery("From ProductDetails").list();
		
		session.clear();
		session.flush();
		session.close();
		
		if (pdList != null && pdList.size() > 0)
		{
			return pdList;
		}
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductDetails> getProductDetailsByProductCode(String productCode)
	{
		Session session = sessionFactory.openSession();
		List<ProductDetails> pdList = session.createQuery("From ProductDetails pd where pd.productCode='" + productCode + "''").list();
		session.clear();
		session.flush();
		session.close();
		
		if (pdList != null && pdList.size() > 0)
		{
			return pdList;
		}
		return null;
	}
	
	
	public List<ProductDetails> getProductDetailsByCategory(String category)
	{
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<ProductDetails> pdList = session.createQuery("From ProductDetails where productCategory='"+category+"'").list();
		session.clear();
		session.flush();
		session.close();
		
		if (pdList != null && pdList.size() > 0)
		{
			return pdList;
		}
		return null;
	}
	
	
	public List<ProductDetails> getItemDetailsWithProductID(String productID)
	{
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<ProductDetails> pdList = session.createQuery("From ProductDetails where prid='"+productID+"'").list();
		session.clear();
		session.flush();
		session.close();
		
		if (pdList != null && pdList.size() > 0)
		{
			return pdList;
		}
		return null;
	}
	
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	
}
