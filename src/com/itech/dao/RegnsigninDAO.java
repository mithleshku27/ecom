package com.itech.dao;

import java.util.List;

import com.itech.model.RegSignin;

public interface RegnsigninDAO {

	public RegSignin saveRegnsigninDetails(RegSignin dto);

	public List<RegSignin> findRegnsigninDetails(RegSignin dto);	
}
