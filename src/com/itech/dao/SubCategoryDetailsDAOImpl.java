package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.SubCategoryDetails;


public class SubCategoryDetailsDAOImpl implements SubCategoryDetailsDAO
{
	
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public SubCategoryDetails saveSubCategory(SubCategoryDetails dto)
	{
		try
		{
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit();
			DetachedCriteria maxId = DetachedCriteria.forClass(SubCategoryDetails.class).setProjection(Projections.max("itemId"));
			List<SubCategoryDetails> scdList = session.createCriteria(SubCategoryDetails.class).add(Property.forName("itemId").eq(maxId)).list();
			session.clear();
			session.close();
			return scdList.get(0);
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SubCategoryDetails> getSubCategoryDetailsList()
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<SubCategoryDetails> list = session.createQuery("From SubCategoryDetails").list();
			if (list != null && list.size() > 0)
			{
				return list;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
}
