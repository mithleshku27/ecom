package com.itech.dao;

import java.util.List;

import com.itech.model.Country;

public interface CountryDAO
{
	public List<Country> getContry();
}
