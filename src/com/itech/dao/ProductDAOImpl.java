package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.itech.model.Product; 


public class ProductDAOImpl implements ProductDAO
{
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Product saveProduct(Product dto)
	{
		try
		{
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.save(dto);
			tx.commit(); 
			DetachedCriteria maxId = DetachedCriteria.forClass(Product.class).setProjection(Projections.max("pid"));
			List<Product> pList = session.createCriteria(Product.class).add(Property.forName("pid").eq(maxId)).list();
			session.clear();
			session.close();
			return pList.get(0);
		}
		catch (Exception er)
		{
			
			er.printStackTrace();
			
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> productList()
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<Product> pList = session.createQuery("From Product").list();
			session.close();
			if (pList != null && pList.size() > 0)
			{
				return pList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> productList(int proudctId)
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<Product> pList = session.createQuery("From Product p where p.pid="+proudctId).list();
			session.close();
			if (pList != null && pList.size() > 0)
			{
				return pList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
			
		}
		return null;
	}
	
}
