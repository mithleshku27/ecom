package com.itech.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.itech.model.Country;
import com.itech.model.State;


public class StateDAOImpl implements StateDAO
{
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<State> getState(int stateId)
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<State> stateList = session.createQuery("From State s where s.stateId=" + stateId).list();
			session.close();
			if (stateList != null && stateList.size() > 0)
			{
				return stateList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<State> getStateByCountryId(int countryId)
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<State> stateList = session.createQuery("From State s where s.countryId=" + countryId).list();
			session.close();
			if (stateList != null && stateList.size() > 0)
			{
				return stateList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Country> getCountry(String countryName)
	{
		try
		{
			Session session = sessionFactory.openSession();
			List<Country> stateList = session.createQuery("From Country c where c.value='" + countryName+"'").list();
			session.close();
			if (stateList != null && stateList.size() > 0)
			{
				return stateList;
			}
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return null;
	}
	
}
