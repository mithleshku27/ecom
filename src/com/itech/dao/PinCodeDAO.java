


package com.itech.dao;

import java.util.List;

import com.itech.model.PinCodeModel;

public interface PinCodeDAO
{
	public PinCodeModel  savePinDetails(PinCodeModel dto);
	public List<PinCodeModel>  getPinDetailsList();
}
