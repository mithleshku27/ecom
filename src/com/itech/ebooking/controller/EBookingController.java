package com.itech.ebooking.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.Country;
import com.itech.service.CountryService;


@Controller
public class EBookingController
{
	@Autowired
	private CountryService  countryService;
	@RequestMapping(value="/ebooking",method=RequestMethod.GET)
	public ModelAndView ebooking(HttpServletRequest request, HttpServletResponse response)
	{
		List<Country> countryList = countryService.getContry();
		HttpSession session=request.getSession();
		session.setAttribute("countryList", countryList);
		return new ModelAndView("e-booking");
	}
	
	@RequestMapping(value="/ebookingcontinue",method=RequestMethod.POST)
	public ModelAndView ebookingcontinue(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("api-ecommerces");
	}
	
	@RequestMapping(value="/checkout",method=RequestMethod.GET)
	public ModelAndView payuform(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("payuform");
	}
	
}
