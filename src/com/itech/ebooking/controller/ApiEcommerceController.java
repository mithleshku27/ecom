package com.itech.ebooking.controller;

 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itech.service.ApiEcommerceService;
import com.itech.model.Ecommerce;

@Controller
public class ApiEcommerceController {
	
	@Autowired
	private ApiEcommerceService ecommerceDetailService;

	@RequestMapping(value = "/open-api-ecommerce", method = RequestMethod.GET)
	public ModelAndView openApiEcommerce(HttpServletRequest request, HttpServletResponse response) {
		
		return new ModelAndView("api-ecommerce");
	}
	
	@RequestMapping(value = "/open-api-ecommerce-n", method = RequestMethod.GET)
	public ModelAndView openApiEcommerceNew(HttpServletRequest request, HttpServletResponse response) {
		
		return new ModelAndView("api-ecommerce-n");
	}
	
	
	@RequestMapping(value = "/proceed-api-ecommerce", method = RequestMethod.POST)
	public ModelAndView proceedApiEcommerce(HttpServletRequest request, HttpServletResponse response, @ModelAttribute Ecommerce dto) {
		
		Ecommerce ecom= ecommerceDetailService.saveAddressDetails(dto);
		request.setAttribute("dtos", ecom);
		
		return new ModelAndView("api-ecommerce");
	}
}
