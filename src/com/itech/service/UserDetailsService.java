package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.UserDAO;
import com.itech.model.ClientSignUp;
import com.itech.model.UserDetails;


@Service
public class UserDetailsService
{
	
	@Autowired
	private UserDAO userDAO;
	
	public UserDetails saveUserDetails(UserDetails dto)
	{
		
		return userDAO.saveUserDetails(dto);
	}
	
	public List<UserDetails> findUserDetails(UserDetails dto)
	{
		
		return userDAO.findUserDetails(dto);
	}
	
	public List<ClientSignUp> findAdminUserDetails(ClientSignUp dto)
	{
		return userDAO.findAdminUserDetails(dto);
	}
	
}
