package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.ApiEcommerceDAO;
import com.itech.model.Ecommerce;

@Service
public class ApiEcommerceService {
	@Autowired
	private ApiEcommerceDAO ecommerceDAO;

	public Ecommerce saveAddressDetails(Ecommerce dto) {

		return ecommerceDAO.saveApiEcommerceDetails(dto);
	}

	public List<Ecommerce> findAddressDetails(Ecommerce dto) {
		                    
		return ecommerceDAO.findApiEcommerceDetails(dto);
	}
}
