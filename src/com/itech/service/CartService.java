package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.CartDAO;
import com.itech.model.Cart;


@Service
public class CartService {
	
	@Autowired
	private CartDAO cartDAO;
	
	public Cart saveCartDetails(Cart dto)
	{
		return cartDAO.saveCartDetails(dto);
	}
		
	public List<Cart> findCartDetails(Cart dto)
	{
		return cartDAO.findCartDetails(dto);
	}
	
	public String findUpdateCartDetails(Cart dto)
	{
		return cartDAO.findUpdateCartDetails(dto);
	}
	
	public String DeleteCartDetails(Cart dto)
	{
		return cartDAO.DeleteCartDetails(dto);
	}
}
