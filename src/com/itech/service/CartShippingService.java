package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.CartShippingDAO;
import com.itech.model.Cart;
import com.itech.model.CartShipping;

@Service
public class CartShippingService
{
	@Autowired
	private CartShippingDAO cartShippingDAO;

	public CartShipping saveCartShippingDetails(CartShipping dto, Cart cartdto) {

		return cartShippingDAO.saveCartShippingDetails(dto, cartdto);
	}

	public List<CartShipping> findCartShippingDetails(CartShipping dto) {
		                    
		return cartShippingDAO.findCartShippingDetails(dto);
	}
}
