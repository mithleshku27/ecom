package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.RegnsigninDAO;
import com.itech.model.RegSignin;

@Service
public class RegnsigninService {
	
	 @Autowired
		private RegnsigninDAO regnsigninDAO;

		public RegSignin saveRegnsigninDetails(RegSignin dto) {

			return regnsigninDAO.saveRegnsigninDetails(dto);
		}

		public List<RegSignin> findRegnsigninDetails(RegSignin dto) {

			return regnsigninDAO.findRegnsigninDetails(dto);
		}

}
