package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.CategoryDetailsDAO;
import com.itech.model.CategoryDetails;

@Service
public class CategoryDetailsService 
{
	@Autowired
	private CategoryDetailsDAO CategoryDetailsDAO;

	
	public CategoryDetails saveCategoryDetails(CategoryDetails dto)
	{

		return CategoryDetailsDAO.saveCategoryDetails(dto);
	}

	
	public List<CategoryDetails> getCategoryDetailsList()
	{

		return CategoryDetailsDAO.getCategoryDetailsList();
	}
	
}
