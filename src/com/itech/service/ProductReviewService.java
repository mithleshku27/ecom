package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.ProductReviewDAO;
import com.itech.model.ProductReview;

@Service
public class ProductReviewService
{
	@Autowired
	private ProductReviewDAO productReviewDAO;

	public ProductReview saveProductReviewDetails(ProductReview dto) {

		return productReviewDAO.saveProductReviewDetails(dto);
	}

	public List<ProductReview> findProductReviewDetails(ProductReview dto) {
		                    
		return productReviewDAO.findProductReviewDetails(dto);
	}
}
