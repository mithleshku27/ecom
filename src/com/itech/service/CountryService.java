package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.CountryDAO;
import com.itech.model.Country;


@Service
public class CountryService
{
	@Autowired
	private CountryDAO countryDAO;
	
	public List<Country> getContry()
	{
		return countryDAO.getContry();
	}
	
}
