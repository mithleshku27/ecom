package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.ProductDetailsDAO;
import com.itech.model.ProductDetails;


@Service
public class ProductDetailsService
{
	@Autowired
	private ProductDetailsDAO productDetailsDAO;
	
	public ProductDetails saveProductDetails(ProductDetails dto)
	{
		
		return productDetailsDAO.saveProductDetails(dto);
	}
	
	
	public List<ProductDetails> getProductDetails()
	{
		
		return productDetailsDAO.getProductDetails();
	}
	
	
	public List<ProductDetails> getProductDetailsByProductCode(String productCode)
	{
		
		return productDetailsDAO.getProductDetailsByProductCode(productCode);
	}
	
	public List<ProductDetails> getProductDetailsByCategory(String category){
		return productDetailsDAO.getProductDetailsByCategory(category);
	}
	
	public List<ProductDetails> getItemDetailsWithProductID(String productID)
	{
		return productDetailsDAO.getItemDetailsWithProductID(productID);
	}
}
