package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.ProductDAO;
import com.itech.model.Product;

@Service
public class ProductService  
{
	@Autowired
	private ProductDAO productDAO;


	public Product saveProduct(Product dto)
	{
		
		return productDAO.saveProduct(dto);
	}


	public List<Product> productList()
	{
		
		return productDAO.productList();
	}


	public List<Product> productList(int proudctId)
	{
		
		return productDAO.productList(proudctId);
	}
	
}
