package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.CityDAO;
import com.itech.model.City;
import com.itech.model.State;


@Service
public class CityService
{
	@Autowired
	private CityDAO cityDAO;
	
	public List<City> getCity(int cityId)
	{
		return cityDAO.getCity(cityId);
	}
	
	public List<City> getCityByState(int stateId)
	{
		return cityDAO.getCityByState(stateId);
	}
	
	
	public List<State> getStateName(String stateName){
		
		return cityDAO.getStateName(stateName);
	}
	
}
