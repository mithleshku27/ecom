package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.AddressDAO;
import com.itech.model.Address;

@Service
public class AddressService {
	
	 @Autowired
		private AddressDAO addressDAO;

		public Address saveAddressDetails(Address dto) {

			return addressDAO.saveAuthorDetails(dto);
		}

		public List<Address> findAddressDetails(Address dto) {
			                    
			return addressDAO.findAddressDetails(dto);
		}

}
