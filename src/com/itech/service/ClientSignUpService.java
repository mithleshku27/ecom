package com.itech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.ClientSignUpDAO;
import com.itech.model.ClientSignUp;

@Service
public class ClientSignUpService {
	
	@Autowired
	private ClientSignUpDAO clientSignUpDAO;
 
	public ClientSignUp saveClientSignUp(ClientSignUp dto)
	{
		return clientSignUpDAO.saveClientSignUp(dto);
	}
	
}
