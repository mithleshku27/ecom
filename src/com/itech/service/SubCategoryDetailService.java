package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.SubCategoryDetailsDAO;
import com.itech.model.SubCategoryDetails;


@Service
public class SubCategoryDetailService
{
	@Autowired
	private SubCategoryDetailsDAO subCategoryDetailsDAO;
	
	public SubCategoryDetails saveSubCategory(SubCategoryDetails dto)
	{
		
		return subCategoryDetailsDAO.saveSubCategory(dto);
	}
	
	
	public List<SubCategoryDetails> getSubCategoryDetailsList()
	{
		
		return subCategoryDetailsDAO.getSubCategoryDetailsList();
	}
	
}
