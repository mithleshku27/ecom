package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.StateDAO;
import com.itech.model.Country;
import com.itech.model.State;


@Service
public class StateService
{
	@Autowired
	private StateDAO stateDAO;
	
	public List<State> getState(int stateId)
	{
		return stateDAO.getState(stateId);
	}
	
	public List<State> getStateByCountryId(int countryId)
	{
		return stateDAO.getStateByCountryId(countryId);
	}
	
	public List<Country> getCountry(String countryName){
		return stateDAO.getCountry(countryName);
	}
}
