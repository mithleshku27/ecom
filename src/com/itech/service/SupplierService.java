

package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.SupplierDAO;
import com.itech.model.Supplier;


@Service
public class SupplierService
{
	@Autowired
	private SupplierDAO supplierDAO;
	
	public Supplier saveSupplierDetails(Supplier dto)
	{
		
		return supplierDAO.saveSupplierDetails(dto);
	}
	
	
	public List<Supplier> getSupplierDetailsList()
	{
		
		return supplierDAO.findSupplierDetails();
				
	}
}
	