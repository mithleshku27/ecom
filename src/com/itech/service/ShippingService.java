package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.ShippingDAO;
import com.itech.model.Shipping;
import com.itech.model.SubCategoryDetails;


@Service
public class ShippingService
{
	@Autowired
	private ShippingDAO shippingDAO;
	
	public Shipping saveShippingDetails(Shipping dto)
	{
		
		return shippingDAO.saveShippingDetails(dto);
	}
	
	
	public List<Shipping> getShippingDetailsList()
	{
		
		return shippingDAO.findShippingDetails();
				
	}
}
	