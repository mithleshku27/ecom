package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.itech.dao.WholesaleDAO;
import com.itech.model.WholesaleProduct;


@Service
public class WholesaleService
{
	@Autowired
	private WholesaleDAO wholesaleDAO;
	
	public WholesaleProduct saveWholesale(WholesaleProduct dto)
	{
		
		return wholesaleDAO.saveWholesale(dto);
	}
	
	
	public List<WholesaleProduct> getWholesale(WholesaleProduct dto)
	{
		
		return wholesaleDAO.getWholesale(dto);
	}
	
	
	public List<WholesaleProduct> getWholesaleList()
	{
		
		return wholesaleDAO.getWholesaleList();
	}
	
}
