package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.ProductStockDAO;
import com.itech.model.ProductStock;


@Service
public class ProductStockService
{
	@Autowired
	private ProductStockDAO productStockDAO;
	
	public ProductStock saveProductStock(ProductStock dto)
	{
		
		return productStockDAO.saveProductStock(dto);
	}
	
	
	public List<ProductStock> getProductStockList()
	{
		
		return productStockDAO.getProductStockList();
	}
	
	
	public List<ProductStock> getProductStockByPSId(int pstockId)
	{
		
		return productStockDAO.getProductStockByPSId(pstockId);
	}
	
}
