

package com.itech.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.PinCodeDAO;
import com.itech.model.PinCodeModel;


@Service
public class PinCodeService
{
	@Autowired
	private PinCodeDAO pinDAO;
	
	public PinCodeModel savePinDetail(PinCodeModel dto)
	{
		
		return pinDAO.savePinDetails(dto);
	}
	
	
	public List<PinCodeModel> getPinDetailsList()
	{
		
		return pinDAO.getPinDetailsList();
	}
	
}
