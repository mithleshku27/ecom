package com.itech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itech.dao.AuthorDAO;
import com.itech.model.Author;


@Service
public class AuthorService {

	 @Autowired
	private AuthorDAO authorDAO;

	public Author saveAuthorDetails(Author dto) {

		return authorDAO.saveAuthorDetails(dto);
	}

	public List<Author> findAuthorDetails(Author dto) {

		return authorDAO.findAuthorDetails(dto);
	}
}