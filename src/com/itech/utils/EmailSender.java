package com.itech.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.web.multipart.MultipartFile;


public class EmailSender extends Thread {
	static Random random = new Random();
	//String to_email;
	String text;
	//MultipartFile file;
	MultipartFile file;
	String subject;
	//String file;
	String to_email;
	

	public EmailSender(String text, String subject,String to_email) {
		super();
		//this.to_email = to_email;
		this.text = text;
		this.file = null;
		this.subject = subject;
		this.to_email = to_email;
	}
	
	

	public EmailSender(String text, MultipartFile file, String subject, String to_email) {
		super();
		//this.to_email = to_email;
		this.text = text;
		this.file = file;
		this.subject = subject;
		this.to_email = to_email;
	}



	public void run()  {
		long start = System.currentTimeMillis();
		final String username = "itechmasssecommerce@gmail.com";
		final String password = "itech@mass";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		//String code = String.valueOf(random.nextInt(9000) + 1000);
		
		
		
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to_email));
			message.setSubject(subject);
			MimeBodyPart bodyPart1 = new MimeBodyPart();
			bodyPart1.setText(text);
		
			MimeMultipart multipart = new MimeMultipart();
			File file2 = null;
			if(file != null){
			MimeBodyPart bodyPart2 = new MimeBodyPart();
			

			
			
			
			try {
				file2 = convert(file);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DataSource data = new FileDataSource(file2);  
			
			bodyPart2.setDataHandler(new DataHandler(data));
			bodyPart2.setFileName("Bill");
		
//			multipart.addBodyPart((BodyPart) file);
			
			multipart.addBodyPart(bodyPart2);
			}
			multipart.addBodyPart(bodyPart1);
			
			message.setContent(multipart);
				
			Transport.send(message);
			if(file2 != null)
			file2.delete();

			System.out.println( subject + " Mail sent");
			long end = System.currentTimeMillis();
			System.out.print((end-start)/1000);
			
		} catch (MessagingException e) {
			//return new JSONObject().put("status", "fail").toString();
			System.out.println("mail Failed" + " " + e);
		}

	}

	/*@SuppressWarnings("deprecation")
	public static String verifyPhone(String phone) throws Exception{
		String code = String.valueOf(random.nextInt(9000) + 1000);
		String url = Constants.SMS_VERIFY_URL.replace("{phone}", phone);
		//url = url.replace("{from}", "Codeyeti");
		url = url.replace("{MESSAGE}",URLEncoder.encode("Your Dazl Code is "+ code));
		String json = HttpCaller.get(url);
		
		JSONObject jobj = new JSONObject(json);
		if(!jobj.getJSONObject("data").getJSONObject("0").get("status").equals("AWAITED-DLR"))
			return null;
		return code;
	}*/
	public static void main(String[] args) throws Exception {
 	EmailSender mail = new EmailSender( "Hello", "Hello this is the test mail","mithleshku27@gmail.com");
 	mail.start();
//		System.out.println("dsfsd");
		//verifyPhone("8826644243");
	}
	
	public File convert(MultipartFile file) throws IOException
	{    
	    File convFile = new File("/home/ubuntu/billpic/" + file.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close(); 
	    return convFile;
	}
	
	
}
