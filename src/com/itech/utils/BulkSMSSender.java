package com.itech.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class BulkSMSSender
{
	
	//authkey="85d38befeaf17252e88b604140caa851";
	//http://sms.bulksmsserviceproviders.com/api/balance.php?authkey=YourAuthKey&route=A
	
	//http://sms.bulksmsserviceproviders.com/api/balance.php?authkey=85d38befeaf17252e88b604140caa851&route=4
	
	
	public static void sendSMS(String mobile,String messages){
		
		//Your authentication key
		String authkey = "85d38befeaf17252e88b604140caa851";
		//Multiple mobiles numbers separated by comma
		String mobiles = mobile;//"8882397104";
		//Sender ID,While using route4 sender id should be 6 characters long.
		String senderId = "999999";//itechm
		//Your message to send, Add URL encoding here.
		String message = messages;//"Test message";
		//define route
		String route="B";
		//Prepare Url 8.4  16800 2 lack  sms 19236 
		URLConnection myURLConnection=null;
		URL myURL=null;
		BufferedReader reader=null;
		//encoding message 
		String encoded_message=URLEncoder.encode(message);
		//Send SMS API
		//String mainUrl="http://sms.bulksmsserviceproviders.com/api/send_http.php";
	//	String mainUrl="http://sms.bulksmsserviceproviders.com/api/balance.php?authkey=85d38befeaf17252e88b604140caa851&route=4";
		//// genereated  http://sms.bulksmsserviceproviders.com/api/send_http.php?authkey=85d38befeaf17252e88b604140caa851&mobiles=8882397104&message=this+is+the+test&sender=itechm&route=4
		//multi number http://sms.bulksmsserviceproviders.com/api/send_http.php?authkey=YourAuthKey&mobiles=9999999991,9999999992&message=Hello+Worlds&sender=SenderID&route=A
		//String mainUrl="http://sms.bulksmsserviceproviders.com/api/send_http.php?authkey=85d38befeaf17252e88b604140caa851&route=4";
    	String mainUrl="http://sms.bulksmsserviceproviders.com/api/send_http.php?";//authkey=85d38befeaf17252e88b604140caa851&mobiles=8882397104,8586010562&message=Hello+Worlds&sender=itechm&route=A";
		//103.12.133.204
		
		//String mainUrl="http://sms.bulksmsserviceproviders.com/api/balance.php?";//authkey=YourAuthKey&route=A
		//Prepare parameter string 
		StringBuilder sbPostData= new StringBuilder(mainUrl);
	   sbPostData.append("authkey="+authkey); 
		sbPostData.append("&mobiles="+mobiles);
		sbPostData.append("&message="+encoded_message);		
		sbPostData.append("&sender="+senderId);
		sbPostData.append("&route="+route);
		System.out.println(sbPostData.toString());
		//final string
		mainUrl = sbPostData.toString();
		try
		{
		    //prepare connection
		    myURL = new URL(mainUrl);
		    myURLConnection = myURL.openConnection();
		    myURLConnection.connect();
		    reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		    //reading response 
		    String response;
		    while ((response = reader.readLine()) != null) 
		    //print response 
		    System.out.println(response);

		    //finally close connection
		    reader.close();
		} 
		catch (IOException e) 
		{ 
		    e.printStackTrace();
		} 
	}
	
	public static void main(String[] args)
	{
		//sendSMS();
	}
}
