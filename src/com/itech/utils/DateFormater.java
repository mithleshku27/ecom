package com.itech.utils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * 
 * @author Mithlesh Kumar
 * 
 */

public class DateFormater
{
	
	public static String getStringSqlDateDot(String dateString)
	{
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.text.SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("MM/dd/yyyy");
		String sqlDate = null;
		try
		{
			
			Date date = dateFormat.parse(dateString);
			sqlDate = dateFormat1.format(date);// dateString+":00.0";
		}
		catch (Exception e)
		{
			sqlDate = "";
		}
		return sqlDate;
	}
	
	public static String formatStringDate(String dateString)
	{
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
		java.text.SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("dd-MMM-yyyy");
		String sqlDate = null;
		try
		{
			
			Date date = dateFormat.parse(dateString);
			sqlDate = dateFormat1.format(date);// dateString+":00.0";
		}
		catch (Exception e)
		{
			sqlDate = "";
		}
		return sqlDate;
	}
	
	
	public static String formatStringDate1(String dateString)
	{
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		java.text.SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("dd-MMM-yyyy");
		String sqlDate = null;
		try
		{
			
			Date date = dateFormat.parse(dateString);
			sqlDate = dateFormat1.format(date);// dateString+":00.0";
		}
		catch (Exception e)
		{
			sqlDate = "";
		}
		return sqlDate;
	}
	
	public static String getformatStringDate(String dateString)
	{
		 String sqlDate = null;
		try
		{
			String dateStr = dateString;//"Mon Jun 18 00:00:00 IST 2012";
			DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			Date date = (Date)formatter.parse(dateStr);
			System.out.println(date);        

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" +         cal.get(Calendar.YEAR);
			System.out.println("formatedDate : " + formatedDate);    
			sqlDate=formatedDate;
			 
		}
		catch (Exception e)
		{ e.printStackTrace();
			sqlDate = "";
		}
		return sqlDate;
	}
	public static String formatStringToMMDate(String dateString)
	{
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd-MMM-yyyy");
		java.text.SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("MM/dd/yyyy");
		String sqlDate = null;
		try
		{
			
			Date date = dateFormat.parse(dateString);
			sqlDate = dateFormat1.format(date);// dateString+":00.0";
		}
		catch (Exception e)
		{
			sqlDate = "";
		}
		return sqlDate;
	}
	
	public static String getMonthYr()
	{
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(Calendar.MONTH);
		month++;
		String sMonth = null;
		if (month < 10)
		{
			sMonth = "0" + month;
		}
		else
		{
			sMonth = Integer.toString(month);
		}
		int year = calendar.get(Calendar.YEAR);
		String sMm_yyyy = sMonth + "_" + Integer.toString(year); // date format
		// mm_yyyy
		return sMm_yyyy;
	}
	
	public static String Currentdates()
	{
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(Calendar.MONTH);
		month++;
		String sMonth = null;
		if (month < 10)
		{
			sMonth = "0" + Integer.toString(month);
		}
		else
		{
			sMonth = Integer.toString(month);
		}
		int day = calendar.get(Calendar.DATE);
		String sDay = null;
		if (day < 10)
		{
			sDay = "0" + Integer.toString(day);
		}
		else
		{
			sDay = Integer.toString(day);
		}
		int year = calendar.get(Calendar.YEAR);
		String temp = sMonth + "/" + sDay + "/" + year;
		
		return formatStringDate(temp);
	}
	
	public static String Currentdate()
	{
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(Calendar.MONTH);
		month++;
		String sMonth = null;
		if (month < 10)
		{
			sMonth = "0" + Integer.toString(month);
		}
		else
		{
			sMonth = Integer.toString(month);
		}
		int day = calendar.get(Calendar.DATE);
		String sDay = null;
		if (day < 10)
		{
			sDay = "0" + Integer.toString(day);
		}
		else
		{
			sDay = Integer.toString(day);
		}
		int year = calendar.get(Calendar.YEAR);
		String temp = sMonth + "/" + sDay + "/" + year;
		return temp;
	}
	
	public static boolean isLesserDate(String pDate)
	{
		int iMonth = Integer.parseInt(pDate.substring(0, pDate.indexOf('/'))) - 1;
		String sDatePart = pDate.substring(pDate.indexOf('/') + 1, pDate.lastIndexOf('/'));
		String sYearPart = pDate.substring(pDate.lastIndexOf('/') + 1);
		Calendar calendar = new GregorianCalendar(Integer.parseInt(sYearPart), iMonth, Integer.parseInt(sDatePart));
		GregorianCalendar oCal = new GregorianCalendar();
		oCal.setTime(new Date());
		if (calendar.before(oCal))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	 
	
	public static boolean ValidateCurrentDate(String fromDates, String toDates)
	{
		
		boolean b = true;
		String fromDate = formatStringToMMDate(fromDates);
		String toDate = formatStringToMMDate(toDates);
		
		System.out.println(" fromDate  " + fromDate + " To " + toDates);
		
		int stTime = (Integer.parseInt(fromDate.substring(0, 2)) * 60) + Integer.parseInt(fromDate.substring(3, 5));
		int enTime = (Integer.parseInt(toDate.substring(0, 2)) * 60) + Integer.parseInt(toDate.substring(3, 5));
		
		System.out.println(stTime + "  " + enTime);
		if (enTime <= stTime)
		{
			b = false;
		}
		return b;
	}
	
	public static boolean ValidateToDate(String fromDates, String toDates)
	{
		
		boolean b = true;
		
		int stTime = (Integer.parseInt(fromDates.substring(0, 2)) * 60) + Integer.parseInt(fromDates.substring(3, 5));
		int enTime = (Integer.parseInt(toDates.substring(0, 2)) * 60) + Integer.parseInt(toDates.substring(3, 5));
		if (enTime < stTime)
		{
			b = false;
		}
		return b;
	}
	
	public static String charFormatDate(String dates)
	{
		
		String month[] = new String[] {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		
		if (dates != null)
		{
			int day = Integer.parseInt(dates.substring(3, 5));
			int months = Integer.parseInt(dates.substring(0, 2));
			int year = Integer.parseInt(dates.substring(6, 10));
			String date = +day + "-" + month[months] + "-" + year;
			return date;
		}
		return "";
	}
	
	public static String getMonth(String sStrDate)
	{
		
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd-MMM-yyyy");
		java.text.SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("MM");
		
		String sqlDate = null;
		try
		{
			
			Date date = dateFormat.parse(sStrDate);
			sqlDate = dateFormat1.format(date);
		}
		catch (Exception e)
		{
			
		}
		return sqlDate;
	}
	
	public static String getYear(String sStrDate)
	{
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd-MMM-yyyy");
		java.text.SimpleDateFormat dateFormat1 = new java.text.SimpleDateFormat("yyyy");
		String sqlDate = null;
		try
		{
			Date date = dateFormat.parse(sStrDate);
			sqlDate = dateFormat1.format(date);
		}
		catch (Exception e)
		{
			
		}
		return sqlDate;
	}
	
	public static void main(String[] args)
	{
		// System.out.println(ValidateToDate("12/11/2011", "23/11/2011"));
		// String date = "12/11/2011";
		// System.out.println(charFormatDate("12/01/2011"));
		//
		// String month[] = new String[] {"", "Jan", "Feb", "Mar", "Apr", "May",
		// "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
		//
		
	System.out.println("Formated as " +formatStringDate1("Thu Mar 17 00:00:00 IST 2016")); 
		

	}
}
