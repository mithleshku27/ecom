package com.itech.model;

import javax.persistence.Column;

public class CartList
{
	@Column(name = "userid")
	private Integer userID;
	
	@Column(name = "productid")
	private Integer productID;

	public Integer getUserID()
	{
		return userID;
	}

	public void setUserID(Integer userID)
	{
		this.userID = userID;
	}

	public Integer getProductID()
	{
		return productID;
	}

	public void setProductID(Integer productID)
	{
		this.productID = productID;
	}
	
	
}
