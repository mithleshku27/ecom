package com.itech.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "states")
public class State
{ 
	 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "state_id")
	private Integer stateId;
	@Column(name = "country_id")
	private Integer countryId;
	@Column(name = "state_name")
	private String stateName;
	@Column(name = "status")
	private int status;
	public Integer getStateId()
	{
		return stateId;
	}
	public void setStateId(Integer stateId)
	{
		this.stateId = stateId;
	}
	public Integer getCountryId()
	{
		return countryId;
	}
	public void setCountryId(Integer countryId)
	{
		this.countryId = countryId;
	}
	 
	public String getStateName()
	{
		return stateName;
	}
	public void setStateName(String stateName)
	{
		this.stateName = stateName;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	 
	
}
