package com.itech.model;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "tbl_product_stock")
public class ProductStock
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "psId")
	private Integer psId;
	
	@Column(name = "productId")
	private Integer productId;
	
	@Column(name = "orderInStockQuantity")
	private BigDecimal orderInStockQuantity;
	
	@Column(name = "saleOutQuantity")
	private BigDecimal saleOutQuantity;
	
	@Column(name="unitprice")
	private BigDecimal unitprice;
	
	@Column(name="saleUnitPrice")
	private BigDecimal saleUnitPrice;
	
	@Column(name = "quantityDescription")
	private String quantityDescription;
	
	@Column(name = "createdDate")
	private Date createdDate;
	
	@Column(name = "modifiedDate")
	private Date modifiedDate;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "modifiedBy")
	private String modifiedBy;
	
	@Transient
	private String message;

	 

	public Integer getPsId()
	{
		return psId;
	}

	public void setPsId(Integer psId)
	{
		this.psId = psId;
	}

	public Integer getProductId()
	{
		return productId;
	}

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	public BigDecimal getOrderInStockQuantity()
	{
		return orderInStockQuantity;
	}

	public void setOrderInStockQuantity(BigDecimal orderInStockQuantity)
	{
		this.orderInStockQuantity = orderInStockQuantity;
	}

	public BigDecimal getSaleOutQuantity()
	{
		return saleOutQuantity;
	}

	public void setSaleOutQuantity(BigDecimal saleOutQuantity)
	{
		this.saleOutQuantity = saleOutQuantity;
	}

	public BigDecimal getUnitprice()
	{
		return unitprice;
	}

	public void setUnitprice(BigDecimal unitprice)
	{
		this.unitprice = unitprice;
	}

	public BigDecimal getSaleUnitPrice()
	{
		return saleUnitPrice;
	}

	public void setSaleUnitPrice(BigDecimal saleUnitPrice)
	{
		this.saleUnitPrice = saleUnitPrice;
	}

	public String getQuantityDescription()
	{
		return quantityDescription;
	}

	public void setQuantityDescription(String quantityDescription)
	{
		this.quantityDescription = quantityDescription;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public Date getModifiedDate()
	{
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public String getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
	
	 
	
}
