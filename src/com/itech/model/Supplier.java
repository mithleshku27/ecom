
package com.itech.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/*import javax.persistence.Lob;*/
import javax.persistence.Table;

@Entity
@Table(name = "tbl_supplier_details")
/*@NamedQueries({ @NamedQuery(name = "@HQL_GET_ALL_USER_DETAILS", query = "from UserDetails") })
@NamedNativeQueries({ @NamedNativeQuery(name = "@SQL_GET_ALL_USER_DETAILS", query = "select id, firstName, mobile,email from UserDetails") })*/
public class Supplier {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id")
	private Integer id;
	

	

	@Column(name = "company")
	private String company;
	
	@Column(name = "jobtitle")
	private String jobtitle;

	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "email")
	private String email;
	
	
	@Column(name = "webPage")
	private String webPage;

	@Column(name="buisnessPhone")
	private Integer buisnessPhone;

	@Column(name="fax")
	private String fax;
	
	@Column(name = "homePhones")
	private Integer homePhones;
	
	
	@Column(name = "mobile")
	private Integer mobile;
	
	
	@Column(name = "address")
	private String address;
	
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "stateprovince")
	private String stateprovince;
	

	@Column(name = "postalcode")
	private Integer postalcode;
	
	@Column(name = "countryregion")
	private String countryregion;
	
	
	@Column(name = "notes")
	private String notes;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getJobtitle() {
		return jobtitle;
	}


	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getWebPage() {
		return webPage;
	}


	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}


	public Integer getBuisnessPhone() {
		return buisnessPhone;
	}


	public void setBuisnessPhone(Integer buisnessPhone) {
		this.buisnessPhone = buisnessPhone;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public Integer getHomePhones() {
		return homePhones;
	}


	public void setHomePhones(Integer homePhones) {
		this.homePhones = homePhones;
	}


	public Integer getMobile() {
		return mobile;
	}


	public void setMobile(Integer mobile) {
		this.mobile = mobile;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getStateprovince() {
		return stateprovince;
	}


	public void setStateprovince(String stateprovince) {
		this.stateprovince = stateprovince;
	}


	public Integer getPostalcode() {
		return postalcode;
	}


	public void setPostalcode(Integer postalcode) {
		this.postalcode = postalcode;
	}


	public String getCountryregion() {
		return countryregion;
	}


	public void setCountryregion(String countryregion) {
		this.countryregion = countryregion;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	

	
	

}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
