package com.itech.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "tbl_pin_code_details")
public class PinCodeModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pid")
	private Integer pid;

	@Column(name = "pincode" )
	private Integer pincode;

	@Column(name = "areaLocationAddress",unique=true)
	private String arealoc;
	@Column(name = "mobile",unique=true)
	private String mobile;

	@Column(name = "cratedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date cratedDate;

	@Column(name = "modifiedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;

	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "modidfiedBy")
	private String modidfiedBy;

	@Column(name = "statusAt")
	private String statusAt;

	@Transient
	private String messages;

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public String getArealoc() {
		return arealoc;
	}

	public void setArealoc(String arealoc) {
		this.arealoc = arealoc;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getCratedDate() {
		return cratedDate;
	}

	public void setCratedDate(Date cratedDate) {
		this.cratedDate = cratedDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModidfiedBy() {
		return modidfiedBy;
	}

	public void setModidfiedBy(String modidfiedBy) {
		this.modidfiedBy = modidfiedBy;
	}

	public String getStatusAt() {
		return statusAt;
	}

	public void setStatusAt(String statusAt) {
		this.statusAt = statusAt;
	}

	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}
 
	 
}
