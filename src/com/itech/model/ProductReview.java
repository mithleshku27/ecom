package com.itech.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_product_review")
public class ProductReview
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "reviewdate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date reviewDate;
	
	@Column(name = "productid")
	private String productID;
	
	@Column(name = "productname")
	private String productName;
	
	@Column(name = "username")
	private String userName;
	
	@Column(name = "emailid")
	private String emailID;
	
	@Column(name = "reviewmessage")
	private String reviewMessage;
	
	@Column(name = "userid")
	private String userID;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Date getReviewDate()
	{
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate)
	{
		this.reviewDate = reviewDate;
	}

	public String getProductID()
	{
		return productID;
	}

	public void setProductID(String productID)
	{
		this.productID = productID;
	}

	public String getProductName()
	{
		return productName;
	}

	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getEmailID()
	{
		return emailID;
	}

	public void setEmailID(String emailID)
	{
		this.emailID = emailID;
	}

	public String getReviewMessage()
	{
		return reviewMessage;
	}

	public void setReviewMessage(String reviewMessage)
	{
		this.reviewMessage = reviewMessage;
	}

	public String getUserID()
	{
		return userID;
	}

	public void setUserID(String userID)
	{
		this.userID = userID;
	}
	
	
}
