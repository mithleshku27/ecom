package com.itech.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "tbl_category_details")
public class CategoryDetails
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ctId")
	private int ctId;
	
	@Column(name = "cotegoryName")
	private String cotegoryName;  
	
	@Column(name = "productName")
	private String productName;  
	
	@Column(name = "productDescripion") 
	private String productDescripion;  
	
	@Column(name = "cotegoryDetials")
	private String cotegoryDetials;
	
	@Column(name = "createdDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "modyfiedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modyfiedDate;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "modifiedBy")
	private String modifiedBy;

	public int getCtId()
	{
		return ctId;
	}

	public void setCtId(int ctId)
	{
		this.ctId = ctId;
	}

	public String getCotegoryName()
	{
		return cotegoryName;
	}

	public void setCotegoryName(String cotegoryName)
	{
		this.cotegoryName = cotegoryName;
	}

	public String getProductName()
	{
		return productName;
	}

	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	public String getProductDescripion()
	{
		return productDescripion;
	}

	public void setProductDescripion(String productDescripion)
	{
		this.productDescripion = productDescripion;
	}

	public String getCotegoryDetials()
	{
		return cotegoryDetials;
	}

	public void setCotegoryDetials(String cotegoryDetials)
	{
		this.cotegoryDetials = cotegoryDetials;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public Date getModyfiedDate()
	{
		return modyfiedDate;
	}

	public void setModyfiedDate(Date modyfiedDate)
	{
		this.modyfiedDate = modyfiedDate;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public String getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	 
}
