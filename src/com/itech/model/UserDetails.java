package com.itech.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
 

@Entity
@Table(name = "tbl_user_details")
@NamedQueries({ @NamedQuery(name = "@HQL_GET_ALL_USER_DETAILS", query = "from UserDetails") })
@NamedNativeQueries({ @NamedNativeQuery(name = "@SQL_GET_ALL_USER_DETAILS", query = "select id, firstName, mobile,email from UserDetails") })
public class UserDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "fristName")
	private String fristName; 
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "contact_details")
	private String contactDetails;
	
	@Column(name = "users")
	private String users;
	
	@Column(name = "uPassword")
	private String uPassword;

	@Column(name = "role")
	private String userRole;
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	 
	public String getFristName()
	{
		return fristName;
	}

	public void setFristName(String fristName)
	{
		this.fristName = fristName;
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getContactDetails()
	{
		return contactDetails;
	}

	public void setContactDetails(String contactDetails)
	{
		this.contactDetails = contactDetails;
	}

	public String getUsers()
	{
		return users;
	}

	public void setUsers(String users)
	{
		this.users = users;
	}

	public String getuPassword()
	{
		return uPassword;
	}

	public void setuPassword(String uPassword)
	{
		this.uPassword = uPassword;
	}

	public String getUserRole()
	{
		return userRole;
	}

	public void setUserRole(String userRole)
	{
		this.userRole = userRole;
	}

 
 

}
