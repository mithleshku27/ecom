package com.itech.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_api_ecommerce")
public class Ecommerce {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "nameFrom")
	private String nameFrom;

	@Column(name = "companyFrom")
	private String companyFrom;

	@Column(name = "countryFrom")
	private String countryFrom;

	@Column(name = "address1From")
	private String address1From;

	
	@Column(name = "postalcodeFrom")
	private String postalcodeFrom;

	@Column(name = "cityFrom")
	private String cityFrom;

	@Column(name = "stateFrom")
	private String stateFrom;
	
	@Column(name = "stateTo")
	private String stateTo;
	
	@Column(name = "pinFrom")
	private String pinFrom;
	
	@Column(name = "pinTo")
	private String pinTo;
	
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getNameFrom()
	{
		return nameFrom;
	}

	public void setNameFrom(String nameFrom)
	{
		this.nameFrom = nameFrom;
	}

	public String getCompanyFrom()
	{
		return companyFrom;
	}

	public void setCompanyFrom(String companyFrom)
	{
		this.companyFrom = companyFrom;
	}

	public String getCountryFrom()
	{
		return countryFrom;
	}

	public void setCountryFrom(String countryFrom)
	{
		this.countryFrom = countryFrom;
	}

	public String getAddress1From()
	{
		return address1From;
	}

	public void setAddress1From(String address1From)
	{
		this.address1From = address1From;
	}

	public String getPostalcodeFrom()
	{
		return postalcodeFrom;
	}

	public void setPostalcodeFrom(String postalcodeFrom)
	{
		this.postalcodeFrom = postalcodeFrom;
	}

	public String getCityFrom()
	{
		return cityFrom;
	}

	public void setCityFrom(String cityFrom)
	{
		this.cityFrom = cityFrom;
	}

	public String getStateFrom()
	{
		return stateFrom;
	}

	public void setStateFrom(String stateFrom)
	{
		this.stateFrom = stateFrom;
	}

	public String getStateTo()
	{
		return stateTo;
	}

	public void setStateTo(String stateTo)
	{
		this.stateTo = stateTo;
	}

	public String getPinFrom()
	{
		return pinFrom;
	}

	public void setPinFrom(String pinFrom)
	{
		this.pinFrom = pinFrom;
	}

	public String getPinTo()
	{
		return pinTo;
	}

	public void setPinTo(String pinTo)
	{
		this.pinTo = pinTo;
	}

	public String getPhoneFrom()
	{
		return phoneFrom;
	}

	public void setPhoneFrom(String phoneFrom)
	{
		this.phoneFrom = phoneFrom;
	}

	public String getExtFrom()
	{
		return extFrom;
	}

	public void setExtFrom(String extFrom)
	{
		this.extFrom = extFrom;
	}

	public String getEmailFrom()
	{
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom)
	{
		this.emailFrom = emailFrom;
	}

	public Date getCartDate()
	{
		return cartDate;
	}

	public void setCartDate(Date cartDate)
	{
		this.cartDate = cartDate;
	}

	public Date getBillDate()
	{
		return billDate;
	}

	public void setBillDate(Date billDate)
	{
		this.billDate = billDate;
	}

	public Date getPaymentDate()
	{
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate)
	{
		this.paymentDate = paymentDate;
	}

	public String getPaidstatus()
	{
		return paidstatus;
	}

	public void setPaidstatus(String paidstatus)
	{
		this.paidstatus = paidstatus;
	}

	public String getNameTo()
	{
		return nameTo;
	}

	public void setNameTo(String nameTo)
	{
		this.nameTo = nameTo;
	}

	public String getCompanyTo()
	{
		return companyTo;
	}

	public void setCompanyTo(String companyTo)
	{
		this.companyTo = companyTo;
	}

	public String getCountryTo()
	{
		return countryTo;
	}

	public void setCountryTo(String countryTo)
	{
		this.countryTo = countryTo;
	}

	public String getAddress1To()
	{
		return address1To;
	}

	public void setAddress1To(String address1To)
	{
		this.address1To = address1To;
	}

	public String getEcocostTo()
	{
		return ecocostTo;
	}

	public void setEcocostTo(String ecocostTo)
	{
		this.ecocostTo = ecocostTo;
	}

	public String getShippingcost()
	{
		return shippingcost;
	}

	public void setShippingcost(String shippingcost)
	{
		this.shippingcost = shippingcost;
	}

	public String getPhoneTo()
	{
		return phoneTo;
	}

	public void setPhoneTo(String phoneTo)
	{
		this.phoneTo = phoneTo;
	}

	public String getExtTo()
	{
		return extTo;
	}

	public void setExtTo(String extTo)
	{
		this.extTo = extTo;
	}

	public String getEmailTo()
	{
		return emailTo;
	}

	public void setEmailTo(String emailTo)
	{
		this.emailTo = emailTo;
	}

	public int getIsResidential()
	{
		return isResidential;
	}

	public void setIsResidential(int isResidential)
	{
		this.isResidential = isResidential;
	}

	@Column(name = "phoneFrom")
	private String phoneFrom;

	@Column(name = "extFrom")
	private String extFrom;

	@Column(name = "emailFrom")
	private String emailFrom;

	
	@Column(name = "cartdate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date cartDate;

	@Column(name = "billdate")
	private Date billDate;

	@Column(name = "paymentdate")
	private Date paymentDate;

	@Column(name = "paidstatus")
	private String paidstatus;
	
	@Column(name = "nameTo")
	private String nameTo;

	@Column(name = "companyTo")
	private String companyTo;

	@Column(name = "countryTo")
	private String countryTo;

	@Column(name = "address1To")
	private String address1To;

	

	@Column(name = "postalcodeTo")
	private String ecocostTo;

	@Column(name = "cityTo")
	private String shippingcost;


	@Column(name = "phoneTo")
	private String phoneTo;

	
	@Column(name = "extTo")
	private String extTo;

	@Column(name = "emailTo")
	private String emailTo;

	@Column(name = "isResidential")
	private int isResidential;

	
}
