package com.itech.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_shipping_cart")
public class CartShipping
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "userid")
	private Integer userid;
	
	@Column(name = "username")
	private String username;

	@Column(name = "customername")
	private String customername;
	

	@Column(name = "customeremail")
	private String customeremail;
	

	@Column(name = "address1")
	private String address1;
	

	@Column(name = "address2")
	private String address2;
	

	@Column(name = "pincode")
	private String pincode;
	

	@Column(name = "countryname")
	private String countryname;
	

	@Column(name = "statename")
	private String statename;
	

	@Column(name = "mobileno")
	private String mobileno;
	

	@Column(name = "shippingnote")
	private String shippingnote;

	
	@Column(name = "emailid")
	private String emailid;

	@Column(name = "cityname")
	private String cityname;


	public String getCityname()
	{
		return cityname;
	}


	public void setCityname(String cityname)
	{
		this.cityname = cityname;
	}


	public String getEmailid()
	{
		return emailid;
	}


	public void setEmailid(String emailid)
	{
		this.emailid = emailid;
	}


	public Integer getId()
	{
		return id;
	}


	public void setId(Integer id)
	{
		this.id = id;
	}


	public Integer getUserid()
	{
		return userid;
	}


	public void setUserid(Integer userid)
	{
		this.userid = userid;
	}


	public String getUsername()
	{
		return username;
	}


	public void setUsername(String username)
	{
		this.username = username;
	}


	public String getCustomername()
	{
		return customername;
	}


	public void setCustomername(String customername)
	{
		this.customername = customername;
	}


	public String getCustomeremail()
	{
		return customeremail;
	}


	public void setCustomeremail(String customeremail)
	{
		this.customeremail = customeremail;
	}


	public String getAddress1()
	{
		return address1;
	}


	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}


	public String getAddress2()
	{
		return address2;
	}


	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}


	public String getPincode()
	{
		return pincode;
	}


	public void setPincode(String pincode)
	{
		this.pincode = pincode;
	}


	public String getCountryname()
	{
		return countryname;
	}


	public void setCountryname(String countryname)
	{
		this.countryname = countryname;
	}


	public String getStatename()
	{
		return statename;
	}


	public void setStatename(String statename)
	{
		this.statename = statename;
	}


	public String getMobileno()
	{
		return mobileno;
	}


	public void setMobileno(String mobileno)
	{
		this.mobileno = mobileno;
	}


	public String getShippingnote()
	{
		return shippingnote;
	}


	public void setShippingnote(String shippingnote)
	{
		this.shippingnote = shippingnote;
	}
	
	
}
