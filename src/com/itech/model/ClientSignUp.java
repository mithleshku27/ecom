package com.itech.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
 

@Entity
@Table(name = "tbl_client_signup")
public class ClientSignUp {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="firstname")
	private  String firstname;

	@Column(name="lastname")
	private String lastname;

	@Column(name="address")
	private String address;

	@Column(name="city")
	private String city;

	@Column(name="state")
	private String state;

	@Column(name="pin")
	private String pin;

	@Column(name="email")
	private String email;

	@Column(name="mobileno")
	private String mobileno;

	@Column(name="password")
	private String password;

	@Column(name="is_valid_email")
	private int is_valid_email;

	@Column(name="is_valid_mobile")
	private int is_valid_mobile;

	@Column(name="registration_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registration_date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getIs_valid_email() {
		return is_valid_email;
	}

	public void setIs_valid_email(int is_valid_email) {
		this.is_valid_email = is_valid_email;
	}

	public int getIs_valid_mobile() {
		return is_valid_mobile;
	}

	public void setIs_valid_mobile(int is_valid_mobile) {
		this.is_valid_mobile = is_valid_mobile;
	}

	public Date getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Date registration_date) {
		this.registration_date = registration_date;
	}

	 
	
}
