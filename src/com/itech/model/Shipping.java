package com.itech.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
 

@Entity
@Table(name = "tbl_shipping_details")
/*@NamedQueries({ @NamedQuery(name = "@HQL_GET_ALL_USER_DETAILS", query = "from UserDetails") })
@NamedNativeQueries({ @NamedNativeQuery(name = "@SQL_GET_ALL_USER_DETAILS", query = "select id, firstName, mobile,email from UserDetails") })*/
public class Shipping {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id")
	private Integer id;
	
	
	@Column(name = "countrylocationfrm")
	private String countrylocationfrm;
	
	
	@Column(name = "postalcodefrm")
	private Integer postalcodefrm;
	
	@Column(name = "cityfrm")
	private String cityfrm;

	
	@Column(name = "countrylocationto")
	private String countrylocationto;
	

	
	@Column(name = "postalcodeto")
	private Integer postalcodeto;
	
	@Column(name = "cityto")
	private String cityto;
	
	
	@Column(name = "nmofpkgs")
	private Integer nmofpkgs;


	
	
	
	@Column(name="weight")
	private int weight;
	
	
	
	
	
	
	@Column(name="unitweight")
	private int unitweight;
	
	@Column(name = "pkupdrpoff")
	private String pkupdrpoff;
	
	
	

	@Column(name="shippingdate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date shippingdate;




	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public String getCountrylocationfrm() {
		return countrylocationfrm;
	}




	public void setCountrylocationfrm(String countrylocationfrm) {
		this.countrylocationfrm = countrylocationfrm;
	}




	public Integer getPostalcodefrm() {
		return postalcodefrm;
	}




	public void setPostalcodefrm(Integer postalcodefrm) {
		this.postalcodefrm = postalcodefrm;
	}




	public String getCityfrm() {
		return cityfrm;
	}




	public void setCityfrm(String cityfrm) {
		this.cityfrm = cityfrm;
	}




	public String getCountrylocationto() {
		return countrylocationto;
	}




	public void setCountrylocationto(String countrylocationto) {
		this.countrylocationto = countrylocationto;
	}




	public Integer getPostalcodeto() {
		return postalcodeto;
	}




	public void setPostalcodeto(Integer postalcodeto) {
		this.postalcodeto = postalcodeto;
	}




	public String getCityto() {
		return cityto;
	}




	public void setCityto(String cityto) {
		this.cityto = cityto;
	}




	public Integer getNmofpkgs() {
		return nmofpkgs;
	}




	public void setNmofpkgs(Integer nmofpkgs) {
		this.nmofpkgs = nmofpkgs;
	}




	public int getWeight() {
		return weight;
	}




	public void setWeight(int weight) {
		this.weight = weight;
	}




	public int getUnitweight() {
		return unitweight;
	}




	public void setUnitweight(int unitweight) {
		this.unitweight = unitweight;
	}




	public String getPkupdrpoff() {
		return pkupdrpoff;
	}




	public void setPkupdrpoff(String pkupdrpoff) {
		this.pkupdrpoff = pkupdrpoff;
	}




	public Date getShippingdate() {
		return shippingdate;
	}




	public void setShippingdate(Date shippingdate) {
		this.shippingdate = shippingdate;
	}







	

}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
