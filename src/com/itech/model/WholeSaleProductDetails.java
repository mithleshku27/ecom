package com.itech.model;


import java.util.Date; 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "tbl_whole_sale_product_details")
public class WholeSaleProductDetails
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "wholesaleId")
	private String wholesaleId;
	
	@Column(name = "productName")
	private String productName;
	
	@Column(name = "productDescription")
	private String productDescription;
	
	@Column(name = "manufacturingDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date manufacturingDate;
	
	@Column(name = "productCode")
	private String productCode;
	
	@Column(name = "quantity")
	private double quantity;
	
	@Column(name = "unitPrice")
	private double unitPrice;
	
	@Column(name = "amount")
	private double amount;
	@Column(name = "wholeSaleProductDetails")
	private Integer wholeSaleProductDetails;
	@Column(name = "createdDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "modifiedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "modifiedBy")
	private String modifiedBy;
	
	public String getWholesaleId()
	{
		return wholesaleId;
	}
	
	public void setWholesaleId(String wholesaleId)
	{
		this.wholesaleId = wholesaleId;
	}
	
	public String getProductName()
	{
		return productName;
	}
	
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	
	public String getProductDescription()
	{
		return productDescription;
	}
	
	public void setProductDescription(String productDescription)
	{
		this.productDescription = productDescription;
	}
	
	public Date getManufacturingDate()
	{
		return manufacturingDate;
	}
	
	public void setManufacturingDate(Date manufacturingDate)
	{
		this.manufacturingDate = manufacturingDate;
	}
	
	public String getProductCode()
	{
		return productCode;
	}
	
	public void setProductCode(String productCode)
	{
		this.productCode = productCode;
	}
	
	public double getQuantity()
	{
		return quantity;
	}
	
	public void setQuantity(double quantity)
	{
		this.quantity = quantity;
	}
	
	public double getUnitPrice()
	{
		return unitPrice;
	}
	
	public void setUnitPrice(double unitPrice)
	{
		this.unitPrice = unitPrice;
	}
	
	public double getAmount()
	{
		return amount;
	}
	
	public void setAmount(double amount)
	{
		this.amount = amount;
	}
	
	public Date getCreatedDate()
	{
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}
	
	public Date getModifiedDate()
	{
		return modifiedDate;
	}
	
	public void setModifiedDate(Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}
	
	public String getCreatedBy()
	{
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}
	
	public String getModifiedBy()
	{
		return modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	
	public Integer getWholeSaleProductDetails()
	{
		return wholeSaleProductDetails;
	}
	
	public void setWholeSaleProductDetails(Integer wholeSaleProductDetails)
	{
		this.wholeSaleProductDetails = wholeSaleProductDetails;
	}
	
	
}
