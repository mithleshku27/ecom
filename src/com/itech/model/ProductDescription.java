package com.itech.model; 
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
 

@Entity
@Table(name = "tbl_product_description")
public class ProductDescription {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="productname")
	private  String productname;
		
	@Column(name="productcode")
	private String productcode;
	
	@Column(name="producttype")
	private String producttype;
	
	@Column(name="size")
	private int size;
	
	@Column(name="weight")
	private int weight;
	
	@Column(name="energysaving")
	private String energysaving;
	
	@Column(name="manufacturingdate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date manufacturingdate;
	
	@Column(name="quantity")
	private double quantity;
	
	@Column(name="unitprice")
	private double unitprice;
	
	@Column(name="description")
	private String description;
	
	@Column(name="inputvoltage")
	private String inputvoltage;
	
	@Column(name="inputpower")
	private String inputpower;
	
	@Column(name="frequency")
	private String frequency;
	
	@Column(name="powerfactor")
	private String powerfactor;
	
	@Column(name="colourtempratures")
	private String colourtempratures;
	
	@Column(name="colourrenderingindex")
	private String colourrenderingindex;
	
	@Column(name="ingressprotection")
	private String ingressprotection;
	
	@Column(name="impactprotection")
	private String impactprotection;
	
	@Column(name="lifespan")
	private String lifespan;
	
	@Column(name="safetystandard")
	private String safetystandard;
	
	@Column(name="emcemistandard")
	private String emcemistandard;
	
	@Column(name="harmonics")
	private String harmonics;
	
	@Column(name="lumenus")
	private String lumenus;
	
	@Column(name="incandescent")
	private String incandescent;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getProductcode() {
		return productcode;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public String getProducttype() {
		return producttype;
	}

	public void setProducttype(String producttype) {
		this.producttype = producttype;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getEnergysaving() {
		return energysaving;
	}

	public void setEnergysaving(String energysaving) {
		this.energysaving = energysaving;
	}

	public Date getManufacturingdate() {
		return manufacturingdate;
	}

	public void setManufacturingdate(Date manufacturingdate) {
		this.manufacturingdate = manufacturingdate;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInputvoltage() {
		return inputvoltage;
	}

	public void setInputvoltage(String inputvoltage) {
		this.inputvoltage = inputvoltage;
	}

	public String getInputpower() {
		return inputpower;
	}

	public void setInputpower(String inputpower) {
		this.inputpower = inputpower;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getPowerfactor() {
		return powerfactor;
	}

	public void setPowerfactor(String powerfactor) {
		this.powerfactor = powerfactor;
	}

	public String getColourtempratures() {
		return colourtempratures;
	}

	public void setColourtempratures(String colourtempratures) {
		this.colourtempratures = colourtempratures;
	}

	public String getColourrenderingindex() {
		return colourrenderingindex;
	}

	public void setColourrenderingindex(String colourrenderingindex) {
		this.colourrenderingindex = colourrenderingindex;
	}

	public String getIngressprotection() {
		return ingressprotection;
	}

	public void setIngressprotection(String ingressprotection) {
		this.ingressprotection = ingressprotection;
	}

	public String getImpactprotection() {
		return impactprotection;
	}

	public void setImpactprotection(String impactprotection) {
		this.impactprotection = impactprotection;
	}

	public String getLifespan() {
		return lifespan;
	}

	public void setLifespan(String lifespan) {
		this.lifespan = lifespan;
	}

	public String getSafetystandard() {
		return safetystandard;
	}

	public void setSafetystandard(String safetystandard) {
		this.safetystandard = safetystandard;
	}

	public String getEmcemistandard() {
		return emcemistandard;
	}

	public void setEmcemistandard(String emcemistandard) {
		this.emcemistandard = emcemistandard;
	}

	public String getHarmonics() {
		return harmonics;
	}

	public void setHarmonics(String harmonics) {
		this.harmonics = harmonics;
	}

	public String getLumenus() {
		return lumenus;
	}

	public void setLumenus(String lumenus) {
		this.lumenus = lumenus;
	}

	public String getIncandescent() {
		return incandescent;
	}

	public void setIncandescent(String incandescent) {
		this.incandescent = incandescent;
	}
 
	
	

}
