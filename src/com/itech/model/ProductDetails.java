package com.itech.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "tbl_productDetails")
public class ProductDetails
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "prid")
	private Integer prid;
	
	@Column(name = "productName")
	private String productName;
	
	@Column(name = "productCode")
	private String productCode;
	
	@Lob
	@Column(name = "productImage", nullable = false, columnDefinition = "mediumblob")
	private byte[] imageFile;
	
	@Column(name = "unitPrice")
	private double unitPrice;
	
	@Column(name = "totalQuantity")
	private double totalQuantity;
	
	@Column(name = "productCategory")
	private String productCategory;
	
	@Column(name = "itemCategory")
	private String itemCategory;
	
	@Column(name = "createdDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "modifiedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;
	
	@Column(name = "modifiedBy")
	private String modifiedBy;
	
	public Integer getPrid()
	{
		return prid;
	}
	
	public void setPrid(Integer prid)
	{
		this.prid = prid;
	}
	
	public String getProductName()
	{
		return productName;
	}
	
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	
	public String getProductCode()
	{
		return productCode;
	}
	
	public void setProductCode(String productCode)
	{
		this.productCode = productCode;
	}
	
	public byte[] getImageFile()
	{
		return imageFile;
	}
	
	public void setImageFile(byte[] imageFile)
	{
		this.imageFile = imageFile;
	}
	
	public double getUnitPrice()
	{
		return unitPrice;
	}
	
	public void setUnitPrice(double unitPrice)
	{
		this.unitPrice = unitPrice;
	}
	
	public double getTotalQuantity()
	{
		return totalQuantity;
	}
	
	public void setTotalQuantity(double totalQuantity)
	{
		this.totalQuantity = totalQuantity;
	}
	
	public Date getCreatedDate()
	{
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}
	
	public String getCreatedBy()
	{
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}
	
	public Date getModifiedDate()
	{
		return modifiedDate;
	}
	
	public void setModifiedDate(Date modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}
	
	public String getModifiedBy()
	{
		return modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public String getProductCategory()
	{
		return productCategory;
	}

	public void setProductCategory(String productCategory)
	{
		this.productCategory = productCategory;
	}

	public String getItemCategory()
	{
		return itemCategory;
	}

	public void setItemCategory(String itemCategory)
	{
		this.itemCategory = itemCategory;
	}
	
	
}
