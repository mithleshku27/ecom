package com.itech.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name = "tbl_user_cart")
public class Cart
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	public Date getDeliveryDate()
	{
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}




	@Column(name = "userid")
	private Integer userID;
	
	@Column(name = "productid")
	private Integer productID;
	
	@Column(name = "productname")
	private String productName;
	
	@Column(name = "productPrice")
	private double productPrice;
	
	@Column(name = "quantity")
	private double quantity;
	
	@Column(name = "ecocost")
	private double ecocost;
	
	@Column(name = "shippingcost")
	private double shippingcost;
	
	@Column(name = "totalamount")
	private double totalamount;
	
	@Column(name = "cartdate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date cartDate;

	@Column(name = "billdate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date billDate;

	@Column(name = "deliverydate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deliveryDate;
	
	@Column(name = "paymentdate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date paymentDate;
	
	
	

	@Transient
	private String cartDates;
	@Transient
	private String billDates;
	@Transient
	private String deliveryDates;
	@Transient
	private String paymentDates;
	
	
	@Column(name = "paidstatus")
	private Integer paidstatus;
	
	@Column(name = "shippingid")
	private Integer shippingid;
	 
	@Transient
	private byte[] imageFile;
	
	public byte[] getImageFile()
	{
		return imageFile;
	}
	
	public void setImageFile(byte[] imageFile)
	{
		this.imageFile = imageFile;
	}
	
	public Integer getShippingid()
	{
		return shippingid;
	}
	
	public void setShippingid(Integer shippingid)
	{
		this.shippingid = shippingid;
	}
	
	public Integer getId()
	{
		return id;
	}
	
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public Integer getUserID()
	{
		return userID;
	}
	 
	public String getProductName()
	{
		return productName;
	}
	
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	
	public double getProductPrice()
	{
		return productPrice;
	}
	
	public void setProductPrice(double productPrice)
	{
		this.productPrice = productPrice;
	}
	
	public double getQuantity()
	{
		return quantity;
	}
	
	public void setQuantity(double quantity)
	{
		this.quantity = quantity;
	}
	
	public double getEcocost()
	{
		return ecocost;
	}
	
	public void setEcocost(double ecocost)
	{
		this.ecocost = ecocost;
	}
	
	public double getShippingcost()
	{
		return shippingcost;
	}
	
	public void setShippingcost(double shippingcost)
	{
		this.shippingcost = shippingcost;
	}
	
	public double getTotalamount()
	{
		return totalamount;
	}
	
	public void setTotalamount(double totalamount)
	{
		this.totalamount = totalamount;
	}
	
	public Date getCartDate()
	{
		return cartDate;
	}
	
	public void setCartDate(Date cartDate)
	{
		this.cartDate = cartDate;
	}
	
	public Date getBillDate()
	{
		return billDate;
	}
	
	public void setBillDate(Date billDate)
	{
		this.billDate = billDate;
	}
	
	public Date getPaymentDate()
	{
		return paymentDate;
	}
	
	public void setPaymentDate(Date paymentDate)
	{
		this.paymentDate = paymentDate;
	}
	
	public Integer getPaidstatus()
	{
		return paidstatus;
	}
	
	public void setPaidstatus(Integer paidstatus)
	{
		this.paidstatus = paidstatus;
	}

	public Integer getProductID()
	{
		return productID;
	}

	public void setProductID(Integer productID)
	{
		this.productID = productID;
	}

	 
	 

	public void setUserID(Integer userID)
	{
		this.userID = userID;
	}

	public String getCartDates()
	{
		return cartDates;
	}

	public void setCartDates(String cartDates)
	{
		this.cartDates = cartDates;
	}

	public String getBillDates()
	{
		return billDates;
	}

	public void setBillDates(String billDates)
	{
		this.billDates = billDates;
	}

	public String getDeliveryDates()
	{
		return deliveryDates;
	}

	public void setDeliveryDates(String deliveryDates)
	{
		this.deliveryDates = deliveryDates;
	}

	public String getPaymentDates()
	{
		return paymentDates;
	}

	public void setPaymentDates(String paymentDates)
	{
		this.paymentDates = paymentDates;
	}

	 
}
