package com.itech.model;


import java.math.BigDecimal;


public class Price
{
	private Integer productId;
	private Integer partyId;
	private Integer prodCatalogId;
	private Integer supplierId;
	private Integer productStoreId;
	private Integer productStoreGroupId;
	private Integer agreementId;
	private Integer quantity;
	private Integer currencyUomId;
	private Integer checkIncludeVat;	
	private BigDecimal productPrice;
}
