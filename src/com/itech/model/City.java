package com.itech.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "cities")
public class City
{    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "city_id")
	private Integer cityId;
	@Column(name = "city")
	private String city;
	@Column(name = "state_id")
	private Integer stateId;
	@Column(name = "status")
	private int status;
	public Integer getCityId()
	{
		return cityId;
	}
	public void setCityId(Integer cityId)
	{
		this.cityId = cityId;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public Integer getStateId()
	{
		return stateId;
	}
	public void setStateId(Integer stateId)
	{
		this.stateId = stateId;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	
	
	
}
