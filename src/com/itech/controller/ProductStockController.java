package com.itech.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.City;
import com.itech.model.Product;
import com.itech.model.ProductStock;
import com.itech.service.ProductService;
import com.itech.service.ProductStockService;


@Controller
public class ProductStockController
{
	@Autowired
	ProductService productService;
	
	@Autowired
	private ProductStockService productStockService;
	
	@RequestMapping(value = "/admin/openstock", method = RequestMethod.GET)
	public ModelAndView openStock(HttpServletRequest request, HttpServletResponse response)
	{
		
		return new ModelAndView("productStock");
		
	}
	
	
	@RequestMapping(value = "/admin/savestock", method = RequestMethod.POST)
	public ModelAndView saveStock(HttpServletRequest request, HttpServletResponse response, @ModelAttribute ProductStock dto)
	{
		ProductStock saveBack = productStockService.saveProductStock(dto);
		request.setAttribute("saveBack", saveBack);
		;
		return new ModelAndView("productStock");
		
	}
	
	@RequestMapping(value = "/admin/fetch/productList", method = RequestMethod.GET)
	@ResponseBody
	public String getProductList(HttpServletRequest request, HttpServletResponse response)
	{
		List<Product> pList = productService.productList();
		StringBuilder productBuilder = new StringBuilder();
		productBuilder.append("<option value='0'>--Select Product--</option>");
		
		if (pList != null && pList.size() > 0)
		{
			for (Product dto : pList)
			{
				productBuilder.append("<option value='" + dto.getProductCode() + "'>" + dto.getProductName() + "</option>");
				
			}
			return productBuilder.toString();
		}
		
		return null;
	}
	
}
