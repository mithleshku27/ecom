
package com.itech.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.Supplier;
import com.itech.service.SupplierService;



@Controller
public class SupplierController
{
	@Autowired
	SupplierService supplService;
	
	@RequestMapping(value = "/admin/supplierdetail", method = RequestMethod.GET)
	public ModelAndView supplierdetails(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("supplierDetails");
	}
	
	
	
	
	@RequestMapping(value = "/admin/savesupplierDetails", method = RequestMethod.POST)
	public ModelAndView savesupplierDetails(HttpServletRequest request,HttpServletResponse response,@ModelAttribute Supplier dto)

	{
	
		Supplier dtos = supplService.saveSupplierDetails(dto);
		request.setAttribute("dtos", dtos);
		return new ModelAndView("supplierDetails");
	}
	

}


