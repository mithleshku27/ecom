package com.itech.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.Product;
import com.itech.service.ProductService;


@Controller
public class ProductDescriptionController
{
	
	@Autowired
	ProductService productService;
	
	@RequestMapping(value = "/admin/openpd", method = RequestMethod.GET)
	public ModelAndView openProduct(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("addProductDescriptionInfo");
	}
	
	
	@RequestMapping(value = "/admin/saveProducts", method = RequestMethod.POST)
	public ModelAndView saveProducts(HttpServletRequest request, HttpServletResponse response, @ModelAttribute Product dto)
	{
		
		Product dtos = productService.saveProduct(dto);
		request.setAttribute("dtos", dtos);
		return new ModelAndView("addProductDescriptionInfo");
	}
}
