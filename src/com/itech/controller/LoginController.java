package com.itech.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.ClientSignUp;
import com.itech.model.UserDetails;
import com.itech.service.ClientSignUpService;
import com.itech.service.UserDetailsService;
import com.itech.utils.StrongAESAPI;


@Controller
public class LoginController
{
	@Autowired
	private UserDetailsService userDetailService;
	@Autowired
	private ClientSignUpService clientSignUpService;
	
	@RequestMapping(value = "/consumer/login", method = RequestMethod.GET)
	public ModelAndView openLogin(HttpServletRequest request, HttpServletResponse response)
	{
		
		return new ModelAndView("signin");
	}
	
	
	
	@RequestMapping(value = "/consumer/loginProceed", method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response, @ModelAttribute UserDetails userDetails)
	{
		 
		String encriptPass = "";
		try
		{
			encriptPass = StrongAESAPI.encrypt(userDetails.getuPassword());
		}
		catch (Exception e)
		{ e.printStackTrace();
		}
		if (encriptPass != "")
		{
			userDetails.setuPassword(encriptPass);
		}
		List<UserDetails> ud = userDetailService.findUserDetails(userDetails);
		HttpSession session = request.getSession();		
		if (ud != null && ud.size() > 0)
		{
			session.setAttribute("userDetails", ud.get(0));
			return new ModelAndView("consuler-welcome-page");
		}
		else
		{
			return new ModelAndView("consumer-signinFail");
		} 
	}
	

	@RequestMapping(value = "/consumer/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session =request.getSession();
		if(session.isNew()){
			session.invalidate();
		}
		return new ModelAndView("index");
	}
	
	@RequestMapping(value = "/admin/login", method = RequestMethod.GET)
	public ModelAndView openAdminLogin(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session =request.getSession();
		if(session.isNew()){
			session.invalidate();
		}
		return new ModelAndView("admin-signin");
	}
	
	@RequestMapping(value = "/admin/loginProceed", method = RequestMethod.POST)
	public ModelAndView adminlogin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute ClientSignUp userDetails)
	{
		 
		String encriptPass = "";
		try
		{
			encriptPass = StrongAESAPI.encrypt(userDetails.getPassword());
		}
		catch (Exception e)
		{ e.printStackTrace();
		}
		if (encriptPass != "")
		{
			userDetails.setPassword(encriptPass);
		}
		List<ClientSignUp> ud = userDetailService.findAdminUserDetails(userDetails);
		HttpSession session = request.getSession();
		
		if (ud != null && ud.size() > 0)
		{
			session.setAttribute("ClientSignUp", ud.get(0));
			return new ModelAndView("welcome-page");
		}
		else
		{
			return new ModelAndView("admin-signin-fail");
		} 
	}
	
	@RequestMapping(value = "/admin/logout", method = RequestMethod.GET)
	public ModelAndView adminlogout(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session =request.getSession();
		if(session.isNew()){
			session.invalidate();
		}
		return new ModelAndView("admin-signin-fail");
	}
}
