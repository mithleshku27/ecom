package com.itech.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itech.model.City;
import com.itech.model.Country;
import com.itech.model.State;
import com.itech.service.CityService;
import com.itech.service.CountryService;
import com.itech.service.StateService;


@Controller
public class CommanController
{
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;
	@Autowired
	private CityService cityService;
	
	@RequestMapping(value = "/fetch/city/by/stateid", method = RequestMethod.GET)
	@ResponseBody
	public String getCityByStateId(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		StringBuilder cityBuilder = new StringBuilder();
		cityBuilder.append("<option value='0'>--Select State--</option>");
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			int stateIds = Integer.parseInt(stateId);
			
			List<City> cityList = cityService.getCityByState(stateIds);
			if (cityList != null && cityList.size() > 0)
			{
				for (City dto : cityList)
				{
					cityBuilder.append("<option value='" + dto.getCity() + "'>" + dto.getCity() + "</option>");
					
				}
				return cityBuilder.toString();
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/fetch/city/by/stateName", method = RequestMethod.GET)
	@ResponseBody
	public String getCityByStateName(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateName)
	{
		StringBuilder cityBuilder = new StringBuilder();
		cityBuilder.append("<option value='0'>--Select State--</option>");
		if (stateName != null && !stateName.equalsIgnoreCase(""))
		{
			List<State> stateList = cityService.getStateName(stateName);
			if (stateList != null && stateList.size() > 0)
			{
				int ii= stateList.get(0).getStateId();
				List<City> cityList = cityService.getCityByState(ii);
				
				if (cityList != null && cityList.size() > 0)
				{
					for (City dto : cityList)
					{
						cityBuilder.append("<option value='" + dto.getCity() + "'>" + dto.getCity() + "</option>");
						
					}
				}
				return cityBuilder.toString();
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/fetch/city/by/cityid", method = RequestMethod.GET)
	@ResponseBody
	public String getCityById(HttpServletRequest request, HttpServletResponse response, @RequestParam String cityId)
	{
		StringBuilder cityBuilder = new StringBuilder();
		cityBuilder.append("<option value='0'>--Select State--</option>");
		if (cityId != null && !cityId.equalsIgnoreCase(""))
		{
			int cityIds = Integer.parseInt(cityId);
			List<City> cityList = cityService.getCity(cityIds);
			if (cityList != null && cityList.size() > 0)
			{
				for (City dto : cityList)
				{
					cityBuilder.append("<option value='" + dto.getCity() + "'>" + dto.getCity() + "</option>");
					
				}
				return cityBuilder.toString();
			}
		}
		return null;
	}
	
	
	@RequestMapping(value = "/fetch/city/by/stateIdList", method = RequestMethod.GET)
	@ResponseBody
	public List<City> getCityByStateIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			int stateIds = Integer.parseInt(stateId);
			List<City> cityList = cityService.getCityByState(stateIds);
			if (cityList != null && cityList.size() > 0)
			{
				return cityList;
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/fetch/city/by/cityIdList", method = RequestMethod.GET)
	@ResponseBody
	public List<City> getCityByIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String cityId)
	{
		if (cityId != null && !cityId.equalsIgnoreCase(""))
		{
			int cityIds = Integer.parseInt(cityId);
			List<City> cityList = cityService.getCity(cityIds);
			if (cityList != null && cityList.size() > 0)
			{
				return cityList;
			}
		}
		return null;
	}
	
	
	@RequestMapping(value = "/fetch/state/by/stateId", method = RequestMethod.GET)
	@ResponseBody
	public String getStateById(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		StringBuilder stateBuilder = new StringBuilder();
		stateBuilder.append("<option value='0'>--Select State--</option>");
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			// stateService.
			int stateIds = Integer.parseInt(stateId);
			List<State> stateList = stateService.getState(stateIds);
			if (stateList != null && stateList.size() > 0)
			{
				for (State dto : stateList)
				{
					stateBuilder.append("<option value='" + dto.getStateName() + "'>" + dto.getStateName() + "</option>");
				}
			}
		}
		return stateBuilder.toString();
	}
	
	@RequestMapping(value = "/fetch/state/by/countryName", method = RequestMethod.GET)
	@ResponseBody
	public String getStateByCountryId(HttpServletRequest request, HttpServletResponse response, @RequestParam String countryName)
	{
		
		StringBuilder stateBuilder = new StringBuilder();
		stateBuilder.append("<option value='0'>--Select State--</option>");
		if (countryName != null && !countryName.equalsIgnoreCase(""))
		{
			List<Country> countryList = stateService.getCountry(countryName);
			int countryIds = countryList.get(0).getId();
			List<State> stateList = stateService.getStateByCountryId(countryIds);
			if (stateList != null && stateList.size() > 0)
			{
				for (State dto : stateList)
				{
					stateBuilder.append("<option value='" + dto.getStateName() + "'>" + dto.getStateName() + "</option>");
				}
			}
		}
		return stateBuilder.toString();
		
	}
	
	@RequestMapping(value = "/fetch/state/by/stateIdList", method = RequestMethod.GET)
	@ResponseBody
	public List<State> getStateByIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			int stateIds = Integer.parseInt(stateId);
			List<State> stateList = stateService.getState(stateIds);
			if (stateList != null && stateList.size() > 0)
			{
				return stateList;
			}
		}
		return null;
		
	}
	
 
	
	 
	@RequestMapping(value = "/fetch/countryList", method = RequestMethod.GET)
	@ResponseBody
	public List<Country> getCountryList(HttpServletRequest request, HttpServletResponse response)
	{
		List<Country> countryList = countryService.getContry();
		return countryList;
		
		
	}
	
	

	@RequestMapping(value = "/fetch/state/by/countryIdList", method = RequestMethod.POST)
	@ResponseBody
	public List<State> getStateByCountryIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String countryId)
	{
		if (countryId != null && !countryId.equalsIgnoreCase(""))
		{
			int countryIds = Integer.parseInt(countryId);
			List<State> stateList = stateService.getStateByCountryId(countryIds);
			if (stateList != null && stateList.size() > 0)
			{
				return stateList;
			}
		}
		return null;
	}
	
	
	@RequestMapping(value = "/fetch/country", method = RequestMethod.GET)
	@ResponseBody
	public String getCountry(HttpServletRequest request, HttpServletResponse response)
	{
		List<Country> countryList = countryService.getContry();
		StringBuilder countryBuilder = new StringBuilder();
		countryBuilder.append("<option value='0'>--Select Country--</option>");
		for (Country dto : countryList)
		{
		if(dto.getValue().equals("India")){ 
			countryBuilder.append("<option value='" + dto.getValue() + "' selected='selected'>" + dto.getValue() + "</option>");
			}else{
			countryBuilder.append("<option value='" + dto.getValue() + "'>" + dto.getValue() + "</option>");
			}
		}
		return countryBuilder.toString();
		
		
	}
	
	 
	
	
}



/*package com.itech.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itech.model.City;
import com.itech.model.Country;
import com.itech.model.State;
import com.itech.service.CityService;
import com.itech.service.CountryService;
import com.itech.service.StateService;


@Controller
public class CommanController
{
	@Autowired
	private CountryService countryService;
	@Autowired
	private StateService stateService;
	@Autowired
	private CityService cityService;
	
	@RequestMapping(value = "/fetch/city/by/stateid", method = RequestMethod.POST)
	@ResponseBody
	public String getCityByStateId(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		StringBuilder cityBuilder = new StringBuilder();
		cityBuilder.append("<option value='0'>--Select State--</option>");
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			int stateIds = Integer.parseInt(stateId);
			List<City> cityList = cityService.getCityByState(stateIds);
			if (cityList != null && cityList.size() > 0)
			{
				for (City dto : cityList)
				{
					cityBuilder.append("<option value='" + dto.getCity() + "'>" + dto.getCity() + "</option>");
					
				}
				return cityBuilder.toString();
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/fetch/city/by/cityid", method = RequestMethod.POST)
	@ResponseBody
	public String getCityById(HttpServletRequest request, HttpServletResponse response, @RequestParam String cityId)
	{
		StringBuilder cityBuilder = new StringBuilder();
		cityBuilder.append("<option value='0'>--Select State--</option>");
		if (cityId != null && !cityId.equalsIgnoreCase(""))
		{
			int cityIds = Integer.parseInt(cityId);
			List<City> cityList = cityService.getCity(cityIds);
			if (cityList != null && cityList.size() > 0)
			{
				for (City dto : cityList)
				{
					cityBuilder.append("<option value='" + dto.getCity() + "'>" + dto.getCity() + "</option>");
					
				}
				return cityBuilder.toString();
			}
		}
		return null;
	}
	
	
	@RequestMapping(value = "/fetch/city/by/stateIdList", method = RequestMethod.POST)
	@ResponseBody
	public List<City> getCityByStateIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			int stateIds = Integer.parseInt(stateId);
			List<City> cityList = cityService.getCityByState(stateIds);
			if (cityList != null && cityList.size() > 0)
			{
				return cityList;
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/fetch/city/by/cityIdList", method = RequestMethod.POST)
	@ResponseBody
	public List<City> getCityByIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String cityId)
	{
		if (cityId != null && !cityId.equalsIgnoreCase(""))
		{
			int cityIds = Integer.parseInt(cityId);
			List<City> cityList = cityService.getCity(cityIds);
			if (cityList != null && cityList.size() > 0)
			{
				return cityList;
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/fetch/city/by/stateName", method = RequestMethod.GET)
	@ResponseBody
	public String getCityByStateName(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateName)
	{
		StringBuilder cityBuilder = new StringBuilder();
		cityBuilder.append("<option value='0'>--Select State--</option>");
		if (stateName != null && !stateName.equalsIgnoreCase(""))
		{
			List<State> stateList = cityService.getStateName(stateName);
			if (stateList != null && stateList.size() > 0)
			{
				int ii= stateList.get(0).getStateId();
				List<City> cityList = cityService.getCityByState(ii);
				
				if (cityList != null && cityList.size() > 0)
				{
					for (City dto : cityList)
					{
						cityBuilder.append("<option value='" + dto.getCity() + "'>" + dto.getCity() + "</option>");
						
					}
				}
				return cityBuilder.toString();
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/fetch/state/by/stateId", method = RequestMethod.POST)
	@ResponseBody
	public String getStateById(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		StringBuilder stateBuilder = new StringBuilder();
		stateBuilder.append("<option value='0'>--Select State--</option>");
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			// stateService.
			int stateIds = Integer.parseInt(stateId);
			List<State> stateList = stateService.getState(stateIds);
			if (stateList != null && stateList.size() > 0)
			{
				for (State dto : stateList)
				{
					stateBuilder.append("<option value='" + dto.getStateName() + "'>" + dto.getStateName() + "</option>");
				}
			}
		}
		return stateBuilder.toString();
	}
	
	@RequestMapping(value = "/fetch/state/by/countryName", method = RequestMethod.POST)
	@ResponseBody
	public String getStateByCountryId(HttpServletRequest request, HttpServletResponse response, @RequestParam String countryName)
	{
		
		StringBuilder stateBuilder = new StringBuilder();
		stateBuilder.append("<option value='0'>--Select State--</option>");
		if (countryName != null && !countryName.equalsIgnoreCase(""))
		{
			List<Country> countryList = stateService.getCountry(countryName);
			int countryIds = countryList.get(0).getId();
			List<State> stateList = stateService.getStateByCountryId(countryIds);
			if (stateList != null && stateList.size() > 0)
			{
				for (State dto : stateList)
				{
					stateBuilder.append("<option value='" + dto.getStateName() + "'>" + dto.getStateName() + "</option>");
				}
			}
		}
		return stateBuilder.toString();
		
	}
	
	@RequestMapping(value = "/fetch/state/by/stateIdList", method = RequestMethod.POST)
	@ResponseBody
	public List<State> getStateByIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String stateId)
	{
		if (stateId != null && !stateId.equalsIgnoreCase(""))
		{
			int stateIds = Integer.parseInt(stateId);
			List<State> stateList = stateService.getState(stateIds);
			if (stateList != null && stateList.size() > 0)
			{
				return stateList;
			}
		}
		return null;
		
	}
	
	@RequestMapping(value = "/fetch/state/by/countryIdList", method = RequestMethod.POST)
	@ResponseBody
	public List<State> getStateByCountryIdList(HttpServletRequest request, HttpServletResponse response, @RequestParam String countryId)
	{
		if (countryId != null && !countryId.equalsIgnoreCase(""))
		{
			int countryIds = Integer.parseInt(countryId);
			List<State> stateList = stateService.getStateByCountryId(countryIds);
			if (stateList != null && stateList.size() > 0)
			{
				return stateList;
			}
		}
		return null;
	}
	
	
	@RequestMapping(value = "/fetch/country", method = RequestMethod.POST)
	@ResponseBody
	public String getCountry(HttpServletRequest request, HttpServletResponse response)
	{
		List<Country> countryList = countryService.getContry();
		StringBuilder countryBuilder = new StringBuilder();
		countryBuilder.append("<option value='0'>--Select Country--</option>");
		for (Country dto : countryList)
		{
		if(dto.getValue().equals("India")){ 
			countryBuilder.append("<option value='" + dto.getValue() + "' selected='selected'>" + dto.getValue() + "</option>");
			}else{
			countryBuilder.append("<option value='" + dto.getValue() + "'>" + dto.getValue() + "</option>");
			}
		}
		return countryBuilder.toString();
		
		
	}
	
	@RequestMapping(value = "/fetch/countryList", method = RequestMethod.POST)
	@ResponseBody
	public List<Country> getCountryList(HttpServletRequest request, HttpServletResponse response)
	{
		List<Country> countryList = countryService.getContry();
		return countryList;
		
		
	}
	
	
}
*/