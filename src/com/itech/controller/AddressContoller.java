package com.itech.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.Address;
import com.itech.service.AddressService;
@Controller
public class AddressContoller {
	
	@Autowired
	private AddressService addressDetailService;

	@RequestMapping(value = "/openaddress", method = RequestMethod.GET)
	public ModelAndView openAddress(HttpServletRequest request,
			HttpServletResponse response) {
		return new ModelAndView("address");
	}

	@RequestMapping(value = "/proceedAddressSignUp", method = RequestMethod.POST, produces = {
			"application/xml", "application/json" })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public Address proceedAddressSignUp(HttpServletRequest request,
			HttpServletResponse response, @RequestBody Address address) {
		Address d1 = addressDetailService.saveAddressDetails(address);

		return d1;
	}


}
