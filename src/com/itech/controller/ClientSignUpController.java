package com.itech.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.ClientSignUp;
import com.itech.service.ClientSignUpService;
import com.itech.utils.BulkSMSSender;
import com.itech.utils.CodeGenrater;
import com.itech.utils.EmailSender;

@Controller

public class ClientSignUpController {

	@Autowired
	private ClientSignUpService clientSignUpService;

	@RequestMapping(value = "/admin/signup", method = RequestMethod.GET)
	public ModelAndView openAddress(HttpServletRequest request,	HttpServletResponse response) {
		return new ModelAndView("admin-signup");
	}
	
	@RequestMapping(value = "/admin/mailVerification", method = RequestMethod.POST, produces = {"application/xml", "application/json" })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK) 
	public String mailVerification(HttpServletRequest request,	HttpServletResponse response,@RequestBody ClientSignUp dto) {
	HttpSession session=request.getSession();
	String optCode=CodeGenrater.generateRideCode();
	System.out.println(dto.getMobileno());
	BulkSMSSender.sendSMS(dto.getMobileno(), "Your OPT number is :"+optCode);
	//SMSSender.sendSMS(dto.getMobileno(), "Your OPT number is :"+optCode);
	session.setAttribute("OPT", optCode);
	EmailSender mail= new EmailSender("Hi,\n please verified the OPT code. Your OPT number is :"+optCode, "Verified", dto.getEmail());
	mail.start();
	return "";
	}

	
	@RequestMapping(value = "/admin/openClientSignUpPost", method = RequestMethod.POST, produces = {"application/xml", "application/json" })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public ClientSignUp openClientSignUpPost(HttpServletRequest request,HttpServletResponse response, @RequestBody ClientSignUp dto) {
	 ClientSignUp csp = clientSignUpService.saveClientSignUp(dto);
		return csp;
	}

}
