package com.itech.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.ClientSignUp;
import com.itech.model.UserDetails;
import com.itech.service.ClientSignUpService;
import com.itech.service.UserDetailsService;
import com.itech.utils.StrongAESAPI;


@Controller
public class UserDetailController
{
	
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private ClientSignUpService clientSignUpService;
		
	@RequestMapping(value = "/consumer/signin", method = RequestMethod.GET)
	public ModelAndView openSingIn(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("consumer-signin");
	}
	 
	@RequestMapping(value = "/consumer/signup", method = RequestMethod.GET)
	public ModelAndView openSingup(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("signup");
	}

	@RequestMapping(value = "/consumer/signupproceed", method = RequestMethod.POST, produces = {"application/xml", "application/json"})
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public UserDetails proceedUserSignUp(HttpServletRequest request, HttpServletResponse response, @RequestBody UserDetails userDetails)
	{
		String ases="";
		try
		{
			ases=	StrongAESAPI.encrypt(userDetails.getuPassword());
		}
		catch (Exception e)
		{ 
			e.printStackTrace();
		}
		
		userDetails.setuPassword(ases);
		UserDetails d = userDetailsService.saveUserDetails(userDetails);
		
		return d;
	}
	
	
	@RequestMapping(value = "/admin/signupproceed", method = RequestMethod.POST, produces = {"application/xml", "application/json"})
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public ClientSignUp proceedAdminUserSignUp(HttpServletRequest request, HttpServletResponse response, @RequestBody ClientSignUp userDetails)
	{
		String ases="";
		try
		{
			ases=	StrongAESAPI.encrypt(userDetails.getPassword());
			userDetails.setPassword(ases);
			ClientSignUp d = clientSignUpService.saveClientSignUp(userDetails);
			
			return d;
		}
		catch (Exception e)
		{ 
			e.printStackTrace();
			return new ClientSignUp();
		}
		
		
	}
	
	
}
