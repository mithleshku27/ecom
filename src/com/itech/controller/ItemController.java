 /*



package com.itech.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.Cart;
import com.itech.model.ProductDetails;
import com.itech.service.CartService;
import com.itech.service.ProductDetailsService;


@Controller
public class ItemController
{
	@Autowired
	private ProductDetailsService productDetailsService;
	@Autowired
	private CartService cartDetailsService;
	
	@RequestMapping(value = "/admin/itemDetails", method = RequestMethod.GET)
	public ModelAndView itemDetails(HttpServletRequest request, HttpServletResponse response)
	{
		
		
		return new ModelAndView("item");
	}
	
	
	@RequestMapping(value = "/admin/geristeritemDetails", method = RequestMethod.POST)
	public ModelAndView regItemDetails(HttpServletRequest request, HttpServletResponse response, @RequestParam("fileImage") MultipartFile fileImage, @ModelAttribute ProductDetails dto)
	{
		
		if (!fileImage.isEmpty())
		{
			System.out.println(fileImage.getOriginalFilename());
			
			try
			{
				byte[] bytes = fileImage.getBytes();
				dto.setImageFile(bytes);
			}
			catch (IOException e)
			{
				
				e.printStackTrace();
			}
		}
		
		System.out.println(dto.getTotalQuantity());
		ProductDetails dtos = productDetailsService.saveProductDetails(dto);
		request.setAttribute("dtos", dtos);
		return new ModelAndView("item");
	}
	
	@RequestMapping(value = "/admin/getItemDetails", method = RequestMethod.GET)
	public ModelAndView getItemForm(HttpServletRequest request, HttpServletResponse response)
	{
		List<ProductDetails> list = productDetailsService.getProductDetails();
		request.setAttribute("list", list);
		
		return new ModelAndView("viewproductItem");
	}
	
	
	@RequestMapping(value = "/consumer/getItemDetailsWithProductID", method = RequestMethod.GET)
	public ModelAndView getItemDetailsWithProductID(HttpServletRequest request, HttpServletResponse response, @RequestParam("productID") String productID)
	{
		List<ProductDetails> list = productDetailsService.getItemDetailsWithProductID(productID);
		HttpSession session = request.getSession();
		session.setAttribute("list", list);
		// request.setAttribute("list", list);
		List<ProductDetails> alllist = productDetailsService.getProductDetailsByCategory("women");
		// HttpSession session=request.getSession();
		// session.setAttribute("alllist", alllist);
		request.setAttribute("alllist", alllist);
		
		return new ModelAndView("product-view");
	}
	
	@RequestMapping(value = "/consumer/fetchProduct", method = RequestMethod.POST)
	@ResponseBody
	public String addItemDetailsInCart(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam String productName, @RequestParam String productPrice, 
			@RequestParam String productID, @RequestParam String quantity)
	{
		Cart dto = new Cart();
		if (productID != null && productID.equalsIgnoreCase(""))
		{
			dto.setProductID(Integer.parseInt(productID));
		}
		dto.setProductName(productName);
		dto.setQuantity(new Integer(Integer.parseInt(quantity)));
		dto.setUserID(Integer.parseInt(productID));
		
		Cart crt = cartDetailsService.saveCartDetails(dto);
		
		return "success";
	}
	
	@RequestMapping(value = "/consumer/openCart", method = RequestMethod.GET)
	public ModelAndView getItemDetailsInCart(HttpServletRequest request, HttpServletResponse response, @RequestParam("userID") String userID)
	{
		
		 * List<ProductDetails> list =
		 * productDetailsService.getItemDetailsWithProductID(productID);
		 * HttpSession session=request.getSession();
		 * session.setAttribute("list", list); List<ProductDetails> alllist =
		 * productDetailsService.getProductDetailsByCategory("women");
		 * request.setAttribute("alllist", alllist);
		 
		
		return new ModelAndView("cart");
	}
	
	@RequestMapping(value = "/consumer/checkout", method = RequestMethod.GET)
	public ModelAndView getItemDetailsInCheckout(HttpServletRequest request, HttpServletResponse response, @RequestParam("userID") String userID)
	{
		
		return new ModelAndView("checkout");
	}
	

	 * @RequestMapping(value = "/getItemDetailsWithCategory", method = RequestMethod.GET)
	 * public ModelAndView getItemDetailsWithCategory(HttpServletRequest request, HttpServletResponse response, @RequestParam("productCategory") String
	 * productCategory)
	 * {
	 * List<ProductDetails> list = productDetailsService.getProductDetailsByCategory(productCategory);
	 * request.setAttribute("list", list);
	 * 
	 * return new ModelAndView("productView");
	 * }
	 
}*/




package com.itech.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.Cart;
import com.itech.model.CartList;
import com.itech.model.ProductDetails;
import com.itech.model.UserDetails;
import com.itech.service.CartService;
import com.itech.service.ProductDetailsService;


@Controller
public class ItemController
{
	@Autowired
	private ProductDetailsService productDetailsService;
	@Autowired
	private CartService cartDetailsService;
	
	@RequestMapping(value = "/admin/itemDetails", method = RequestMethod.GET)
	public ModelAndView itemDetails(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("item");
	}
	
	
	@RequestMapping(value = "/admin/geristeritemDetails", method = RequestMethod.POST)
	public ModelAndView regItemDetails(HttpServletRequest request, HttpServletResponse response, @RequestParam("fileImage") MultipartFile fileImage, @ModelAttribute ProductDetails dto)
	{
		
		if (!fileImage.isEmpty())
		{
			System.out.println(fileImage.getOriginalFilename());
			
			try
			{
				byte[] bytes = fileImage.getBytes();
				dto.setImageFile(bytes);
			}
			catch (IOException e)
			{
				
				e.printStackTrace();
			}
		}
		
		System.out.println(dto.getTotalQuantity());
		ProductDetails dtos = productDetailsService.saveProductDetails(dto);
		request.setAttribute("dtos", dtos);
		return new ModelAndView("item");
	}
	
	@RequestMapping(value = "/admin/getItemDetails", method = RequestMethod.GET)
	public ModelAndView getItemForm(HttpServletRequest request, HttpServletResponse response)
	{
		List<ProductDetails> list = productDetailsService.getProductDetails();
		request.setAttribute("list", list);
		
		return new ModelAndView("viewproductItem");
	}
	
	
	@RequestMapping(value = "/consumer/getItemDetailsWithProductID", method = RequestMethod.GET)
	public ModelAndView getItemDetailsWithProductID(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam("productID") String productID)
	{
		List<ProductDetails> list = productDetailsService.getItemDetailsWithProductID(productID);
		HttpSession session = request.getSession();
		session.setAttribute("list", list);
		// request.setAttribute("list", list);
		List<ProductDetails> alllist = productDetailsService.getProductDetailsByCategory("women");
		// HttpSession session=request.getSession();
		// session.setAttribute("alllist", alllist);
		request.setAttribute("alllist", alllist);
		
		return new ModelAndView("product-view");
	}
	
	@RequestMapping(value = "/consumer/fetchProduct", method = RequestMethod.POST)
	@ResponseBody
	public  String addItemDetailsInCart(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam String productName, @RequestParam String productPrice, 
			@RequestParam String productID, @RequestParam String quantity, 
			@RequestParam String deliveryDate)
	{
		HttpSession session = request.getSession();
		UserDetails ud = (UserDetails)session.getAttribute("userDetails");
		//if (ud == null) { 	return "failed";	}
		Integer userid=ud.getId();
		Cart dto = new Cart();
		if (productID != null && !productID.equalsIgnoreCase("")) 	
		{	dto.setProductID(Integer.parseInt(productID)); }
		//dto.setProductID(Integer.parseInt(productID)); 
		dto.setProductName(productName);
		dto.setProductPrice(Double.parseDouble(productPrice));
		dto.setQuantity(new Integer(Integer.parseInt(quantity)));
		dto.setUserID(userid);
		dto.setPaidstatus(0);
		dto.setCartDate(new Date());
		dto.setDeliveryDate(new Date()); // Change Using Date Formater.
		
		Cart cartList = cartDetailsService.saveCartDetails(dto);
		List<Cart> crt = cartDetailsService.findCartDetails(cartList);
		CartList crtList= new  CartList();
		for (Cart c : crt){
			crtList.setProductID(c.getProductID());
			crtList.setUserID(c.getUserID());
		}
		
		String totalAddtoCart=""+crt.size();
		session.setAttribute("Cart-Detail", crt);
		//if (crt == null) { 	return "failed";	}
		return totalAddtoCart;
	}
	
	@RequestMapping(value = "/consumer/getCartList.json", method = RequestMethod.POST)
	public @ResponseBody  List<CartList> getCartList(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		UserDetails ud = (UserDetails)session.getAttribute("userDetails");
		//if (ud == null) { 	return "failed";	}
		Integer userid=ud.getId();
		Cart dto = new Cart();
		
		dto.setUserID(userid);
		List<Cart> crt = cartDetailsService.findCartDetails(dto);
		List<CartList> crtList1= new ArrayList<CartList>();
		for (Cart c : crt){
			CartList crtList= new  CartList();
			crtList.setProductID(c.getProductID());
			crtList.setUserID(c.getUserID());
			crtList1.add(crtList);
		}
		
		return crtList1;
	}
	
	
	
	@RequestMapping(value = "/consumer/openCart", method = RequestMethod.GET)
	public ModelAndView getItemDetailsInCart(HttpServletRequest request, 
			HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		UserDetails ud = (UserDetails)session.getAttribute("userDetails");
		if (ud == null) { 	return new ModelAndView("consumer-signin"); 	}
		Cart dto= new Cart();
		dto.setUserID(ud.getId());
		List<Cart> crt = cartDetailsService.findCartDetails(dto);
		request.setAttribute("crtList", crt);
		
		return new ModelAndView("cart");
	}
	
	
}

