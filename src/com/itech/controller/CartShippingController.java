package com.itech.controller;


import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.service.CartShippingService;
import com.itech.service.CartService;
import com.itech.model.CartShipping;
import com.itech.model.Cart;
import com.itech.model.UserDetails;

import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class CartShippingController
{
	
	@Autowired
	private CartShippingService cartShippingService;
	
	@Autowired
	private CartService cartService;
	
	@RequestMapping(value = "/consumer/checkout", method = RequestMethod.GET)
	public ModelAndView proceedCheckoutNow(HttpServletRequest request, HttpServletResponse response, @ModelAttribute CartShipping cartShipping)
	{
		HttpSession session = request.getSession();
		UserDetails ud = (UserDetails)session.getAttribute("userDetails");
		if (ud == null)
		{
			return new ModelAndView("consumer-signin");
		}
		else
		{
			Cart cartDto = new Cart();
			cartDto.setUserID(ud.getId());
			List<Cart> crt = cartService.findCartDetails(cartDto);
			request.setAttribute("list", crt);
			// CartShipping cs= cartShippingService.saveCartShippingDetails(cartShipping);
		}
		return new ModelAndView("checkout");
	}
	
	@RequestMapping(value = "/consumer/proceedCheckoutNow", method = RequestMethod.POST)
	public ModelAndView proceedCheckoutDone(HttpServletRequest request, HttpServletResponse response, @ModelAttribute CartShipping cartShipping, @ModelAttribute Cart cartdto)
	{
		HttpSession session = request.getSession();
		UserDetails ud = (UserDetails)session.getAttribute("userDetails");
		if (ud == null)
		{
			return new ModelAndView("consumer-signin");
		}
		else
		{
			Integer userid = ud.getId();
			String usernmae = ud.getFristName();
			cartShipping.setUserid(userid);
			cartShipping.setUsername(usernmae);
			cartdto.setUserID(userid);
			CartShipping cs = cartShippingService.saveCartShippingDetails(cartShipping, cartdto);
			request.setAttribute("cslist", cs);
			
			List<Cart> crt = cartService.findCartDetails(cartdto);
			request.setAttribute("list", crt);
			
			if (cs == null)
			{
				return new ModelAndView("errorpages");
			}
			// return new ModelAndView("payment-success");
			return new ModelAndView("successpaid");
		}
	}
	
	
	@RequestMapping(value = "/consumer/proceedCartUpdate", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public String proceedCartUpdate(HttpServletRequest request, HttpServletResponse response, @RequestParam String id, @RequestParam String quantity)
	{
		
		Cart cart = new Cart();
		cart.setId(Integer.parseInt(id));
		cart.setQuantity(Integer.parseInt(quantity));
		String status = cartService.findUpdateCartDetails(cart);
		
		// CartShipping cs= cartShippingService.saveCartShippingDetails(cart);
		return "success";// new ModelAndView("checkout");
	}
	
	@RequestMapping(value = "/consumer/proceedCartDelete", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public String proceedCartDelete(HttpServletRequest request, HttpServletResponse response, @RequestParam String id)
	{
		
		Cart cart = new Cart();
		cart.setId(Integer.parseInt(id));
		String status = cartService.DeleteCartDetails(cart);
		
		// CartShipping cs= cartShippingService.saveCartShippingDetails(cart);
		return "success";// new ModelAndView("checkout");
	}
	
	@RequestMapping(value = "/consumer/payment-success", method = RequestMethod.GET)
	public ModelAndView proceedPaymentSuccess(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("payment-success");
	}
	
}
