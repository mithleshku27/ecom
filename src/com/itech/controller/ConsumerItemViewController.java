package com.itech.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.ProductDetails;
import com.itech.service.ProductDetailsService;


@Controller 
public class ConsumerItemViewController
{
	@Autowired
	ProductDetailsService productDetailsService;
	
 
	@RequestMapping(value = "/consumer/orderdetails", method = RequestMethod.GET)
	public ModelAndView orderDetails(HttpServletRequest request, HttpServletResponse response)
	{
		
		
		return new ModelAndView("orderdetails");
	}
	
	@RequestMapping(value = "/consumer/view", method = RequestMethod.GET)
	public ModelAndView getItemDetailsWithCategory(HttpServletRequest request, HttpServletResponse response, @RequestParam("productCategory") String productCategory)
	{
		List<ProductDetails> list = productDetailsService.getProductDetailsByCategory(productCategory);
		request.setAttribute("list", list);
		
		return new ModelAndView("productView");
	}
}
