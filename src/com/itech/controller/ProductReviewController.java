package com.itech.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.ProductReview;
import com.itech.service.ProductReviewService;

@Controller
public class ProductReviewController
{
	@Autowired
	private ProductReviewService productReviewService;

	/*@RequestMapping(value = "/consumer/proceedProductReview", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public String proceedProductReview(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String userID,
			@RequestParam String productName, @RequestParam String userName, 
			@RequestParam String emailID, @RequestParam String reviewMessage, 
			@RequestParam String  productID) {
		
		ProductReview pr= new ProductReview();
		pr.setUserID(userID);
		pr.setUserName(userName);
		pr.setProductID(productID);
		pr.setProductName(productName);
		pr.setEmailID(emailID);
		pr.setReviewMessage(reviewMessage);
		pr.setReviewDate(new Date());

		ProductReview d1 = productReviewService.saveProductReviewDetails(pr);
		return "success";
	}*/
	
	@RequestMapping(value = "/consumer/proceedProductReview", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public String proceedProductReview(HttpServletRequest request,
			HttpServletResponse response, @RequestBody ProductReview productReview ) {
		
		productReview.setReviewDate(new Date());
		ProductReview d1 = productReviewService.saveProductReviewDetails(productReview);
		return "success";
	}
}
