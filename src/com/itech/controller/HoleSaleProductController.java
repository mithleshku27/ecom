package com.itech.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.WholeSaleProductDetails;
import com.itech.model.WholesaleProduct;
import com.itech.service.WholesaleService;


@Controller
@RequestMapping(value = "/admin")
public class HoleSaleProductController
{
	@Autowired
	private WholesaleService wholesaleService;
	
	@RequestMapping(value = "/productHoleSales")
	public ModelAndView openCorporateSale(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("productHoleSales");
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/savewholesale", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public WholesaleProduct saveWholesale(HttpServletRequest request, HttpServletResponse response, @RequestParam String names, @RequestParam String orderDate, @RequestParam String mobile, @RequestParam String email, @RequestParam String country, @RequestParam String state, @RequestParam String city, @RequestParam String address, @RequestParam String pincode, @RequestParam String productName, @RequestParam String productDescription, @RequestParam String manufacturingDate, @RequestParam String productCode, @RequestParam String quantity, @RequestParam String unitPrice, @RequestParam String amount)
	{
		
		WholesaleProduct dto = new WholesaleProduct();
		dto.setAddress(address);
		dto.setCountry(country);
		dto.setEmail(email);
		dto.setMobile(mobile);
		dto.setNames(names);
		dto.setPincode(pincode);
		
		String[] manufacturingDates = manufacturingDate.split(",");
		String[] productCodes = productCode.split(",");
		String[] productDescriptions = productDescription.split(",");
		String[] productNames = productName.split(",");
		String[] quantitys = quantity.split(",");
		String[] amounts = amount.split(",");
		String[] unitPrices = unitPrice.split(",");
		List<WholeSaleProductDetails> whsList = new ArrayList<WholeSaleProductDetails>();
		
		for (int i = 0; i < productCodes.length; i++)
		{
			WholeSaleProductDetails whdto = new WholeSaleProductDetails();
			whdto.setManufacturingDate(new Date(manufacturingDates[i]));
			whdto.setProductCode(productCodes[i]);
			whdto.setProductDescription(productDescriptions[i]);
			whdto.setProductName(productNames[i]);
			if (quantitys[i] != "" && quantitys[i] != "")
			{
				whdto.setQuantity(Double.parseDouble(quantitys[i]));
			}
			if (unitPrices[i] != "" && unitPrices[i] != "")
			{
				whdto.setUnitPrice(Double.parseDouble(unitPrices[i]));
			}
			if (amounts[i] != "" && amounts[i] != "0")
			{
				whdto.setAmount(Double.parseDouble(amounts[i]));
				
			}
			whsList.add(whdto);
		}
		System.out.println(dto.getAddress());
		dto.setWholeSaleProductDetails(whsList);
		WholesaleProduct d = wholesaleService.saveWholesale(dto);
		
		return d;
	}
}
