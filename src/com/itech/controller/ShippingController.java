package com.itech.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.itech.model.Shipping;
import com.itech.service.ShippingService;

@Controller
public class ShippingController
{
	@Autowired
	ShippingService shpngService;
	
	@RequestMapping(value = "/admin/shipngdetails", method = RequestMethod.GET)
	public ModelAndView shipngdetails(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("Shipping_Details");
	}
	
	
	
	
	@RequestMapping(value = "/admin/saveshippingDetails", method = RequestMethod.POST)
	public ModelAndView saveshippingDetails(HttpServletRequest request,HttpServletResponse response, @ModelAttribute Shipping dto) {
		
		Shipping dtos = shpngService.saveShippingDetails(dto);
		request.setAttribute("dtos", dtos);
		return new ModelAndView("Shipping_Details");
	}
	
}
	
	