package com.itech.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.ClientSignUp;
import com.itech.model.PinCodeModel;
import com.itech.service.PinCodeService;


@Controller
public class PinCodeController
{
	@Autowired
	private PinCodeService pincodeService;
	
	@RequestMapping(value = "/admin/pincodedetail")
	public ModelAndView openPinCodeDetails(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("pincd");
	}
	
	@RequestMapping(value = "/admin/savepindtl", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public ModelAndView savePinDetail(HttpServletRequest request, HttpServletResponse response, @RequestParam String pincode, @RequestParam String avalableDeliveryBoy, @RequestParam String deliveryMobile, @RequestParam String availableBakery, @RequestParam String bakeryMobile, @RequestParam String availableFlorist, @RequestParam String floristMobile, @RequestParam String otherLocation, @RequestParam String otherMobile)
	{
		try
		{
			String[] avalableDeliveryBoyVal = avalableDeliveryBoy.split(",");
			String[] deliveryMobileVal = deliveryMobile.split(",");
			String[] availableBakeryVal = availableBakery.split(",");
			String[] bakeryMobileVal = bakeryMobile.split(",");
			String[] availableFloristVal = availableFlorist.split(",");
			String[] floristMobileVal = floristMobile.split(",");
			String[] otherLocationVal = otherLocation.split(",");
			String[] otherMobileVal = otherMobile.split(",");
			String message="";
			
			HttpSession session = request.getSession();
			ClientSignUp dto = (ClientSignUp)session.getAttribute("adminUserDetails");
			if (avalableDeliveryBoyVal != null && avalableDeliveryBoyVal.length > 0)
			{
				for (int i = 0; i < avalableDeliveryBoyVal.length; i++)
				{
					PinCodeModel dtos = new PinCodeModel();
					if (pincode != null && !pincode.equalsIgnoreCase(""))
					{
						int pincods = Integer.parseInt(pincode);
						dtos.setPincode(pincods);
					}
					dtos.setArealoc(avalableDeliveryBoyVal[i]);
					dtos.setMobile(deliveryMobileVal[i]);
					dtos.setStatusAt("Delivery");
					if (dto != null)
					{
						dtos.setCreatedBy("" + dto.getId());
					}
					dtos.setCratedDate(new java.util.Date());
					PinCodeModel	messageDto=	pincodeService.savePinDetail(dtos);
					if(messageDto!=null && messageDto.getPid()!=null)
					{
					message="Succesfully saved!";
					}else{
						message="Fail!";
					}
				}
			}
			if (availableBakeryVal != null && availableBakeryVal.length > 0)
			{
				
				for (int i = 0; i < availableBakeryVal.length; i++)
				{
					PinCodeModel dtos = new PinCodeModel();
					if (pincode != null && !pincode.equalsIgnoreCase(""))
					{
						int pincods = Integer.parseInt(pincode);
						dtos.setPincode(pincods);
					}
					dtos.setArealoc(availableBakeryVal[i]);
					dtos.setMobile(bakeryMobileVal[i]);
					dtos.setStatusAt("Bakery");
					 
					if (dto != null)
					{
						dtos.setCreatedBy("" + dto.getId());
					}
					dtos.setCratedDate(new java.util.Date());
					PinCodeModel	messageDto=	pincodeService.savePinDetail(dtos);
					if(messageDto!=null && messageDto.getPid()!=null)
					{
					message="Succesfully saved!";
					}else{
						message="Fail!";
					}
				}
			}
			if (availableFloristVal != null && availableFloristVal.length > 0)
			{
				
				for (int i = 0; i < availableFloristVal.length; i++)
				{
					PinCodeModel dtos = new PinCodeModel();
					if (pincode != null && !pincode.equalsIgnoreCase(""))
					{
						int pincods = Integer.parseInt(pincode);
						dtos.setPincode(pincods);
					}
					dtos.setArealoc(availableFloristVal[i]);
					dtos.setMobile(floristMobileVal[i]);
					dtos.setStatusAt("Floris");
					if (dto != null)
					{
						dtos.setCreatedBy("" + dto.getId());
					}
					dtos.setCratedDate(new java.util.Date());
					PinCodeModel	messageDto=	pincodeService.savePinDetail(dtos);
					if(messageDto!=null && messageDto.getPid()!=null)
					{
					message="Succesfully saved!";
					}else{
						message="Fail!";
					}
				}
			}
			if (otherLocationVal != null && otherLocationVal.length > 0)
			{
				
				for (int i = 0; i < otherLocationVal.length; i++)
				{
					PinCodeModel dtos = new PinCodeModel();
					if (pincode != null && !pincode.equalsIgnoreCase(""))
					{
						int pincods = Integer.parseInt(pincode);
						dtos.setPincode(pincods);
					}
					dtos.setArealoc(otherLocationVal[i]);
					dtos.setMobile(otherMobileVal[i]);
					dtos.setStatusAt("other");
					if (dto != null)
					{
						dtos.setCreatedBy("" + dto.getId());
					}
					dtos.setCratedDate(new java.util.Date());
					PinCodeModel	messageDto=	pincodeService.savePinDetail(dtos);
					if(messageDto!=null && messageDto.getPid()!=null)
					{
					message="Succesfully saved!";
					}else{
						message="Fail!";
					}
				}
				request.setAttribute("message", message);
			}
			 
		}
		catch (Exception er)
		{
			er.printStackTrace();
		}
		return new ModelAndView("pincd");
	}
}
