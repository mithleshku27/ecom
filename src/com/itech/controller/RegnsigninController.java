package com.itech.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.RegSignin;
import com.itech.service.RegnsigninService;

@Controller
public class RegnsigninController {
	
	@Autowired
	private RegnsigninService RegnsigninDetailService;

	@RequestMapping(value = "/openregnSignin", method = RequestMethod.GET)
	public ModelAndView openRegnsignin(HttpServletRequest request,
			HttpServletResponse response) {
		return new ModelAndView("regnsignin");
	}

	@RequestMapping(value = "/proceedRegnsigninSignUp", method = RequestMethod.POST)

	public RegSignin proceedRegnsigninSignUp(HttpServletRequest request,HttpServletResponse response, @RequestBody RegSignin regnsignin) {
		RegSignin d1 = RegnsigninDetailService.saveRegnsigninDetails(regnsignin);
		return d1;
	}

}
