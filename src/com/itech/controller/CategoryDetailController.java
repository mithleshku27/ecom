package com.itech.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.CategoryDetails;
import com.itech.service.CategoryDetailsService;


@Controller
@RequestMapping(value = "/admin")
public class CategoryDetailController
{
	@Autowired
	private CategoryDetailsService categoryDetailsService;
	
	@RequestMapping(value = "/opencd", method = RequestMethod.GET)
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("productCategoryDetails");
	}
	
	@RequestMapping(value = "/savecd", method = RequestMethod.POST, produces = {"application/xml", "application/json"})
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public CategoryDetails savecategoryDetails(HttpServletRequest request, HttpServletResponse response, @RequestBody CategoryDetails dto)
	{
		CategoryDetails d = categoryDetailsService.saveCategoryDetails(dto);
		
		return d;
	}
	
}
