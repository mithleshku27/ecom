package com.itech.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.itech.model.Author;

import com.itech.service.AuthorService;

@Controller
public class AuthorController {
	@Autowired
	private AuthorService authorDetailService;

	@RequestMapping(value = "/openauthor", method = RequestMethod.GET)
	public ModelAndView openAuthor(HttpServletRequest request,HttpServletResponse response) {
		return new ModelAndView("author");
	}


	@RequestMapping(value = "/proceedAuthorSignUp", method = RequestMethod.POST, produces = {
			"application/xml", "application/json" })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public Author proceedUserSignUp(HttpServletRequest request,
			HttpServletResponse response, @RequestBody Author author) {
		Author d1 = authorDetailService.saveAuthorDetails(author);

		return d1;
	}

}
