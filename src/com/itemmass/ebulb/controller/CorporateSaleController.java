package com.itemmass.ebulb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
 
@Controller
public class CorporateSaleController
{
	@RequestMapping(value="/openCorporateSale")
	public ModelAndView openCorporateSale(HttpServletRequest request,HttpServletResponse response){
		return new ModelAndView("corporatesales");
	}
} 
