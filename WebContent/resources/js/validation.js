var specialKeys = new Array();
specialKeys.push(8); //Backspace
function IsNumeric(e) 
		{
		var keyCode = e.which ? e.which : e.keyCode
		var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
		return ret;
		}

function IsAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}


function IsValidEmail(id) {
    var x = id.value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if(x==""){return true;}
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        id.focus();
        id.select();
        return false;
    }
}


function checkdate(input){
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(input))
	alert("Invalid Date Format. Please correct and submit again.");
	else{ //Detailed check for valid date ranges
	var monthfield=input.value.split("/")[1];
	var dayfield=input.value.split("/")[0];
	var yearfield=input.value.split("/")[2];
	var dayobj = new Date(yearfield, monthfield-1, dayfield);
	if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
	alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
	else
	returnval=true;
	}
	if (returnval==false) input.select();
	return returnval;
	}

function isDecimalNumeric(txt, evt) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46) {
        //Check if the text already contains the . character
        if (txt.value.indexOf('.') === -1) {
            return true;
        } else {
            return false;
        }
    } else {
        if (charCode > 31
             && (charCode < 48 || charCode > 57))
            return false;
    }
    return true;
}
