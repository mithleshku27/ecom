<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<title>Welcome Itech Mass Pvt. Ltd.</title>
<head>
<link type="text/css" href="/e-commerce/resources/css/bootstrap.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css"
	rel="stylesheet" media="screen" />
<link href="/e-commerce/resources/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<link href="/e-commerce/resources/jtable/css/jquery.datetimepicker.css"
	rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery.datetimepicker.js"
	type="text/javascript"></script>
<link rel="icon" type="image/png" sizes="96x96"
	href="/e-commerce/resources/images/app-fevicon.png">

<link href="/e-commerce/resources/web/css/style.css" rel='stylesheet'
	type='text/css' />
<link rel="stylesheet"
	href="/e-commerce/resources/web/css/jquery.countdown.css" />
<!--/css-->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300'
	rel='stylesheet' type='text/css'>
<!--fonts-->
<script src="/e-commerce/resources/web/js/jquery-1.11.1.min.js"
	type="text/javascript"></script>
<script src="/e-commerce/resources/web/js/jquery.countdown.js"></script>
<script src="/e-commerce/resources/web/js/script.js"></script>

</head>
<body>
	<div
		style="width: 100%; float: left; margin: auto; position: relative; z-index: 999999;">
		<header id="header" class="gradient4Color"
			style="background-color: #f7f7f7;">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-12">
					<div id="hs_logo">
						<a href="javascript:void(0);"> <img
							src="/e-commerce/resources/images/logo.png" alt="">
						</a>

					</div>
				</div>

				<div class="col-lg-9 col-md-9 col-sm-12">

					<nav>
					<ul class="hs_menu collapse navbar-collapse"
						id="bs-example-navbar-collapse-1" style="margin-top: 6px;">
						<li><a href="/e-commerce/consumer/signin" class="linkmenu">Sign
								In</a></li>
						<li><a href="/e-commerce/consumer/signup" class="linkmenu">Sign	Up</a></li>

					</ul>
					</nav>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="hs_social">
						<ul>
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #314987"><i
									class="fa fa-facebook"></i></a></li>
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #00b2e4"><i
									class="fa fa-twitter"></i></a></li>
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #d01820"><i
									class="fa fa-google-plus"></i></a></li>
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #329edc"><i
									class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 nav_crm">

					<nav id="nav" role="navigation"> <a href="#nav"
						title="Show navigation"><i class="fa fa-bars"></i></a> <a
						href="javascript:void(0);" title="Hide navigation"><i
						class="fa fa-bars"></i></a>

					<ul class="hs_menu " id="bs-example-navbar-collapse-1">
						<li class="glyphicon glyphicon-home red"><a
							href="javascript:void(0);">Home</a></li>


						<li><a href="javascript:void(0);"
							class="drop-arrow glyphicon glyphicon-shopping-cart red">
								Women,Baby & Kids</a>

							<ul>
								<li><a href="javascript:void(0);" class="drop-arrow">Women</a>

									<ul>
										<li><a
											href="/e-commerce/consumer/view?productCategory=women">Dresses</a>
										<li><a href="javascript:void(0);" class="drop-arrow">Top
												& Tees</a></li>

										<li><a href="javascript:void(0);" class="drop-arrow">Saari
										</a></li>




										<li><a href="javascript:void(0);">Sweaters</a></li>
										<li><a href="javascript:void(0);">Fashion Hoodies &
												Sweetshirt</a></li>

										<li><a href="javascript:void(0);">Jeans</a></li>
										<li><a href="javascript:void(0);">pants</a></li>
										<li><a href="javascript:void(0);">skirts </a></li>

										<li><a href="javascript:void(0);">shorts</a></li>
										<li><a href="javascript:void(0);">Leggins</a></li>
										<li><a href="javascript:void(0);">Active</a></li>
										<li><a href="javascript:void(0);">Coats,Jackets &
												Vests </a></li>
										<li><a href="javascript:void(0);">suiting & Blazers</a></li>
										<li><a href="javascript:void(0);">Socks & Hosiery</a></li>

									</ul></li>
								<li><a href="javascript:void(0);"> Baby & kids clothing</a>

									<ul>

										<li><a href="javascript:void(0);">Boys clothing</a></li>


										<li><a href="javascript:void(0);">Girls Clothing</a></li>



										<li><a href="javascript:void(0);">Baby Boys Clothing</a></li>
										<li><a href="javascript:void(0);">Baby Girls Clothing</a></li>

										<li><a href="javascript:void(0);">Combo Pack</a></li>
										<li><a href="javascript:void(0);">Character Clothing</a></li>
									</ul></li>


								<li><a href="javascript:void(0);" class="drop-arrow">Shop
										Clothing by Age</a>

									<ul>
										<li><a href="javascript:void(0);">0-2 Years</a></li>
										<li><a href="javascript:void(0);">2-6 Years</a></li>
										<li><a href="javascript:void(0);">6-10 Years</a></li>
										<li><a href="javascript:void(0);">10+ Years</a></li>
									</ul></li>


								<li><a href="javascript:void(0);" class="drop-arrow">Kids
										and Baby Footwear</a>

									<ul>
										<li><a href="javascript:void(0);">Boys Footwear</a></li>
										<li><a href="javascript:void(0);">Girls Footwear</a></li>
										<li><a href="javascript:void(0);">Baby Footwear</a></li>

									</ul></li>


							</ul></li>

						<li><a href="javascript:void(0);" class="drop-arrow">Lingerie
								& Sleepwear</a>

							<ul>
								<li><a href="javascript:void(0);" class="drop-arrow">MEN</a>

									<ul>

										<li><a href="/e-commerce/booking/consumerBookrideV2V">Underwear</a></li>


										<li><a href="javascript:void(0);" class="drop-arrow">Vests</a></li>



										<li><a href="javascript:void(0);">Sleepwear</a></li>
										<li><a href="javascript:void(0);">Hipsters</a></li>

										<li><a href="javascript:void(0);">Trunks</a></li>
										<li><a href="javascript:void(0);">Boxers</a></li>

										<li><a href="javascript:void(0);">Pyjama</a></li>
										<li><a href="javascript:void(0);">Nightwear Set</a></li>

										<li><a href="javascript:void(0);">Bathrobe</a></li>
										<li><a href="javascript:void(0);">Bermuda</a></li>

										<li><a href="javascript:void(0);">Capri</a></li>

									</ul></li>
								<li><a href="javascript:void(0);" class="drop-arrow">Cloths</a>
									<ul>

										<li><a href="javascript:void(0);">T-Shirt </a></li>
										<li><a href="javascript:void(0);" class="drop-arrow">Shirts</a></li>
										<li><a href="javascript:void(0);">Jeens</a></li>
										<li><a href="javascript:void(0);">Traogar</a></li>
										<li><a href="javascript:void(0);">Sports Bear</a></li>
										<li><a href="javascript:void(0);">Sharee</a></li>

									</ul>
							</ul></li>

 						<li><a href="javascript:void(0);" class="drop-arrow">Flowers</a>
                          	<ul>
								<li><a href="/e-commerce/consumer/view?productCategory=Flower" class="drop-arrow">Roses</a>
									<ul>
										<li><a href="#">Carnations</a></li>
										<li><a href="javascript:void(0);" class="drop-arrow">Gerberas</a></li>
										<li><a href="javascript:void(0);">Lillies and Orchids</a></li>
										<li><a href="javascript:void(0);">Mixed Flowers</a></li>
										<li><a href="javascript:void(0);">Good Luck Plants</a></li>

									</ul></li>
								<li><a href="javascript:void(0);" class="drop-arrow">Combos</a>
									<ul>
										<li><a href="javascript:void(0);">Flowers with Cakes</a></li>
										<li><a href="javascript:void(0);">Flowers with Chocolates</a></li>
										<li><a href="javascript:void(0);">Flowers with Sweets</a></li>
										<li><a href="javascript:void(0);">Flowers with Dryfruits</a></li>
										<li><a href="javascript:void(0);">Flowers with Teddy</a></li>
									</ul>
							</ul></li>
					</ul>

					</nav>
				</div>

			</div>
		</div>
		</header>
	</div>
</head>
<!-- favicon links -->

<script type="text/javascript">
	function bookRequest($scope, $http) {
		$scope.submits = function() {
			var formData = {
				"email" : $scope.email,
				"fristName" : $scope.fristName,
				"mobile" : $scope.mobile,
				"contactDetails" : $scope.contactDetails,
				"users" : $scope.users,
				"uPassword" : $scope.uPassword
			};

			var response = $http.post('/e-commerce/proceed.json', formData);
			response.success(function(data, status, headers, config) {
				$scope.rb = data;
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});
		};

	}
</script>
</head>
<body>

	<div class="clearfix margin_30"></div>
	<div class="clearfix margin_30"></div>
	<div class="clearfix margin_30"></div>
	<div class="clearfix margin_30"></div>
	<div class="content">
		<h2>Something New is Coming Soon</h2>
		<h3>We are currently building a new site will be ready soon.</h3>
		<div class="timer_wrap">
			<div id="counter"></div>
		</div>
	</div>
	<!--/content-->
	<div class="clearfix margin_30"></div>
	<div class="clearfix margin_30"></div>


	<footer>
	<div class="clearfix margin_10"></div>
	<div class="hs_copyright">Copyright &#169; 2016 ITECH MASS PVT.
		LTD. All Rights Reserved.</div>
	</footer>


</body>
</html>