<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<link href="/e-commerce/resources/jtable/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery-1.8.2.js" type="text/javascript"></script>
<script src="/e-commerce/resources/jtable/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script> 
<link href="/e-commerce/resources/jtable/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery.datetimepicker.js" type="text/javascript"></script> 
</head>
<body>
	<div class="container">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form class="form-horizontal"
					action="/e-commerce/geristeritemDetails" method="post"
					enctype="multipart/form-data">
					<fieldset class="borderManager table-responsive">
						<legend class="borderManager" style="font-size: 24px;">
							Hole Sale <span style="color: green;"></span>
						</legend>
						<div class="clearfix margin_10"></div>


						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-user red" style="width: 40px"></i>
										<input type="text" id="names" mobile="names"
										class="form-control input-ml" style="width: 90%;"
										placeholder="Name" required="required" /></span>
								</div>
							</div>  
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-calendar red" style="width: 40px"></i>
										<input type="text" id="orderDate" name="orderDate"
										class="form-control input-ml" style="width: 90%;"
										placeholder="Order Date" required="required" /></span>
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-earphone red" style="width: 40px"></i>
										<input type="text" id="mobile" name="mobile"
										class="form-control input-ml" style="width: 90%;"
										placeholder="Mobile" required="required" /></span>
								</div> 
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-envelope red" style="width: 40px"></i>
										<input type="text" id="email" name="email"
										class="form-control input-ml" style="width: 90%;"
										placeholder="E-Mail" required="required" /></span>
								</div>
							</div>

						</div>


						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-globe red" style="width: 40px"></i>
										<select id="country" name="country"
										class="form-control input-ml" style="width: 90%;"
										required="required">
											<option>Select Country</option>
									</select></span>
								</div>
							</div> 
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-globe red" style="width: 40px"></i>
										<select id="state" name="state" class="form-control input-ml"
										style="width: 90%;" required="required">
											<option>Select State</option>
									</select></span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-globe red" style="width: 40px"></i>
										<select id="city" name="city" class="form-control input-ml"
										style="width: 90%;" required="required">
											<option value="Noida">Noida</option>
											<option value="Ghaziabad">Ghaziabad</option>
									</select> </span>
								</div>
							</div>

						</div>
 

						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-home red" style="width: 40px"></i>
										<textarea id="address" name="address"
											class="form-control input-ml" style="width: 91%;"
											placeholder="Address" required="required"></textarea></span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-map-marker red" style="width: 40px"></i>
										<input type="text" id="pincode" name="pincode"
										class="form-control input-ml" style="width: 90%;"
										placeholder="Pin Code" required="required" /></span>
								</div>
							</div>
						</div>  
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 ">
								<table style="width: 100%;" id="addPdTable"
									class="col-lg-12 col-md-12 col-sm-12" style="text-align: left;">
									<tbody id="addProductTable">
										<tr>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 20px">SN.</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Product Name/Description</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Manufacuring Date</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">Product Code</i></span></th>
													<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Quantity</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Unit Price</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Total Amount</i></span></th>
										</tr>
										<tr>
											<td>1.</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<select name="productName0" id="productName0"
														class="col-lg-3 col-md-3 col-sm-3"
														style="height: 30px; border: none; padding: 1px;">
															<option value="">--Select--</option> 
															<option value="OtherDescription">Other	Description</option>
													</select> <input type="text" name="productDescription0" id="productDescription0"
														onkeyup="isNumberKey(event,'amount0');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 70%; padding: 0px; border: none;"
														placeholder="Product Description ">
													</span>

												</div>

											</td>


											<td>
													<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
													 <input type="text" name="manufacturingDate0" id="manufacturingDate0" class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20" style="width: 100%; padding: 0px; border: none;" placeholder="Manufacturing Date" requered>
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
													 <input type="text" name="productCode0" id="productCode0" 
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Product Code">
													</span>

												</div>
											</td>
                                            <td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
													 <input type="text" name="quantity0" id="quantity0"
														onkeyup="isNumberKey(event,'quantity0');"	class="form-control input-sm col-lg-6 col-md-6 col-sm-6" maxlength="10"	style="width: 100%; padding: 0px; border: none;" placeholder="Quantity">
													</span>

												</div>

											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
													 <input type="text" name="unitPrice0" id="unitPrice0"
														onkeyup="isNumberKey(event,'unitPrice0');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Unit Price">
													</span>

												</div>

											</td>

											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
													 <input type="text" name="amount0" id="amount0"
														onkeyup="isNumberKey(event,'amount0');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Total Amount">
													</span>

												</div>

											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;">
										<input type="button" value="+"	onclick="addProductRow();" class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-plus" /> 
										<input type="button" value="-"  onclick="removToAddproductRow();" class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-minus" />
										<button type="submit" class="btn btn-danger btn-sm btn_nav1" id="linkLogin" value="add"><img alt="" src="/e-commerce/resources/images/save.jpg" width="30px" height="30px"></button>

									</span>
								</div>
							</div>

						</div>


						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
					</fieldset>
				</form>


			</div>
		</div>
	</div>
	
	<script type="text/javascript">
	

	function addProductRow(){
	 var i= $('#addProductTable').children().length;
	 i; 
	    var data="<tr><td>"+i;
	    data +="</td> <td> <div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'> <select name='productName"+i+"' id='productName"+i+"' class='col-lg-3 col-md-3 col-sm-3' style='height: 30px; border: none; padding: 1px;'> <option value=''>--Select--</option> <option value='Shirt'>Shirt</option> <option value='T-Shirt'>T-Shirt</option> <option value='Pent'>Pent</option><option value='Tie'>Tie</option><option value='Other'>Other</option> </select> <input type='text' name='productDescription"+i+"' id='productDescription"+i+"'  class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 70%; padding: 0px; border: none;' placeholder='Product Description '> </span> </div></td> <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='manufacturingDate"+i+"' id='manufacturingDate"+i+"' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='20' style='width: 100%; padding: 0px; border: none;' placeholder='Manufacturing Date'> </span> </div> </td> <td> <div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'> <input type='text' name='productCode"+i+"' id='productCode"+i+"'  class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 100%; padding: 0px; border: none;' placeholder='Product Code'> </span> </div> </td>   <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'><input type='text' name='quantity0' id='quantity0' onkeyup='isNumberKey(event,'quantity0');'	class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width:100%; padding: 0px; border: none;' placeholder='Quantity'> </span> </div></td><td> <div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='unitPrice"+i+"' id='unitPrice"+i+"' onkeyup='isNumberKey(event,'unitPrice"+i+"');' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 100%; padding: 0px; border: none;' placeholder='Unit Price'> </span> </div> </td><td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'> <input type='text' name='amount"+i+"' id='amount0' onkeyup='isNumberKey(event,'amount"+i+"');' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 100%; padding: 0px; border: none;' placeholder='Total Amount'> </span> </div></td> </tr>";
	   	  $('#addPdTable').append(data); 
	}
	
	function removToAddproductRow() {
		var tbl = document.getElementById('addProductTable');
		var lastRow = tbl.rows.length;
		if (lastRow > 1)
			tbl.deleteRow(lastRow - 1);
	}
	
	</script>
</body>
</html>