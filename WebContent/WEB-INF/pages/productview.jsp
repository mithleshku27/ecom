<%@page import="org.apache.catalina.util.Base64" %>
<%@page import="com.itech.model.ProductDetails" %>
<%@page import="java.util.List" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="/e-commerce/resources/productlist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/font-awesome.min.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/prettyPhoto.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/price-range.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/animate.css" rel="stylesheet">
	<link href="/e-commerce/resources/productlist/css/main.css" rel="stylesheet">
	<link href="/e-commerce/resources/productlist/css/responsive.css" rel="stylesheet">
    
      <!-- SINGH -->
    <link type="text/css" href="/e-commerce/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css" rel="stylesheet" media="screen" />
<link href="/e-commerce/resources/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<script src="/e-commerce/resources/js/validation.js"></script>
<link href="/e-commerce/resources/jtable/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery.datetimepicker.js" type="text/javascript"></script>
	
	
	
	
      <!-- SINGH -->
   
    <script src="/e-commerce/resources/js/jsp-script/checkout.js"></script>     
    <script src="/e-commerce/resources/js/jsp-script/product-review.js"></script>  
    <script src="/e-commerce/resources/js/jsp-script/cart-list.js"></script> 
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    
    <script type="text/javascript" src="/e-commerce/resources/js/jquery.elevateZoom-3.0.8.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function() {
    	CheckCartList('cartSize');
    });
    
    $(function () {
        $("#imgImageZoom").elevateZoom({
            cursor: 'pointer',
            imageCrossfade: true,
            loadingIcon: 'loading.gif'
        });
    });
    
  //*********************************************************
  function GoToProductReview(){
	  var productReview = [ { 
			productName: 'SUJEETK', 
			userName: 'SUJEET',
			emailID: 'SUJEET',
			reviewMessage: 'SUJEET',
			userID: '12345',
			productID: '12345' } ];
	   return  ProductReview();//ProductReview(productReview);
  }
  
  //*********************************************************
  function ErrorMessage(message){
	  $("#divMessage").show(); $('#divMessage').delay(2000).fadeOut('slow'); $("#divMessage").html(message); 
  }
    //*********************************************************
    
    function btnAddToCart(){
   	 	var q=$("#quantity").val();
   	 	if(q=="" || q=="0"){   ErrorMessage("Error: Valid quantity required.");	return false;  	}
   	 	
   		var p=$("#productAvailable").val();
  	 	if(p=="" || p=="N"){  ErrorMessage("Error: Valid quantity required.");		return false; 	}
  	 	
  	 	var d=$("#inputDate").val();
  	 	if(p=="" || p=="N"){  ErrorMessage("Error: Delivery date required.");		return false; 	}
  	 	
   		var sellproductName=$("#hdfProductName").val(); 
  		var sellproductPrice=$("#hdfUnitPrice").val();
  		var sellproductID=$("#hdfProductID").val();
  		var sellquantity=$("#quantity").val();
  		var deliveryDate=$("#inputDate").val();
  		//alert(deliveryDate);
      	 //alert(sellproductName +" "+sellproductPrice+" "+sellproductID+"  "+sellquantity);
    	 $.ajax({
    	      type: "POST",
    	      url: "/e-commerce/consumer/fetchProduct",
    	      data:{ 
    	    	  productName: sellproductName,
    	    	  productPrice: sellproductPrice, 
    	    	  productID: sellproductID, 
    	    	  quantity: sellquantity,
    	    	  deliveryDate: deliveryDate
    	      },
    	      success: function(data) {
    	    	  	/* if(data=="failed"){ 
    	    	  		var url = document.location.origin + "/e-commerce/consumer/signin";
    	    	    	window.location.replace(url);//alert(JSON.stringify(data));
    	    	  	} */
    	    	  	CheckCartList('cartSize');
    	    	  	//$("#cartSize").html("Cart : "+ data);
    	    	  	<%-- var	vv= '<%=session.setAttribute("Cart", "Cart : "+ data+") %>'; --%>
    	    	   
    	  	    	//document.getElementById("cartSize").innerHtml="Cart :"+ data;
    	    	  	$("#divMessage").show();
    	  	    	$('#divMessage').delay(2000).fadeOut('slow');
    	  	    	$("#divMessage").html("Item successfully add in cart.");
    	    	
    	      },
              error: function(data){
            	  $("#divMessage").show();
        	    	$('#divMessage').delay(2000).fadeOut('slow');
        	    	$("#divMessage").html("Technical error. Please contact to administrator.");
                  return false;
              }
    	    });
        return true;
    }
    
    function CheckCartList(id){
    	$.ajax({
    		type: "POST",
  	      	url: "/e-commerce/consumer/getCartList.json",
	      	data:{},
	      	success: function(data) {
	      		/* var l=JSON.stringify(data);
	      		alert(l);
	      		alert(l.length); */
	      		var obj = JSON.parse(JSON.stringify(data));
	      		$("#"+id).html("Cart : "+ obj.length);
	      		//alert(obj.length);
	      	}
    	});
    }
    
    function openCart(){
    	//var url = document.location.origin + "/e-commerce/consumer/getItemDetailsWithCategory?productCategory=women";
    	var url = document.location.origin + "/e-commerce/consumer/openCart";
    	window.location.replace(url);
    }
    
    function btnGoToCheckout(){
    	//var url = document.location.origin + "/e-commerce/consumer/getItemDetailsWithCategory?productCategory=women";
    	var url = document.location.origin + "/e-commerce/consumer/checkout";
    	window.location.replace(url);
    	
      return true;
    }
    
    $(document).ready(function(){
   	 var dateToday = new Date();
   	 var dd = dateToday.getDate();
   	 var mm = dateToday.getMonth()+1; 
   	 var yyyy = dateToday.getFullYear();
   	 var toYears=parseInt(yyyy);
   	 $(function() {
   	 $('#inputDate').datetimepicker({
   	 	 showOn: 'button',
   	 	 buttonImage: "/trux/resources/images/calendar.png",
   	     buttonImageOnly: true, 
   	     timepicker:false,
   	     format:'Y/m/d',
   	 	 dayOfWeekStart : 1,
   	 	 lang:'en',
   	 	 yearRange: '1800:' + toYears + '',
   	 	 startDate:	dateToday  
   	 	 });
   	 });
   }); 
</script>
    
    <style type="text/css">
    .available{ background-color: #556B2F; }
    .not-available{ background-color: #FF0000; }
    </style>
	
</head><!--/head-->

<body onload="fillState();">
		<%
		List<ProductDetails> list= (List<ProductDetails>)session.getAttribute("list");
		List<ProductDetails> alllist= (List<ProductDetails>)request.getAttribute("alllist");
		%>

	
	<section>
		<div class="container">
				<div>
				<div class="col-sm-12" >
				<div class="product-details" style=" margin: 0px; padding: 0px;">
				<span  class="btn btn-fefault cart" id="divMessage" style="width: 100%; display: none; text-align: left;"> Error Message. 	</span>
				</div>
				</div></div>
				
				<div class="col-sm-12 padding-right">
	<%if(list!=null && list.size()>0){  
		int i=1;
		for(ProductDetails dto:list){
			String id="id"+ Integer.toString(i);
			i = i + 1;
			String url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
			String large = "data:image/png;base64," + Base64.encode(dto.getImageFile());
	 %>
					<div class="product-details" ><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<%=url%>" alt="" id="imgImageZoom" data-zoom-image="<%=url%>"/>
								<!-- <h3>ZOOM</h3> -->
							</div>
							 <div id="available-product" class="carousel slide available" data-ride="carousel" 
							 style="border: 1px solid red;  margin-top: 20px; text-align: center; ">
								
								 
								  <h2 id="avail-text" style=" color: white; font-size: 22px; font-family: Segoe UI Symbol;">Select City to check Availablity</h2>
								  <!-- <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a> -->
							</div>

						</div>
						<div class="col-sm-7">
						<div class="product-information"  style=" padding: 30px;"><!--/product-information-->
							<input type="hidden" id="productAvailable" value="N">
								<!-- <img src="/e-commerce/resources/productlist/images/product-details/new.jpg" class="newarrival" alt="" /> -->
								<h2><%=dto.getProductName() %></h2>
								<input type="hidden" id="hdfProductName"  name="hdfProductName" value="<%=dto.getProductName() %>">
								<%-- <p>Product ID: <%= dto.getPrid() %></p> --%>
								<input type="hidden" id="hdfProductID" name="hdfProductID" value="<%= dto.getPrid() %>">
								<div class="media-body">
								<h4 class="media-heading" style="color: orange;">Product Detail : </h4>
								<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<!-- <div class="blog-socials">
									<ul>
										<li><a href=""><i class="fa fa-facebook"></i></a></li>
										<li><a href=""><i class="fa fa-twitter"></i></a></li>
										<li><a href=""><i class="fa fa-dribbble"></i></a></li>
										<li><a href=""><i class="fa fa-google-plus"></i></a></li>
									</ul>
									<a class="btn btn-primary" href="">Other Posts</a>
								</div> -->
								</div>
								<img src="/e-commerce/resources/productlist/images/product-details/rating.png" alt="" />
								<span>
									<span>INR <i class="fa fa-inr" style="font-size: 26px;"></i> &nbsp;<%=dto.getUnitPrice() %></span>
									<input type="hidden" id="hdfUnitPrice" name="hdfUnitPrice" class="form-control input-sm" value="<%=dto.getUnitPrice() %>">
									<label>Quantity:</label>
									<input type="text" value="1" id="quantity"  onKeyPress="return IsNumeric(event);"  name="quantity" type="number" maxlength="2"/>
								</span>
								<span>
									<!-- <label>Quantity:</label>
									<input type="text" value="1" id="quantity"  onKeyPress="return IsNumeric(event);"  
									name="quantity" type="number" maxlength="2"/>
									              -->                   
									 <button type="button" class="btn btn-fefault cart" id="btnAddToCart" onclick="btnAddToCart();">
										<i class="fa fa-shopping-cart"></i>
										Add to cart
									</button> 
									<button type="button" class="btn btn-fefault cart" id="btnGoToCheckout" onclick="btnGoToCheckout();">
										<i class="fa fa-shopping-cart"></i>
										Buy Now
									</button> 
									<button type="button" class="btn btn-fefault cart" id="btnContinueShopping" onclick="openCart();">
										<i class="fa fa-shopping-cart"></i>
										Open Cart
									</button> 
								</span><br/><br/>
								
								  <div class="form-horizontal" >
								  <div class="form-group" style="text-align: left;">
								    <label for="inputPassword" class="col-sm-3 control-label">State :</label>
								    <div class="col-sm-5">
								      <select id="selectedState" name="selectedState" class="form-control input-sm" style="border-radius: 0px;" onchange="return SelectState(this.id, 'selectedCity');">
								      <option value="0" selected="selected">--Select State--</option>
								      </select>	
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputPassword" class="col-sm-3 control-label">Delivery City :</label>
								    <div class="col-sm-5">
								      <select id="selectedCity" name="selectedCity" class="form-control input-sm" style="border-radius: 0px;" onchange="return SelectCity(this.id);">
								      <option value="0" selected="selected">--Select City--</option>
								     
								  		</select>	
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputPassword" class="col-sm-3 control-label">Shipping type :</label>
								    <div class="col-sm-5">
								      <select id="selectedCity" name="selectedShippingType" class="form-control input-sm" style="border-radius: 0px;">
								      <option value="0" selected="selected">Shipping Type</option>
								      <option value="1" >Standard Shipping[Rs.0]</option>
								      <option value="2" >Fix Time[Rs.200]</option>
								      <option value="3" >Midnight Delivery[Rs.250]</option>
								  </select>
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputPassword" class="col-sm-3 control-label">Delivery Date :</label>
								    <div class="col-sm-5">
								      <input type="text" class="form-control input-sm" id="inputDate" name="inputDate" placeholder="Date"  style="border-radius: 0px;">
								  
								    </div>
								  </div>
								   
								  </div>	
								

								
							<!-- 	<p><b>Availability:</b> In Stock</p>
								<p><b>Condition:</b> New</p>
								<p><b>Brand:</b> E-SHOPPER</p>
								<a href=""><img src="/e-commerce/resources/productlist/images/product-details/share.png" class="share img-responsive"  alt="" /></a> -->
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
		
	<%} }%>
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#details" data-toggle="tab">Similar Products</a></li>
								<!-- <li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
								<li><a href="#tag" data-toggle="tab">Tag</a></li> -->
								<li><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade" id="details" >
									<%if(alllist!=null && alllist.size()>0){  
		int i=1;
		for(ProductDetails dto:alllist){
			String id="id"+ Integer.toString(i);
			if(i>4){break;}
			i = i + 1;
			String url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
			String large = "data:image/png;base64," + Base64.encode(dto.getImageFile());
	%>	
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="<%=url%>" alt="" />
												<h2><i class="fa fa-inr" style="font-size: 26px;"></i> &nbsp;<%=dto.getUnitPrice() %></h2>
												<p><%= dto.getProductName() %></p>
												<button type="button" class="btn btn-default add-to-cart" style="background-color: orange;"><i class="fa fa-shopping-cart"></i>Add to cart</button>
											</div>
										</div>
									</div>
								</div>
								<% } } %>
							</div>
							
							<div class="tab-pane fade" id="companyprofile" >
							<%if(alllist!=null && alllist.size()>0){  
								int i=1;
								for(ProductDetails dto:alllist){
									String id="id"+ Integer.toString(i);
									if(i>4){break;}
									i = i + 1;
									String url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
									String large = "data:image/png;base64," + Base64.encode(dto.getImageFile());
							%>		<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="<%=url%>" alt="" />
												<h2><i class="fa fa-inr" style="font-size: 26px;"></i> &nbsp;<%=dto.getUnitPrice() %></h2>
												<p><%= dto.getProductName() %></p>
												<button type="button" class="btn btn-default add-to-cart" style="background-color: orange;"><i class="fa fa-shopping-cart"></i>Add to cart</button>
											</div>
										</div>
									</div>
								</div>
							<% } } %></div>
							
							<div class="tab-pane fade" id="tag" >
							<%if(alllist!=null && alllist.size()>0){  
							int i=1;
							for(ProductDetails dto:alllist){
								String id="id"+ Integer.toString(i);
								if(i>4){break;}
								i = i + 1;
								String url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
								String large = "data:image/png;base64," + Base64.encode(dto.getImageFile());
						%>		<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="<%=url%>" alt="" />
												<h2><i class="fa fa-inr" style="font-size: 26px;"></i> &nbsp;<%=dto.getUnitPrice() %></h2>
												<p><%= dto.getProductName() %></p>
												<button type="button" class="btn btn-default add-to-cart" style="background-color: orange;"><i class="fa fa-shopping-cart"></i>Add to cart</button>
											</div>
										</div>
									</div>
								</div>
							<% } } %></div>
							
							<div class="tab-pane fade active in" id="reviews" >
								<div class="col-sm-12">
									<ul>
										<li><a href=""><i class="fa fa-user"></i>EUGEN</a></li>
										<li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
										<li><a href=""><i class="fa fa-calendar-o"></i>31 DEC 2014</a></li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
									<p><b>Write Your Review</b></p>
									
									<form action="#">
										<span>
											<input type="text" placeholder="Your Name"/>
											<input type="email" placeholder="Email Address"/>
										</span>
										<textarea name="" ></textarea>
										<b>Rating: </b> <img src="/e-commerce/resources/productlist/images/product-details/rating.png" alt="" />
										<button type="button" class="btn btn-default pull-right" onclick="return GoToProductReview()">
											Submit
										</button>
									</form>
								</div>
							</div>
							
						</div>
					</div><!--/category-tab-->
					
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">popular items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
							
								<div class="item active">
								<%if(alllist!=null && alllist.size()>0){  
		int i=1;
		for(ProductDetails dto:alllist){
			String id="id"+ Integer.toString(i);
			if(i>3){break;}
			i = i + 1;
			String url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
			String large = "data:image/png;base64," + Base64.encode(dto.getImageFile());
	%>	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="<%=url%>" alt="" />
													<h2><i class="fa fa-inr" style="font-size: 26px;"></i> &nbsp;<%=dto.getUnitPrice() %></h2>
													<p><%=dto.getProductName() %></p>
													<button type="button" class="btn btn-default add-to-cart" style="background-color: orange;"><i class="fa fa-shopping-cart"></i>Add to cart</button>
												</div>
											</div>
										</div>
									</div>
							<%} }%>		
								</div>
								
								
								<div class="item">
								<%if(alllist!=null && alllist.size()>0){  
		int i=1;
		for(ProductDetails dto:alllist){
			String id="id"+ Integer.toString(i);
			if(i>3){break;}
			i = i + 1;
			String url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
			String large = "data:image/png;base64," + Base64.encode(dto.getImageFile());
	%>	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="<%=url%>" alt="" />
													<h2><i class="fa fa-inr" style="font-size: 26px;"></i> &nbsp;<%=dto.getUnitPrice() %></h2>
													<p><%=dto.getProductName() %></p>
													<button type="button" class="btn btn-default add-to-cart" style="background-color: orange;"><i class="fa fa-shopping-cart"></i>Add to cart</button>
												</div>
											</div>
										</div>
									</div>
									<%} }%>		</div>
								
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div><!--/recommended_items-->
					
				</div>
				<!-- ----------- -->
				
			</div>
		
	</section>
	


	
  	 <!-- <script src="/e-commerce/resources/productlist/js/jquery.js"></script>  -->
	<script src="/e-commerce/resources/productlist/js/bootstrap.min.js"></script>
	<script src="/e-commerce/resources/productlist/js/jquery.scrollUp.min.js"></script>
	<script src="/e-commerce/resources/productlist/js/price-range.js"></script>
    <script src="/e-commerce/resources/productlist/js/jquery.prettyPhoto.js"></script>
    <script src="/e-commerce/resources/productlist/js/main.js"></script> 
    
    
</body>
</html>