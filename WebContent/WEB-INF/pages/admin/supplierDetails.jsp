


<%@page import="org.apache.catalina.util.Base64" %>
<%@page import="com.itech.model.Supplier" %>
<%@page import="java.util.List" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link type="text/css" href="/e-commerce/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css" rel="stylesheet" media="screen" />
<link href="/e-commerce/resources/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<script src="/e-commerce/resources/js/validation.js"></script>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


 <script>
       

              
              $(document).ready(function () {
            	  $('#submit').click(function (e) {
            	            var isValid = true;
            	            $('#company,#jobtitle,#firstName,#lastName,#email,#webPage,#buisnessPhone,#fax,#homePhones,#mobile,#address,#city,#stateprovince,#postalcode,#countryregion,#notes').each(function () {
            	                if ($.trim($(this).val()) == '') {
            	                    isValid = false;
            	                    $(this).css({
            	                        "border": "1px solid red",
            	                        "background": "#FFCECE"
            	                        
            	                        	
            	                   
            	                    });
            	                   
            	                }
            	                else {
            	                    $(this).css({
            	                        "border": "",
            	                        "background": "",
            	                       
            	                
            	                    });
            	                   
            	                }
            	            });
            	            if (isValid == false)
            	                e.preventDefault();
            	            $("#divMessage").show();
            	            $('#divMessage').delay(2000).fadeOut('slow');
            	            $("#divMessage").html("Please fill all fields")

            	        });
            	  
            	  $("#divMessage").show();
  	            $('#divMessage').delay(2000).fadeOut('slow');
  	            $("#divMessage").html("Items added successufully")
            	});         
              

           
              


              
              

       </script>



</head>
<body>



	  
<div class="container">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="product-details" style=" margin: 0px; padding: 0px;">
				<span  class="btn btn-fefault cart" id="divMessage" style="width: 100%; display: none; text-align: left;"> Error Message. 	</span>
				</div>
				<form class="form-horizontal" action="savesupplierDetails" method="post"  id="register-form" novalidate="novalidate">
							
					<fieldset class="borderManager table-responsive">
						<legend class="borderManager"
							style="font-size: 24px; background-color: skyblue; color: white;">
							Supplier Details <span style="color: green;"></span>
						</legend>
						<div class="clearfix margin_10"></div>
		
			


						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 ">


								<table style="width: 100%;class="col-lg-12 col-md-12 col-sm-12" style="text-align:left;">
									
								
										
					    <tr>
								
                        
                    
				                        	<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
														<b>Company:</b>
													<span class="input-group-addon" style="text-align: left;">
												
														<input type="text" name="company" id="company"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Company"
														required>
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<b>Job Title:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="jobtitle" id="jobtitle"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Job Title" required>
													</span>

												</div>
											</td>
											   </tr>
					
							
                                          <tr>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<b>FirstName:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="firstName" id="firstName"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="FirstName" required>
													
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
												<b>LastName:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="lastName" id="lastName"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="LastName" required>
													</span>

												</div>
											</td>
											   </tr>					   
											   
											   
											   
											   
											   
											   
											   
											   
					           <tr>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
												<b>E-mail:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="email" id=email
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="E-mail" required>
													
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
												<b>WebPage:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="webPage" id="webPage"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="WebPage" required>
													</span>

												</div>
											</td>
											   </tr>					   
											      
											   
											
											
											
											
											
											
											
											
																   
					           <tr>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<b>BuisnessPhone:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="buisnessPhone" id=buisnessPhone
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="BuisnessPhone" required>
													
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
												<b>Fax:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="fax" id="fax"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Fax" required>
													</span>

												</div>
											</td>
											   </tr>	
											   
											   			
											
											
											
						     <tr>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<b>HomePhones:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="homePhones" id=homePhones
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="HomePhones" required>
													
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
														<b>MobilePhones:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="mobile" id="mobile" 
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="MobilePhones" required>
													</span>

												</div>
											</td>
											   </tr>	
											   		
											
									<tr>
									<td>
									
									<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
													<b>Address:</b>
														<input type="text" name="address" id="address"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Address" required>
													</span>

												</div>
									
									
									
									</td>
									<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<b>City:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="city" id=city
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="City" required>
													
													</span>

												</div>
											</td>
									</tr>
									
									<tr>
									
									
									
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
														<b>State/Province:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="stateprovince" id="stateprovince"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="State/Province" required>
													</span>

												</div>
											</td>
											
											
											
											
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
												<b>Country/Region:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="countryregion" id="countryregion"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Country/Region" required>
													</span>

												</div>
											</td>
								
									</tr>
									
									
									<tr>
									<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
												<b>Zip/PostalCode:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="postalcode" id="postalcode"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Zip/PostalCode" required>
													
													</span>

												</div>
											</td>
									
									<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
												<b>Notes:</b>
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="notes" id="notes"  
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Notes" required>
													</span>

												</div>
											</td>
									
									
									</tr>
									<tr>
									
									<td colspan="2">
									
							
								<div style="margin-bottom: 6px;">
								<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
									<span class="input-group-addon" style="text-align: left;"><i
										style="width: 200px"></i>
										<input type="submit"  value="Save&New"
										class="btn btn-danger btn-sm btn_nav1" width="70px" id="submit" />
										
									</span>	
									</div>	
								</div>
									
									</td>
									
									
									</tr>
								</table>
							</div>
						</div>


						<div class="clearfix margin_10"></div>
						</fieldset>
						</form>
						</div>
						</div>
						</div>
						
</body>
</html>