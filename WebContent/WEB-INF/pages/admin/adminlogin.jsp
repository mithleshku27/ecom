<%@page import="com.itech.model.UserDetails" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>
<meta charset="ISO-8859-1">
<title></title>

<script src="/e-commerce/resources/js/angular.min.js"></script>
<script type="text/javascript">
	function FormSubmitControllerd($scope, $http) {
		$scope.list = [];
		$scope.headerText = 'MMKS';
		$scope.submit = function() {
			var formData = {
				"name" : $scope.name,
				"location" : $scope.location,
				"phone" : $scope.phone
			};

			var response = $http.post('PostFormData', formData);
			response.success(function(data, status, headers, config) {
				$scope.list.push(data);
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});

			//Empty list data after process
			$scope.list = [];

		};
	}

	function Hello($scope, $http) {
		$http.get('/e-commerce/springcontent.json').success(function(data) {
			$scope.user = data;
		});

		/* $http.get('api/user', {params: {id: '5'}
		}).success(function(data, status, headers, config) {
		// Do something successful.
		}).error(function(data, status, headers, config) {
		// Handle the error
		}); */
	}

	function Hello1($scope, $http) {
		$scope.submit = function() {
			var formData = {
				"name" : $scope.name,
				"location" : $scope.location,
				"phone" : $scope.phone
			};

			var response = $http.post('/e-commerce/getUser1.json', formData);
			response.success(function(data, status, headers, config) {
				$scope.list.push(data);
				$scope.u = data;
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});
			$scope.list = [];
		};

	}
</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form class="form-horizontal" action="/admin/loginProceed" method="post">
					<fieldset class="borderManager table-responsive">
					<legend class="borderManager" style="font-size: 24px;">&nbsp;&nbsp;&nbsp;Sign In  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: green;"></span>
					</legend>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon">
								<i class="glyphicon glyphicon-user red" style="color: blue;width: 10%;"></i></span> 
									<input type="text" id="users" name="users" class="form-control" placeholder="User Name" required>
							</div>
						</div>
						 
						<div class="clearfix"></div>
						<br>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon">
								<i class="glyphicon glyphicon-lock red" style="color: blue;width: 10%;"></i></span>
									<input type="password" name="uPassword" id="uPassword" class="form-control" placeholder="Password" required>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="col-lg-12">
							<button type="submit" class="btn btn-danger btn-lg" id="linkLogin">Login</button>
						</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

 