<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Description</title>
<link type="text/css" href="/e-commerce/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css" rel="stylesheet" media="screen" />
<link href="/e-commerce/resources/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<script src="/e-commerce/resources/js/validation.js"></script>
<link href="/e-commerce/resources/jtable/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery.datetimepicker.js" type="text/javascript"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<script type="text/javascript">

 $(document).ready(function(){
	 var dateToday = new Date();
	 var dd = dateToday.getDate();
	 var mm = dateToday.getMonth()+1; 
	 var yyyy = dateToday.getFullYear();
	 var toYears=parseInt(yyyy);
	 $(function() {
	 $('#shippingdate').datetimepicker({
	 	 showOn: 'button',
	 	 buttonImage: "/trux/resources/images/calendar.png",
	     buttonImageOnly: true, 
	     timepicker:false,
	     format:'Y/m/d',
	 	 dayOfWeekStart : 1,
	 	 lang:'en',
	 	 yearRange: '1800:' + toYears + '',
	 	 startDate:	dateToday  
	 	 });
	 });
}); 

function DateValidation()
{
	var dt= $("#shippingdate").val();
	//alert(dt);
	//alert(checkdate(dt));
	if(checkdate(dt)==false){ $("#divError").show(); }
	
	return checkdate(dt);//false;
	}
	
	
function checkdate(input){
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(input)) { /* alert("Invalid Date Format. Please correct and submit again."); */ }
	else
	{ 
		var dayfield=input.split("/")[0];
		var monthfield=input.split("/")[1];
		var yearfield=input.split("/")[2];
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
		{ /* alert("Invalid Day, Month, or Year range detected. Please correct and submit again."); */ }
		else {  returnval=true;}
	
	}
	/* if (returnval==false)
	{ alert("go3");  }
	alert("go4");
	alert(returnval); */
	return returnval;
	}

</script> 





</head>
<body>
<div>
<div class="clearfix hs_margin_10"></div>
<div class="container">  
		

	<div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12"> 
            <form action="saveshippingDetails" method="post">
 				 
 				    <fieldset class="fieldset2 col-lg-12" id="personal">                                    
                       
                            											                                  
              		
					
		            
       <div class="breadcrumb2 flat" style="font-size:150%;color:Indigo"><b>1.Ship From / To</b></div>
		               <!-- <h1 style="font-size:25;color:Indigo">1.Ship From To</h1> -->
		                 <hr/>
                        <div class="clearfix margin_10"></div>
                        
    		          <div class="breadcrumb2 flat" style="font-size:150%;color:Indigo"><b>From</b></div>
    		 
		                
		                 <hr/>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Country Location</div>
                                
                                 <select id="countrylocationfrm" name="countrylocationfrm"  class="form-control input-sm" style="width:100%;">
                                  <option value="INDIA" selected="selected">INDIA</option>
								  <option value="PAKISTAN">PAKISTAN</option>
								  <option value="USA">USA</option>
								  <option value="SRILANKA">SRILANKA</option>
								  <option value="AUSTRALIA">AUSTRALIA</option>
								</select>
                            </div>
                           
                           
                         <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Postal Code</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="postalcodefrm"  name="postalcodefrm" class="form-control input-sm" style="width:100%;" placeholder="Postal Code"  required="required"/>
                            </div></div>
                             
                        
                        
                        
                         <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">City</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="cityfrm"  name="cityfrm" class="form-control input-sm" style="width:100%;" placeholder="City"  required="required"/>
                            </div></div>
                             
                        </div>
                        
                      
                 
                 
                 <div class="clearfix margin_10"></div>
                  
    		            <div class="breadcrumb2 flat" style="font-size:150%;color:Indigo"><b>To</b></div>
    		 
		                 
		                 <hr/>
                  <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Country Location</div>
                                
                                 <select id="countrylocationto" name="countrylocationto"  class="form-control input-sm" style="width:100%;">
                                  <option value="INDIA" selected="selected">INDIA</option>
								  <option value="PAKISTAN">PAKISTAN</option>
								  <option value="USA">USA</option>
								  <option value="SRILANKA">SRILANKA</option>
								  <option value="AUSTRALIA">AUSTRALIA</option>
								</select>
                            </div>
						
						
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Postal Code</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="postalcodeto"  name="postalcodeto" class="form-control input-sm" style="width:100%;" placeholder="Postal Code"  required="required"/>
                            </div></div>
                             
                        
                        
                        
                         <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">City</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="cityto"  name="cityto" class="form-control input-sm" style="width:100%;" placeholder="City"  required="required"/>
                            </div></div>
                           <div class="col-lg-6 col-md-6 col-sm-12">
                           <div>Please Check on Box incase of Residential Address</div>
                             <div class="clearfix margin_10"></div>
                           
                          <label><input type="checkbox" id="resdentladd" name="resdentladd" value="This is a residential address"> This is a residential address</label> <br>	

                          
                             </div>
                        </div>
                     
                        <div class="clearfix margin_10"></div>
    		          
    		  <div class="breadcrumb2 flat" style="font-size:150%;color:Indigo"><b>Additional Information</b></div>
		              
		                 <hr/>
                        <div class="row">
                        
                        
                        <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">No of Packages</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="nmofpkgs"  name="nmofpkgs" class="form-control input-sm" style="width:100%;" placeholder="Postal Code"  required="required"/>
                                 
                            </div></div>
                        
                      
                        
                        
                        
                        
                           	
                            <div class="col-lg-4 col-md-4 col-sm-12">										                                  
                                <div style="margin-bottom:6px;">Weight</div>
                               
                                 <input  type="text" id="weight"  name="weight" class="form-control input-sm" style="width:100%;" placeholder="Postal Code"  required="required"/>

                                 <select id="unitweight" name="unitweight"  class="form-control input-sm" style="width:100%;">
                                  <option value="1" selected="selected">Kgs</option>
								  <option value="2">g</option>
								 </select>
                            </div>
                       
						
						<!-- <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;"></div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="checkbox" id="ship"  name="ship" class="form-control input-sm" style="width:100%;" placeholder="Ship Using acct No."  required="required">Ship usig a FedEx Account Number

                            </div></div> -->
                            
                          <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">PickUp/DropOff</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                 <select id="pkupdrpoff" name="pkupdrpoff"  class="form-control input-sm" style="width:100%;">
                                  <option value="1" selected="selected">FedEx/Pickup</option>
								  <option value="2">FedEx/DropOff</option>
								 
								</select>
                            </div>   
                            </div>  
                        
                        
                        
                      
                        
                            
                            
                             <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Ship Date</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  id="shippingdate"  name="shippingdate" class="form-control input-sm" style="width:100%;" placeholder="ShippingDate"  required="required"/>

                               
                            </div></div>
                            
                            
                            
                            
                            
                            
                             <div class="row" id="divError" style="display:none;">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div style="margin-bottom:6px;" id="divMessage" style="color: red;">* Invalid Date.</div>
                            </div>
                            </div>
                            
                            
                            
                            <div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
                            
                            <div class="col-lg-3 col-md-3 col-sm-12">
							<div class="col-lg-12">
								<button type="submit"  value="Submit" class="form-control">Get transit time</button>
							</div>
						</div>
                            
                            
                          </div>   
                           
                   
                
                       
                        
                        
                    </fieldset>
                    </form>
                    </div>
                    </div>
                    </div>
                    </div>    
                        
                        
                      
               
		



                  
    		         
</body>


</html>