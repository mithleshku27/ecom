<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
tores Management & Stock Control
Main topics of study:

The Supply Function:

Stores
Responsibilities
Organisation
Position of Stores Within the Purchasing & Supply Organisation
Relationships with Other Departments
Materials Management
Logistics
The Supply Chain Concept
The British Standard Guide to Stock Control
Identification of Materials:

Coding of Materials
Advantages of a Coding System
Code Symbols
Interpretation of Codes
Methods of Coding
Self-Validating Codes
Organising a Materials Vocabulary
Specification
Bar Coding
Variety Reductions
Some Widely Used Coding Systems
Receipt & Inspection:

Expediting
Receipts from Suppliers
Transfers from Other Storehouses
Returns from Production or Other Departments
Scrap Arising
Inspection
Vendor Quality Rating
Marshalling Receipts
Receipt of Capital Items Within the Organisation
Issue & Dispatch:

Authorisation of Issues
Identification of Requirements
Timing of Issues
Methods of Issuing Stores for Internal Use
Dispatch of Goods Outside the Organisation
Records & Systems:

Purpose of Stock Records
Manual Systems
Computerised Systems
Electronic Data Interchange
Current Developments
Materials & Accounting

The Value of Stores in Stock
Basis of Material Costing
Methods of Pricing Material
Arrangement of Stores Accounts
Provisions
Control of Stock by Value
Budgetary Control
Annual Audit
Approach to the Provision of Materials:

Reasons for Holding Stock
Dependent & Independent Demand
Approaches Taken in Production Organisations
Differing Stock Control Needs of Construction, Service & Retail Organisations
The Extent of Stockholdings
Ordering Quantities
Range
Consignment Stocktaking
Stock Control Techniques:

Provisioning
Approaches to Control
Visual Approaches to Control
Programming Deliveries
Ordering Quantities
The Need for Differential Control
ABC Analysis Classification of Stock According to Purpose
Forecasting Demand
The Use of Probability in Inventory Control
The Setting of Recorder Levels
The Provision of Safety Stock
Simulation
Physical Security
Responsibility for Stock
Purpose of Stocktaking
Periodic Stocktaking
Continuous Stocktaking
Stocktaking Procedure
Treatment of Discrepancies
Obsolescence & Redundancy
Stock Checking
Storehouses & Stockyards:

New Stores Buildings
Large Central Storehouses
Storehouses Serving One Factory or Operating Unit
Hiring of Storage Accommodation
Stockyards
Construction of Stockyards
Stockyard Facilities
Buildings & Enclosures Within the Stockyard
Stores Operations:

Security
Knowledge of Materials
Prevention of Deterioration
Storehouse Location Systems
Flow
Departmental Stores
Work-In-Progress Stores
Special Storage Facilities
Centralisation of Storage Central Stores
The Assessment of Stores Efficiency
The Measurement of Stores Efficiency
Redundant Stock
Health & Safety:

European Directives on Health & Safety at Work
Manual Lifting
The Control of Substances Hazardous to Health Regulations
Mechanical Lifting
Fire Precautions
Storage Equipment:

Adjustable Steel Shelving
Bins
Pallets
Racks
Measuring Equipment
Ladders & Steps
Cleaning Equipment
General Tools
Live Storage
Automation of Warehouse Work
Materials Handling:

Benefits of Proper Materials Handling
Manual Handling
Mechanical Handling
Assessment of Handling Problems for Mechanisation
Hand-Operated Equipment
Power-Driven Equipment
The Relationship of Materials Handling to Transport
Procedures Manuals:

The Need for Procedure Manuals
Procedures
Advantages & Disadvantages of a Manual
Preparation of the Manual
Contents of the Manual
Publication & Distribution
Implementation of the Manual
Work Study


obiz
</body>
</html>