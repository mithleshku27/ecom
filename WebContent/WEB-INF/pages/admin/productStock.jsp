<%@page import="com.itech.model.Product"%>
<%@page import="org.apache.catalina.util.Base64"%>
<%@page import="com.itech.model.ProductDetails"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link
	href="/e-commerce/resources/jtable/css/jquery-ui-1.10.3.custom.css"
	rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery-1.8.2.js"
	type="text/javascript"></script>
<script src="/e-commerce/resources/jtable/js/jquery-ui-1.10.3.custom.js"
	type="text/javascript"></script>
<link rel="icon" type="image/png" sizes="96x96"
	href="/e-commerce/resources/images/app-fevicon.png">
</head>
<body>
	<div class="container">
		<%
			Product dtos = (Product) request.getAttribute("dtos");
		%>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form class="form-horizontal"
					action="/e-commerce//admin/savestock" method="post">
					<fieldset class="borderManager table-responsive">
						<legend class="borderManager glyphicon glyphicon-euro red"
							style="font-size: 24px; color: blue;">
							Product Stock Details
							<%
							if (dtos != null) {
						%>
							<%=dtos.getPid()%>
							<%
								}
							%>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: green;"></span>
						</legend>

						<script>
							$(document).ready(function() {
								fillProduct();
							});
						</script>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-gift red" style="color: blue;"></i>
								</span> <select id="productId" name="productId" class="form-control"
									placeholder="Product Name" required>

								</select>
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-gift red" style="color: blue;"></i>
								</span> <input type="text" name="orderInStockQuantity"
									id="orderInStockQuantity" class="form-control"
									placeholder="Order In Stock Quantity" required>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-gift red" style="color: blue;"></i>
								</span> <input type="text" name="unitprice" id="unitprice"
									class="form-control" placeholder="Unit Price" required>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-shopping-cart red" style="color: blue;"></i></span>
								     <textarea rows="" cols="" name="saleUnitPrice"	id="saleUnitPrice" class="form-control"
									placeholder="Sale Unit Price Included with vat/tax" required></textarea>

							</div>
						</div>


						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="col-lg-12">
								<button type="submit" class="btn btn-danger btn-lg"
									id="linkLogin">Add Product</button>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
					</fieldset>
				</form>

				<div class="clearfix"></div>
				<div class="clearfix margin_10"></div>

			</div>
		</div>
	</div>

	<script type="text/javascript">
		function fillProduct() {
			
			$.ajax({
						type : "GET",
						url : "/e-commerce/admin/fetch/productList",
						data : {
						// countryName:SelValue
						},
						success : function(data) {
							document.getElementById("productId").innerHTML = data;
							document.getElementById("productId").value.innerHTML = data;

						}
					});
			return true;
		}
	</script>
</body>
</html>