<%@ page language="java" contentType="text/html; charset=ISO-8859-1"    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<title>Welcome Itech Mass Pvt. Ltd.</title>
<head>
<link type="text/css" href="/e-commerce/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css" rel="stylesheet" media="screen" />
<link href="/e-commerce/resources/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<script src="/e-commerce/resources/js/validation.js"></script>
<link href="/e-commerce/resources/jtable/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery.datetimepicker.js" type="text/javascript"></script> 
<link rel="icon" type="image/png" sizes="96x96" href="/e-commerce/resources/images/app-fevicon.png">
  
 <link href="/e-commerce/resources/admin/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/e-commerce/resources/admin/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="e-commerce Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="/e-commerce/resources/admin/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/admin/js/bootstrap.js"></script>

 <link rel="icon" type="image/png" sizes="96x96" href="/e-commerce/resources/images/app-fevicon.png">
<!---fonts-->
<link href='//fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!---fonts-->
<!--script-->
<link rel="stylesheet" href="/e-commerce/resources/admin/css/swipebox.css">
			<script src="/e-commerce/resources/admin/js/jquery.swipebox.min.js"></script> 
			    <script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
				</script>
<!--script-->
</head> 
	<!---header--->
		<div class="header">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="javascript:void(0);bs-example-navbar-collapse-1" aria-expanded="false">
								<i class="sr-only">Toggle navigation</i>
								<i class="icon-bar"></i>
								<i class="icon-bar"></i>
								<i class="icon-bar"></i>
							</button>				  
							<div class="navbar-brand">
								<h1><a href="javascript:void(0);"> <img src="/e-commerce/resources/images/logo.png" alt=""></a></h1>
							</div>
						</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
							
								<li><a href="/e-commerce/admin/login" class="glyphicon glyphicon-home red">Home</a></li>
								<li><a href="javascript:void(0);">About</a></li>
								<li class="dropdown active">
									<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<i class="caret"></i></a>
										<ul class="dropdown-menu">
											<li><a href="javascript:void(0);">Blog</a></li>
											<li><a href="javascript:void(0);">Pricing</a></li>
											<li><a href="javascript:void(0);">FAQ's</a></li> 
											<li><a href="javascript:void(0);">History</a></li>
											<li><a href="javascript:void(0);">Support</a></li>
											<li><a href="javascript:void(0);">Template setting</a></li>
											<li><a href="javascript:void(0);">Login</a></li>
											<li><a href="javascript:void(0);">Portfolio</a></li>
										</ul>
									</li>
								<li><a href="javascript:void(0);">Product</a></li>
								<li class="dropdown">
									<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">AIMS<i class="caret"></i></a>
									<ul class="dropdown-menu">
										<li><a href="javascript:void(0);">A</a></li>
										<li><a href="javascript:void(0);">B</a></li>
										<li><a href=javascript:void(0);">C</a></li>
										<li><a href="javascript:void(0);">D</a></li>
									</ul>			
								</li>
								 
								<li><a href="javascript:void(0);">Contact</a></li>
							</ul>
									  
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
			</div>
		</div>
<body> 
 				     		
<script type="text/javascript">
	function clientSignUpRequests($scope, $http) {
		$scope.verifiey = function() {
			var formData = { "email": $scope.email ,"mobileno": $scope.mobileno };
			var response = $http.post('/e-commerce/admin/mailVerification.json', formData);
			response.success(function(data, status, headers, config) {
			 $scope.rb=data;
			 alert("success");
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});
 		};
 		
		$scope.submitClients = function() {
			var formData = {  
					"firstname": $scope.fristname,
					"lastname": $scope.lastname,
					"address": $scope.address,
					"city": $scope.city,
					"state": $scope.state,
					"pin": $scope.pin,
					"email": $scope.email,
					"mobileno": $scope.mobileno,
					"password": $scope.password
			 };

			//alert($scope.fristname +"   "+$scope.lastname+"  "+$scope.address+" "+$scope.city+"  "+$scope.state +"  "+ $scope.pin +" "+$scope.email +" "+$scope.mobileno  +""+$scope.password); //formData.mobileno
			//return false;
			var response = $http.post('/e-commerce/admin/signupproceed.json', formData);
			response.success(function(data, status, headers, config) {
			 $scope.rb=data;
			 alert("success");
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});
 		};

	}

	
</script>
<script type="text/javascript">

$(document).ready(function(){
	$("#btnVerify").click(function() {
		$("#divVerify").toggle(250);
	});
});

</script>

</head>
<body>
   
<div class="clearfix hs_margin_10"></div>
<div class="container">  
	<div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12"> 
            <form ng-submit="submitClients()" ng-controller="clientSignUpRequests">
 				    <fieldset class="fieldset2 col-lg-12" id="personal">  
                        <div class="col-lg-12 col-md-12 col-sm-12" style="background-color:#f1f1f1;">
                            <h3>Personal's details</h3> 
                        </div>  
                        <div class="clearfix margin_10"></div> 
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">First Name</div>
                                <input  type="text"  id="firstname" ng-model="firstname"  class="form-control input-sm" style="width:100%;" placeholder="Name"  required="required"/>
                            </div>  
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Last Name</div>
                                <input  type="text" id="lastname"  ng-model="lastname" class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        <div class="row">                                       
                            <div class="col-lg-8 col-md-8 col-sm-12">											                                  
                                <div style="margin-bottom:6px; margin-left:4px;">Address</div>
                                <textarea ng-model="address" id="address" class="form-control input-sm" placeholder="Address" style="margin-left:4px;height:65px;width:100%; resize:none;"  required="required"></textarea>
                            </div>
                        </div>              
                          <div class="clearfix margin_10"></div> 
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">City</div>
                                <input  id="city" ng-model="city"  class="form-control input-sm" style="width:100%;" placeholder="City"  required="required"/>
                            </div>  
                            <div class="col-lg-3 col-md-3 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">State</div>
                                <input  type="text" id="state"  ng-model="state" class="form-control input-sm" style="width:100%;" placeholder="State"  required="required"/>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Pin</div>
                                <input  type="text" id="pin"  ng-model="pin" class="form-control input-sm" style="width:100%;" placeholder="Pin"  
                                required="required"  />
                            </div>
                        </div>   
                        
                        <div class="clearfix margin_10"></div> 
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Email ID</div>
                                <input  id="email" ng-model="email"  class="form-control input-sm" style="width:100%;" placeholder="Email ID"  required="required"/>
                            </div>  
                            <div class="col-lg-3 col-md-3 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Mobile No.</div>
                                <input  type="text" id="mobileno"  ng-model="mobileno" class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div>
                            
                            <div class="col-lg-2 col-md-2 col-sm-12 ">											                                  
                                <div style="margin-bottom:6px;">&nbsp;</div>
                                <input type="button" id="btnVerify" ng-click='verifiey()' class="btn btn-info btn-sm" style="width:100%;"  value="Verify Mobile No." > 
                            </div>
                        </div>
                        <div class="clearfix margin_10"></div> 
                        <div class="row" id="divVerify" style="display: none;">
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                <div>&nbsp;</div>
                                <div style="margin-top:10px;">Verify Mobile No.</div>
                            </div>  
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">&nbsp;</div>
                                <input  type="text" id="otpcode"   class="form-control input-sm" style="width:100%;" placeholder="OTP Code"  required="required"/>
                            </div>
                            
                            <div class="col-lg-2 col-md-2 col-sm-12 ">											                                  
                                <div style="margin-bottom:6px;">&nbsp;</div>
                                <input type="button" id="btnVerifySubmit" class="btn btn-info btn-sm" style="width:60%;"  value="Submit" > 
                            </div>
                        </div>
                            
                          
                        <div class="clearfix margin_10"></div> 
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Password</div>
                                <input   type="password" id="password" ng-model="password"  class="form-control input-sm" style="width:100%;" placeholder="Password"  required="required"/>
                            </div>  
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Retype Password</div>
                                <input  type="password" id="retypepassword"   class="form-control input-sm" style="width:100%;" placeholder="Retype Password"  required="required"/>
                            </div>
                        </div>
                        
                                           
                        <div class="clearfix margin_10"></div>  
                            <div class="clearfix margin_10"></div>   
                            <div class="col-lg-4 col-md-4 col-sm-12">     
                                <div style="margin-top:25px; font-size:14px; text-align:left; margin-left:0px;">
                                 <div class="col-lg-4 col-md-4 col-sm-12">     
                                <div style="margin-top:25px; font-size:14px; text-align:left; margin-left:0px;">
                                    <input type="submit" class="btn btn-danger btn-sm" value="Sing Up">                               
                                </div>
                            </div>
                                                                 
                                </div>
                            </div>
                        </fieldset>
                        
              </form>
 </div>
     
     
     
</div></div>

				<!---footer--->
				 
				<div class="footer-section">
					<div class="container">
						<div class="footer-grids">
							<!-- <div class="col-md-3 footer-grid">
								<h4>flickr widget</h4>
								<div class="footer-top">
									<div class="col-md-4 foot-top">
										<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f2.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f3.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="footer-top second">
									<div class="col-md-4 foot-top">
									<img src="images/f4.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f2.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="clearfix"></div>
								</div> -->
							</div>
							<div class="col-md-3 footer-grid">
								<!-- <h4>tag cloud</h4>
								<ul>
								<li><a href="javascript:void(0);">Premium</a></li>
								<li><a href="javascript:void(0);">Graphic</a></li>
								<li><a href="javascript:void(0);">1170px</a></li>
								<li><a href="javascript:void(0);">Photoshop Freebie</a></li>
								<li><a href="javascript:void(0);">Designer</a></li>
								<li><a href="javascript:void(0);">Themes</a></li>
								<li><a href="javascript:void(0);">thislooksgreat chris</a></li>
								<li><a href="javascript:void(0);">Lovely Area</a></li>
								<li><a href="javascript:void(0);">You might use it...</a></li>
								</ul> -->
							</div>
							<div class="col-md-3 footer-grid">
							<!-- <h4>recent posts</h4>
								<div class="recent-grids">
									<div class="col-md-4 recent-great">
										<img src="images/f4.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-8 recent-great1">
										<a href="javascript:void(0);">This is my lovely headline title for this footer section.</a>
										<span>22 October 2014</span>
									</div> -->
									<div class="clearfix"></div>
								</div>
							<!-- 	<div class="recent-grids">
									<div class="col-md-4 recent-great">
										<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-8 recent-great1">
										<a href="javascript:void(0);">This is my lovely headline title for this footer section.</a>
										<span>22 October 2014</span>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="recent-grids">
									<div class="col-md-4 recent-great">
										<img src="images/f3.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-8 recent-great1">
										<a href="javascript:void(0);">This is my lovely headline title for this footer section.</a>
										<span>22 October 2014</span>
									</div>
									<div class="clearfix"></div>
								</div>
							</div> -->
							<div class="col-md-3 footer-grid">
								<h4>Get in touch</h4>
								<p>E-53/54</p>
								<p>Sector-3,Noida,Uttar Pradesh ,India</p>
								<p>Telephone : +</p>
								<p>Telephone : +</p>
								<p>FAX : + </p>
								<p>E-mail : <a href="itechmasssecommerce@gmail.com"> itechmasssecommerce@gmail.com</a></p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="copy-section">
							<p>&copy; 2016 Itech Mass Pvt. Ltd. All rights reserved | Design by <a href="http://itechmass.com/"> ITECH MASS</a></p>
						</div>
					</div>
				 
			<!---footer--->
</body>
</html>  

