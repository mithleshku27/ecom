<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>allotment</title>

<link
	href="/e-commerce/resources/jtable/css/jquery-ui-1.10.3.custom.css"
	rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery-1.8.2.js"
	type="text/javascript"></script>
<script src="/e-commerce/resources/jtable/js/jquery-ui-1.10.3.custom.js"
	type="text/javascript"></script>

</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form class="form-horizontal" action="/e-commerce/admin/savepindtl"
					method="post">

					<fieldset class="borderManager table-responsive">
						<legend class="borderManager glyphicon glyphicon-euro"	style="font-size: 18px; color: blue;">
							Pin Code Wise Area Allotment Details  <b style="color: green;"> <%String message=(String)request.getAttribute("message"); %> <%=message %></b>
						</legend>
						<div class="clearfix margin_10"></div>

                  <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;">
									<i class="glyphicon glyphicon-map-marker red" style="width: 40px;color: maroon;"></i>
										<input type="text" id="pincode" name="pincode"
										class="form-control input-ml" style="width: 90%;" placeholder="Pin Code" required="required" /></span>
								</div>
							</div>
						</div>

						<div style="color: white; background-color: skyblue;">
							<b>Delivery Boy</b>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 ">


								<table style="width: 100%;" id="addPdTable"
									class="col-lg-12 col-md-12 col-sm-12" style="text-align:left;">
									<tbody id="addProductTable">
										<tr>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 20px;">SN.</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon glyphicon-map-marker red"
													style="width: 40px">Available within location</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">Mobile</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">ADD/REMOVE</i></span></th>

										</tr>
										<tr>


											<td>1.</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="arealoc01" id="arealoc01"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Area Location of Existing Delivery Boy"
														required>
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="mobile01" id="mobile01"  onkeyup="isNumberKey(event,'mobile01');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Mobile">
													</span>

												</div>
											</td>
											<td style="height: 50px;">

												<div class="clearfix"></div>
												<div class="clearfix margin_10"></div>
												<div class="row">
													<div class="col-lg-2 col-md-2 col-sm-12">
														<div style="margin-bottom: 6px; height: 50px;">
														<input type="hidden" name="avalableDeliveryBoy" id="avalableDelivery">
														<input type="hidden" name="deliveryMobile" id="deliveryMobile"> 
															<span class="input-group-addon"
																style="text-align: left; height: 30px;"> <input
																type="button" value="+" onclick="addProductRow();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-plus" />
																<input type="button" value="-"
																onclick="removToAddproductRow();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-minus" />
															</span>
														</div>
													</div>

												</div>


											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>


						<div class="clearfix margin_10"></div>





						<div style="color: white; background-color: skyblue;">
							<b>Bakery</b>
						</div>



						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 ">


								<table style="width: 100%;" id="addPdTableN"
									class="col-lg-12 col-md-12 col-sm-12" style="text-align: left;">
									<tbody id="addProductTableN">
										<tr>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 20px">SN.</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon glyphicon-map-marker red"
													style="width: 40px">Available within location</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">Mobile</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">ADD/REMOVE</i></span></th>

										</tr>
										<tr>


											<td>1.</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="arealoc11" id="arealoc11"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Area Location of Existing Bakery" required>
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="mobile11" id="mobile11" onkeyup="isNumberKey(event,'mobile11');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Mobile">
													</span>

												</div>  
											</td>
											<td style="height: 50px;">

												<div class="clearfix"></div>
												<div class="clearfix margin_10"></div>
												<div class="row">   
													<div class="col-lg-2 col-md-2 col-sm-12">
														<div style="margin-bottom: 6px; height: 50px;">
														<input type="hidden" name="availableBakery" id="availableBakery">
														<input type="hidden" name="bakeryMobile" id="bakeryMobile"> 
															<span class="input-group-addon"
																style="text-align: left; height: 30px;"> <input
																type="button" value="+" onclick="addProductRowN();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-plus" />
																<input type="button" value="-"
																onclick="removToAddproductRowN();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-minus" />
															</span>
														</div>
													</div>

												</div>



 
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>





						<div style="color: white; background-color: skyblue;">
							<b>Florist</b>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 ">


								<table style="width: 100%;" id="addPdTableBF"
									class="col-lg-12 col-md-12 col-sm-12" style="text-align: left;">
									<tbody id="addProductTableBF">
										<tr>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 20px">SN.</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon glyphicon-map-marker red"
													style="width: 40px">Available within location</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">Mobile</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">ADD/REMOVE</i></span></th>

										</tr>
										<tr>


											<td>1.</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="arealoc21" id="arealoc21"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Area Location of Existing Bakery" required>
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="mobile21" id="mobile21" onkeyup="isNumberKey(event,'mobile21');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Mobile">
													</span>

												</div> 						</td>
											<td style="height: 50px;">

												<div class="clearfix"></div>
												<div class="clearfix margin_10"></div>
												<div class="row">
													<div class="col-lg-2 col-md-2 col-sm-12">
														<div style="margin-bottom: 6px; height: 50px;">
														<input type="hidden" name="availableFlorist" id="availableFlorist">
														<input type="hidden" name="floristMobile" id="floristMobile"> 
															<span class="input-group-addon"
																style="text-align: left; height: 30px;"> <input
																type="button" value="+" onclick="addProductRowBF();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-plus" />
																<input type="button" value="-"
																onclick="removToAddproductRowBF();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-minus" />
															</span>
														</div>
													</div>

												</div>



											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>

 
						<div style="color: white; background-color: skyblue;">
							<b>Other</b>
						</div>

 
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 ">


								<table style="width: 100%;" id="addPdTableOther"
									class="col-lg-12 col-md-12 col-sm-12" style="text-align: left;">
									<tbody id="addProductTableOther">
										<tr>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 20px">SN.</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon glyphicon-map-marker red"
													style="width: 40px">Available within location</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">Mobile</i></span></th>

											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">ADD/REMOVE</i></span></th>

										</tr>
										<tr>


											<td>1.</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="arealoc31" id="arealoc31"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Other Area Location" required>
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="mobile31" id="mobile31" onkeyup="isNumberKey(event,'mobile31');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Mobile">
													</span>

												</div>
											</td>
											<td style="height: 50px;">

												<div class="clearfix"></div>
												<div class="clearfix margin_10"></div>
												<div class="row">
													<div class="col-lg-2 col-md-2 col-sm-12">
														<div style="margin-bottom: 6px; height: 50px;">
														<input type="hidden" name="otherLocation" id="otherLocation">
														<input type="hidden" name="otherMobile" id="otherMobile"> 
															<span class="input-group-addon"
																style="text-align: left; height: 30px;"> <input
																type="button" value="+" onclick="addProductRowOther();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-plus" />
																<input type="button" value="-"
																onclick="removToAddproductRowOther();"
																class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-minus" />
															</span>
														</div>
													</div>

												</div>


											</td>
										</tr>


									</tbody>
								</table>
							</div>
						</div>



						<div>
							<input type="submit" name="Continue" value="Submit" onclick="submitvalues();">

						</div>

 
					</fieldset>
				</form>


			</div>
		</div>
	</div>


	<script type="text/javascript">
	
	
	  function isNumberKey(evt,id)
	  { 
	     var charCode = (evt.which) ? evt.which : event.keyCode;
	     if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105))
	     { 
	    alert("Please enter numeric value only !");
	     $("input#"+id).val(""); 
	     $("input#"+id).focus(); 
	      document.getElementById(id).value="";
	      document.getElementById(id).focus();
	       return false;
	     }else{
	       //  document.getElementById('message').innerHTML="";
	         return true;	
	     } 
	  }
	  
		function addProductRow() {
			
			var i = $('#addProductTable').children().length;
			i;
			var data = "<tr><td>" + i;
			data += "</td><td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='arealoc0"+ i+ "' id='arealoc0"+ i+ "' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='20' style='width: 100%; padding: 0px; border: none;' placeholder='Area Location of Existing Delivery Boy'> </span> </div> </td>    <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'><input type='text' name='mobile0"+i+"' id='mobile0"+i+"' onkeyup='isNumberKey(event,'mobile0'"+i+"');'	class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width:100%; padding: 0px; border: none;' placeholder='Mobile'> </span> </div></td></tr>";
			$('#addPdTable').append(data);
		}

		function removToAddproductRow() {
			var tbl = document.getElementById('addProductTable');
			var lastRow = tbl.rows.length;
			if (lastRow > 1)
				tbl.deleteRow(lastRow - 1);
		}

		function addProductRowN() {
			var i = $('#addProductTableN').children().length;
			i;
			var data = "<tr><td>" + i;
			data += "</td><td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='arealoc1"	+ i	+ "' id='arealoc1"+ i+ "' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='20' style='width: 100%; padding: 0px; border: none;' placeholder='Area Location of Existing Bakery'> </span> </div> </td>    <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'><input type='text' name='mobile1"+i+"' id='mobile1"+i+"' onkeyup='isNumberKey(event,'mobile1'"+i+"');'	class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width:100%; padding: 0px; border: none;' placeholder='Mobile'> </span> </div></td></tr>";
			$('#addPdTableN').append(data);
		}

		function removToAddproductRowN() {
			var tbl = document.getElementById('addProductTableN');
			var lastRow = tbl.rows.length;
			if (lastRow > 1)
				tbl.deleteRow(lastRow - 1);
		}

		function addProductRowBF() {
			var i = $('#addProductTableBF').children().length;
			i;
			var data = "<tr><td>" + i;
			data += "</td><td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='arealoc2"+ i+ "' id='arealoc2"+ i+ "' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='20' style='width: 100%; padding: 0px; border: none;' placeholder='Area Location of Existing florist'> </span> </div> </td>    <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'><input type='text' name='mobile2"+i+"' id='mobile2"+i+"' onkeyup='isNumberKey(event,'mobile2'"+i+"');'	class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width:100%; padding: 0px; border: none;' placeholder='Mobile'> </span> </div></td></tr>";
			$('#addPdTableBF').append(data);
		}

		function removToAddproductRowBF() {
			var tbl = document.getElementById('addProductTableBF');
			var lastRow = tbl.rows.length;
			if (lastRow > 1)
				tbl.deleteRow(lastRow - 1);
		}

		function addProductRowOther() {
			var i = $('#addProductTableOther').children().length;
			i;
			var data = "<tr><td>" + i;
			data += "</td><td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='arealoc3"+i+"' id='arealoc3"+ i+ "' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='20' style='width: 100%; padding: 0px; border: none;' placeholder='Other Area Location '> </span> </div> </td>    <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'><input type='text' name='mobile3"+i+"' id='mobile3"+i+"' onkeyup='isNumberKey(event,'mobile3"+i+"');'	class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width:100%; padding: 0px; border: none;' placeholder='Mobile'> </span> </div></td></tr>";
			$('#addPdTableOther').append(data);
		}

		function removToAddproductRowOther() {
			var tbl = document.getElementById('addProductTableOther');
			var lastRow = tbl.rows.length;
			if (lastRow > 1)
				tbl.deleteRow(lastRow - 1);
		}
		
		
		
		
		
		

		 function collectToExpendedValueDelivery() {
				var tbl = document.getElementById('addProductTable');
				var lastRow = tbl.rows.length;
				var iteration = lastRow; 
				var arealoc01Val = new Array();  
				var mobile01Val = new Array(); 
				 
			
				
				var idName = new Array("","#arealoc0","#mobile0");
						 
				var arealoc01 = ""; 
				var mobile01 = ""; 
			
				  
				
			 	for ( var i = 1; i < iteration; i++) {	
			 		var arealoc01Inner = ""; 
					var mobile01Inner = "";  
				 	for ( var id = 1; id < idName.length; id++) {
						
					    if(id==1){ 
					    	arealoc01Inner = $(idName[id] + i).val();
				 	     }
					     if(id==2){
					    	 mobile01Inner =$(idName[id] + i).val();
						 }
					     
					 	if (id == (idName.length)-1) {
					 		arealoc01 = arealoc01Inner; 
					 		mobile01 = mobile01Inner; 
							 
					 	} else {
					 		arealoc01 = arealoc01Inner; 
					 		mobile01 = mobile01Inner; 
						}
					}
				 	arealoc01Val.push(arealoc01); 
				 	mobile01Val.push(mobile01); 
					
			 	} 
			 	 
			 	alert(arealoc01Val   +"  ,"+mobile01Val);
			   document.getElementById("avalableDelivery").value=arealoc01Val;
			   document.getElementById("deliveryMobile").value=mobile01Val; 
			   var arealoc01Val = new Array();  
				var mobile01Val = new Array(); 
			 return  mobile01Val;
				
			}
		 
		 
		 
		 
	//	bakery
	 	 function collectToExpendedValueBakery() {
			
				var tbl = document.getElementById('addProductTableN');
				var lastRow = tbl.rows.length;
				var iteration = lastRow; 
				var arealoc11Val = new Array();  
				var mobile11Val = new Array(); 
			 	
				var idName = new Array("","#arealoc1","#mobile1");
				var arealoc11 = ""; 
				var mobile11 = ""; 
							
			 	for ( var i = 1; i < iteration; i++) {	
			 		var arealoc1Inner = ""; 
					var mobile11Inner = "";  
				 	for ( var id = 1; id < idName.length; id++) {
						
					    if(id==1){ 
					    	arealoc11Inner = $(idName[id] + i).val();
				 	     }
					     if(id==2){
					    	 mobile11Inner =$(idName[id] + i).val();
						 }
					     
					 	if (id == (idName.length)-1) {
					 		arealoc11 = arealoc11Inner; 
					 		mobile11 = mobile11Inner; 
							 
					 	} else {
					 		arealoc11 = arealoc11Inner; 
					 		mobile11 = mobile11Inner; 
						}
					}
				 	arealoc11Val.push(arealoc11); 
				 	mobile11Val.push(mobile11); 
					
			 	} 
			 	 
			 	alert(arealoc11Val   +"  ,"+mobile11Val);
			   document.getElementById("availableBakery").value=arealoc11Val;
			   document.getElementById("bakeryMobile").value=mobile11Val; 
			   var arealoc11Val = new Array();  
				var mobile11Val = new Array(); 
			 return  arealoc11Val;
			}
				
			
		 
		 
		 
		 
		 
		// florist
		 
		 
		 function collectToExpendedValueFlorist() {
				var tbl = document.getElementById('addProductTableBF');
				var lastRow = tbl.rows.length;
				var iteration = lastRow; 
				var arealoc21Val = new Array();  
				var mobile21Val = new Array(); 
				var idName = new Array("","#arealoc2","#mobile2");
						 
				var arealoc21 = ""; 
				var mobile21 = ""; 
			
				  
				
			 	for ( var i = 1; i < iteration; i++) {	
			 		var arealoc21Inner = ""; 
					var mobile21Inner = "";  
				 	for ( var id = 1; id < idName.length; id++) {
						
					    if(id==1){ 
					    	arealoc21Inner = $(idName[id] + i).val();
				 	     }
					     if(id==2){
					    	 mobile21Inner =$(idName[id] + i).val();
						 }
					     
					 	if (id == (idName.length)-1) {
					 		arealoc21 = arealoc21Inner; 
					 		mobile21 = mobile21Inner; 
							 
					 	} else {
					 		arealoc21 = arealoc21Inner; 
					 		mobile21 = mobile21Inner; 
						}
					}
				 	arealoc21Val.push(arealoc21); 
				 	mobile21Val.push(mobile21); 
					
			 	} 
			 	 
			 	alert(arealoc21Val   +"  ,"+mobile21Val);
			   document.getElementById("availableFlorist").value=arealoc21Val;
			   document.getElementById("floristMobile").value=mobile21Val; 
			   var arealoc21Val = new Array();  
				var mobile21Val = new Array(); 
			 return  mobile21Val;
				
			}
		 
		 
		 function collectToExpendedValueOther() {
				var tbl = document.getElementById('addProductTableOther');
				var lastRow = tbl.rows.length;
				var iteration = lastRow; 
				var arealoc31Val = new Array();  
				var mobile31Val = new Array(); 
			 	
				var idName = new Array("","#arealoc3","#mobile3");
						 
				var arealoc31 = ""; 
				var mobile31 = ""; 
			 	
			 	for ( var i = 1; i < iteration; i++) {	
			 		var arealoc31Inner = ""; 
					var mobile31Inner = "";  
				 	for ( var id = 1; id < idName.length; id++) {
						
					    if(id==1){ 
					    	arealoc31Inner = $(idName[id] + i).val();
				 	     }
					     if(id==2){
					    	 mobile31Inner =$(idName[id] + i).val();
						 }
					     
					 	if (id == (idName.length)-1) {
					 		arealoc31 = arealoc31Inner; 
					 		mobile31 = mobile31Inner; 
							 
					 	} else {
					 		arealoc31 = arealoc31Inner; 
					 		mobile31 = mobile31Inner; 
						}
					}
				 	arealoc31Val.push(arealoc31); 
				 	mobile31Val.push(mobile31); 
					
			 	} 
			 	 
			 	alert(arealoc31Val   +"  ,"+mobile31Val);
			   document.getElementById("otherLocation").value=arealoc31Val;
			   document.getElementById("otherMobile").value=mobile31Val; 
			   var arealoc31Val = new Array();  
				var mobile31Val = new Array(); 
			 return  mobile31Val;
				
			}
		 
		 function submitvalues()
		 {
			 collectToExpendedValueDelivery();
			 collectToExpendedValueBakery();
			 collectToExpendedValueFlorist();
			 collectToExpendedValueOther();
		 }
		  
		 
	</script>


</body>
</html>