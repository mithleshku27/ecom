<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  
<title>Itech Mass Pvt. LTd.</title>
<link href="/e-commerce/resources/admin/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/e-commerce/resources/admin/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="e-commerce Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="/e-commerce/resources/admin/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/admin/js/bootstrap.js"></script>

 <link rel="icon" type="image/png" sizes="96x96" href="/e-commerce/resources/images/app-fevicon.png">
<!---fonts-->
<link href='//fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!---fonts-->
<!--script-->
<link rel="stylesheet" href="/e-commerce/resources/admin/css/swipebox.css">
			<script src="/e-commerce/resources/admin/js/jquery.swipebox.min.js"></script> 
			    <script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
				</script>
<!--script-->
</head>
<body>
	<!---header--->
		<div class="header">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="javascript:void(0);bs-example-navbar-collapse-1" aria-expanded="false">
								<i class="sr-only">Toggle navigation</i>
								<i class="icon-bar"></i>
								<i class="icon-bar"></i>
								<i class="icon-bar"></i>
							</button>				  
							<div class="navbar-brand">
								<h1><a href="javascript:void(0);"> <img src="/e-commerce/resources/images/logo.png" alt=""></a></h1>
							</div>
						</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
							
								<li><a href="/e-commerce/admin/login" class="glyphicon glyphicon-home red">Home</a></li>
								<li><a href="javascript:void(0);">About</a></li>
								<li class="dropdown active">
									<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<i class="caret"></i></a>
										<ul class="dropdown-menu">
											<li><a href="javascript:void(0);">Blog</a></li>
											<li><a href="javascript:void(0);">Pricing</a></li>
											<li><a href="javascript:void(0);">FAQ's</a></li> 
											<li><a href="javascript:void(0);">History</a></li>
											<li><a href="javascript:void(0);">Support</a></li>
											<li><a href="javascript:void(0);">Template setting</a></li>
											<li><a href="javascript:void(0);">Login</a></li>
											<li><a href="javascript:void(0);">Portfolio</a></li>
										</ul>
									</li>
								<li><a href="javascript:void(0);">Product</a></li>
								<li class="dropdown">
									<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">AIMS<i class="caret"></i></a>
									<ul class="dropdown-menu">
										<li><a href="javascript:void(0);">A</a></li>
										<li><a href="javascript:void(0);">B</a></li>
										<li><a href=javascript:void(0);">C</a></li>
										<li><a href="javascript:void(0);">D</a></li>
									</ul>			
								</li>
								 
								<li><a href="javascript:void(0);">Contact</a></li>
							</ul>
									  
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
			</div>
		</div>
	<!---header--->
		<!---login--->
			<div class="content">
				<div class="main-1">
					<div class="container">
						<div class="login-page">
							<div class="account_grid">
								<div class="col-md-6 login-left">
									 <h3>new customers</h3>
									 <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
									 <a class="acount-btn" href="/e-commerce/admin/signup">Create an Account</a>
								</div>
								<div class="col-md-6 login-right">
									<h3>registered</h3>
									<p>If you have an account with us, please log in.</p>
									<form  action="/e-commerce/admin/loginProceed" method="post">
									  <div>
							    <span>
								<i class="glyphicon glyphicon-user red" style="color: blue;width: 2%;"></i>User E-mail Address</span>
										<input type="text" name="email" id="email" class="form-control" placeholder="Email Address" required>
									 
									  </div>
									  <div>
									  <span  >
							        	<i class="glyphicon glyphicon-lock red" style="color: blue;width: 2%;"> </i> Password</span>
										  <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
									  
									  </div>
									  <a class="forgot" href="javascript:void(0);">Forgot Your Password?</a>
									  <input type="submit" value="Login">
									</form>
								</div>	
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
<!-- login -->
				<!---footer--->
				 
				<div class="footer-section">
					<div class="container">
						<div class="footer-grids">
							<!-- <div class="col-md-3 footer-grid">
								<h4>flickr widget</h4>
								<div class="footer-top">
									<div class="col-md-4 foot-top">
										<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f2.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f3.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="footer-top second">
									<div class="col-md-4 foot-top">
									<img src="images/f4.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f2.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="clearfix"></div>
								</div> -->
							</div>
							<div class="col-md-3 footer-grid">
								<!-- <h4>tag cloud</h4>
								<ul>
								<li><a href="javascript:void(0);">Premium</a></li>
								<li><a href="javascript:void(0);">Graphic</a></li>
								<li><a href="javascript:void(0);">1170px</a></li>
								<li><a href="javascript:void(0);">Photoshop Freebie</a></li>
								<li><a href="javascript:void(0);">Designer</a></li>
								<li><a href="javascript:void(0);">Themes</a></li>
								<li><a href="javascript:void(0);">thislooksgreat chris</a></li>
								<li><a href="javascript:void(0);">Lovely Area</a></li>
								<li><a href="javascript:void(0);">You might use it...</a></li>
								</ul> -->
							</div>
							<div class="col-md-3 footer-grid">
							<!-- <h4>recent posts</h4>
								<div class="recent-grids">
									<div class="col-md-4 recent-great">
										<img src="images/f4.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-8 recent-great1">
										<a href="javascript:void(0);">This is my lovely headline title for this footer section.</a>
										<span>22 October 2014</span>
									</div> -->
									<div class="clearfix"></div>
								</div>
							<!-- 	<div class="recent-grids">
									<div class="col-md-4 recent-great">
										<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-8 recent-great1">
										<a href="javascript:void(0);">This is my lovely headline title for this footer section.</a>
										<span>22 October 2014</span>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="recent-grids">
									<div class="col-md-4 recent-great">
										<img src="images/f3.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-8 recent-great1">
										<a href="javascript:void(0);">This is my lovely headline title for this footer section.</a>
										<span>22 October 2014</span>
									</div>
									<div class="clearfix"></div>
								</div>
							</div> -->
							<div class="col-md-3 footer-grid">
								<h4>Get in touch</h4>
								<p>E-53/54</p>
								<p>Sector-3,Noida,Uttar Pradesh ,India</p>
								<p>Telephone : +</p>
								<p>Telephone : +</p>
								<p>FAX : + </p>
								<p>E-mail : <a href="itechmasssecommerce@gmail.com"> itechmasssecommerce@gmail.com</a></p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="copy-section">
							<p>&copy; 2016 Itech Mass Pvt. Ltd. All rights reserved | Design by <a href="http://itechmass.com/"> ITECH MASS</a></p>
						</div>
					</div>
				 
			<!---footer--->
</body>
</html>