<%@page import="com.itech.model.Country"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Wholesale</title>
 
<link href="/e-commerce/resources/jtable/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery-1.8.2.js" type="text/javascript"></script>
<script src="/e-commerce/resources/jtable/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
<link rel="icon" type="image/png" sizes="96x96" href="/e-commerce/resources/images/app-fevicon.png">

<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<script src="/e-commerce/resources/js/validation.js"></script>
<script type="text/javascript">
	
	  $(document).ready(function(){
		   	 var dateToday = new Date();
		   	 var dd = dateToday.getDate();
		   	 var mm = dateToday.getMonth()+1; 
		   	 var yyyy = dateToday.getFullYear();
		   	 var toYears=parseInt(yyyy);
		   	 $(function() {
		   	 $('#orderDate').datetimepicker({
		   	 	 showOn: 'button',
		   	 	// buttonImage: "/e-commerce/resources/images/calendar.png",
		   	     buttonImageOnly: true, 
		   	     timepicker:false,
		   	     format:'Y/m/d',
		   	 	 dayOfWeekStart : 1,
		   	 	 lang:'en',
		   	 	 yearRange: '1800:' + toYears + '',
		   	 	 startDate:	dateToday  
		   	 	 });
		   	 });
		   }); 
	</script>
</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">

				<form ng-submit="submits()" ng-controller="addWholeSale">
					<fieldset class="borderManager table-responsive">
						<legend class="borderManager glyphicon glyphicon-euro red"
							style="font-size: 24px; color: blue;">
							Wholesale <span style="color: green;"></span>
						</legend>
						<div class="clearfix margin_10"></div>


						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-user red"
										style="width: 40px; color: blue;"></i> <input type="text"
										id="names" ng-model="names" class="form-control input-ml"
										style="width: 90%;" placeholder="Name" required="required" /></span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-calendar red"
										style="width: 40px; color: green;"></i> <input type="text"
										id="orderDate" ng-model="orderDate"
										class="form-control input-ml" style="width: 90%;"
										placeholder="Order Date" required="required" /></span>
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-earphone red"
										style="width: 40px; color: olive;"></i> <input type="text"
										id="mobile" ng-model="mobile" class="form-control input-ml"
										style="width: 90%;" placeholder="Mobile" required="required" /></span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-envelope red"
										style="width: 40px; color: orange;"></i> <input type="text"
										id="email" ng-model="email" class="form-control input-ml"
										style="width: 90%;" placeholder="E-Mail" required="required" /></span>
								</div>
							</div>

						</div>

                         <script>  
                             $(document).ready(function(){    																		 
                            	 fillCountry();
                            	 fillState();
    						}); 
    				        </script>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-globe red"
										style="width: 40px; color: green;"></i>
										 <select id="country"	ng-model="country" class="form-control input-ml"style="width: 90%;" required="required">
											<option value="">--Select Country--</option>
									</select></span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon  glyphicon-globe red"
										style="width: 40px; color: green;"></i>
										 <select id="state"	ng-model="state" class="form-control input-ml"
										style="width: 90%;" required="required">
											<option value="0">--Select State--</option>
									</select></span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-globe red"
										style="width: 40px; color: green;"></i> <select id="city"
										ng-model="city" class="form-control input-ml"
										style="width: 90%;" required="required">
											<option value="0">-Select City-</option>
											<option value="Noida">Noida</option>
											<option value="Ghaziabad">Ghaziabad</option>
									</select> </span>
								</div>
							</div>

						</div>


						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-home red"
										style="width: 40px; color: green;"></i> <textarea id="address"
											ng-model="address" class="form-control input-ml"
											style="width: 91%;" placeholder="Address" required="required"></textarea></span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div style="margin-bottom: 6px;">
									<span class="input-group-addon" style="text-align: left;"><i
										class="glyphicon glyphicon-map-marker red"
										style="width: 40px; color: purple;"></i> <input type="text"
										id="pincode" ng-model="pincode" class="form-control input-ml"
										style="width: 90%;" placeholder="Pin Code" required="required" /></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 ">
								<table style="width: 100%;" id="addPdTable"
									class="col-lg-12 col-md-12 col-sm-12" style="text-align: left;">
									<tbody id="addProductTable">
										<tr>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 20px">SN.</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Product Name/Description</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Manufacuring Date</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon red"
													style="width: 40px">Product Code</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Quantity</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Unit Price</i></span></th>
											<th><span class="input-group-addon"
												style="text-align: left;"><i class="glyphicon  red"
													style="width: 40px">Total Amount</i></span></th>
										</tr>
										<tr>
											<td>1.</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<select name="productName1" id="productName1"
														class="col-lg-3 col-md-3 col-sm-3"
														style="height: 30px; border: none; padding: 1px;">
															<option value="">--Select--</option>
															<option value="OtherDescription">Other
																Description</option>
													</select> <input type="text" name="productDescription1"
														id="productDescription1"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 70%; padding: 0px; border: none;"
														placeholder="Product Description ">
													</span>

												</div>

											</td>


											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="manufacturingDate1"
														id="manufacturingDate1"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="20"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Manufacturing Date" requered>
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="productCode1" id="productCode1"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Product Code">
													</span>

												</div>
											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="quantity1" id="quantity1"
														onkeyup="isNumberKey(event,'quantity1');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Quantity">
													</span>

												</div>

											</td>
											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="unitPrice1" id="unitPrice1"
														onkeyup="isNumberKey(event,'unitPrice1');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Unit Price">
													</span>

												</div>

											</td>

											<td>
												<div class="col-lg-12 col-md-12 col-sm-12"
													style="border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;">
													<span class="input-group-addon" style="text-align: left;">
														<input type="text" name="amount1" id="amount1"
														onkeyup="isNumberKey(event,'amount1');"
														class="form-control input-sm col-lg-6 col-md-6 col-sm-6"
														maxlength="10"
														style="width: 100%; padding: 0px; border: none;"
														placeholder="Total Amount">
													</span>

												</div>

											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div style="margin-bottom: 6px;">

									<input type="hidden" ng-model="productName" id="productName"
										ng-value="productName"> <input type="hidden"
										ng-model="productDescription" id="productDescription">
									<input type="hidden" ng-model="manufacturingDate"
										id="manufacturingDate"> <input type="hidden"
										ng-model="productCode" id="productCode"> <input
										type="hidden" ng-model="quantity" id="quantity"> <input
										type="hidden" ng-model="unitPrice" id="unitPrice"> <input
										type="hidden" ng-model="amount" id="amount"> <span
										class="input-group-addon" style="text-align: left;"> <input
										type="button" value="+" onclick="addProductRow();"
										class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-plus" />
										<input type="button" value="-"
										onclick="removToAddproductRow();"
										class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-minus" />
										<input type="checkbox"
										class="btn btn-danger btn-sm btn_nav1 glyphicon glyphicon-minus"
										onclick="collectAndSubmit();">
										<button type="submit" class="btn btn-danger btn-sm btn_nav1"
											id="linkLogin">
											<img alt="" src="/e-commerce/resources/images/save.jpg"
												width="30px" height="30px">
										</button>
									</span>
								</div>
							</div>

						</div>


						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
					</fieldset>
				</form>


			</div>
		</div>
	</div>

	<script type="text/javascript">
	

	function addProductRow(){
	 var i= $('#addProductTable').children().length;
	 i; 
	    var data="<tr><td>"+i;
	    data +="</td> <td> <div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'> <select name='productName"+i+"' id='productName"+i+"' class='col-lg-3 col-md-3 col-sm-3' style='height: 30px; border: none; padding: 1px;'> <option value=''>--Select--</option> <option value='Shirt'>Shirt</option> <option value='T-Shirt'>T-Shirt</option> <option value='Pent'>Pent</option><option value='Tie'>Tie</option><option value='Other'>Other</option> </select> <input type='text' name='productDescription"+i+"' id='productDescription"+i+"'  class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 70%; padding: 0px; border: none;' placeholder='Product Description '> </span> </div></td> <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='manufacturingDate"+i+"' id='manufacturingDate"+i+"' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='20' style='width: 100%; padding: 0px; border: none;' placeholder='Manufacturing Date'> </span> </div> </td> <td> <div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'> <input type='text' name='productCode"+i+"' id='productCode"+i+"'  class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 100%; padding: 0px; border: none;' placeholder='Product Code'> </span> </div> </td>   <td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'><input type='text' name='quantity"+i+"' id='quantity"+i+"' onkeyup='isNumberKey(event,'quantity'"+i+"');'	class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width:100%; padding: 0px; border: none;' placeholder='Quantity'> </span> </div></td><td> <div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'>  <input type='text' name='unitPrice"+i+"' id='unitPrice"+i+"' onkeyup='isNumberKey(event,'unitPrice"+i+"');' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 100%; padding: 0px; border: none;' placeholder='Unit Price'> </span> </div> </td><td><div class='col-lg-12 col-md-12 col-sm-12' style='border: 1px solid #999; border-radius: 4px; 0 px 12px 0 0; padding: 0;'> <span class='input-group-addon' style='text-align: left;'> <input type='text' name='amount"+i+"' id='amount"+i+"' onkeyup='isNumberKey(event,'amount"+i+"');' class='form-control input-sm col-lg-6 col-md-6 col-sm-6' maxlength='10' style='width: 100%; padding: 0px; border: none;' placeholder='Total Amount'> </span> </div></td> </tr>";
	   	  $('#addPdTable').append(data); 
	}
	
	function removToAddproductRow() {
		var tbl = document.getElementById('addProductTable');
		var lastRow = tbl.rows.length;
		if (lastRow > 1)
			tbl.deleteRow(lastRow - 1);
	}
	
	
	

	 function collectToExpendedValue() {
			var tbl = document.getElementById('addProductTable');
			var lastRow = tbl.rows.length;
			var iteration = lastRow; 
			var productNameVal = new Array();  
			var productDescriptionVal = new Array(); 
			var manufacturingDateVal = new Array(); 
			var productCodeVal = new Array(); 
			var quantityVal = new Array();  
			var unitPriceVal = new Array(); 
			var amountVal = new Array(); 
			
			var idName = new Array("","#productName","#productDescription","#manufacturingDate","#productCode","#quantity","#unitPrice","#amount");
					 
			var productName = ""; 
			var productDescription = ""; 
			var manufacturingDate = "";  
			var productCode = ""; 
			var quantity = "";  
			var unitPrice = ""; 
			var amount = "";  
			
		 	for ( var i = 1; i < iteration; i++) {	
		 		var productNameInner = ""; 
				var productDescriptionInner = ""; 
				var manufacturingDateInner = "";  
				var productCodeInner = ""; 
				var quantityInner = "";  
				var unitPriceInner = ""; 
				var amountInner = ""; 
			 	for ( var id = 1; id < idName.length; id++) {
					
				    if(id==1){ 
				    	productNameInner = $(idName[id] + i).val();
			 	     }
				     if(id==2){
				    	 productDescriptionInner =$(idName[id] + i).val();
					 }
				     if(id==3){
				    	 manufacturingDateInner = $(idName[id] + i).val();
					 }
				     if(id==4){
				    	 productCodeInner = $(idName[id] + i).val();
					 }
				     if(id==5){
				    	 quantityInner = $(idName[id] + i).val();
					 }
				     if(id==6){
				    	 unitPriceInner = $(idName[id] + i).val();
					 }
				     if(id==7){
				    	 amountInner = $(idName[id] + i).val();
					 }
				 	if (id == (idName.length)-1) {
				 		  productName = productNameInner; 
						 productDescription = productDescriptionInner; 
						 manufacturingDate = manufacturingDateInner;  
						 productCode = productCodeInner; 
						 quantity = quantityInner;  
						 unitPrice = unitPriceInner; 
						 amount = amountInner; 
				 	} else {
				 		  productName = productNameInner; 
							 productDescription = productDescriptionInner; 
							 manufacturingDate = manufacturingDateInner;  
							 productCode = productCodeInner; 
							 quantity = quantityInner;  
							 unitPrice = unitPriceInner; 
							 amount = amountInner; 
					}
				}
			 	 productNameVal.push(productName); 
				 productDescriptionVal.push(productDescription);
				 manufacturingDateVal.push(manufacturingDate); 
				 productCodeVal.push(productCode);
				 quantityVal.push(quantity); 
				 unitPriceVal.push(unitPrice);
				 amountVal.push(amount);
				
		 	} 
		 	 
		   document.getElementById("productName").value=productNameVal;
		   document.getElementById("productDescription").value=productDescriptionVal;
		   document.getElementById("manufacturingDate").value=manufacturingDateVal;
		   document.getElementById("productCode").value=productCodeVal;
		   document.getElementById("quantity").value=quantityVal;
		   document.getElementById("unitPrice").value=unitPriceVal;
		   document.getElementById("amount").value=amountVal; 


			  productNameVal = new Array();  
			  productDescriptionVal = new Array(); 
			  manufacturingDateVal = new Array(); 
			  productCodeVal = new Array(); 
			  quantityVal = new Array();  
			  unitPriceVal = new Array(); 
			  amountVal = new Array(); 
		 return  unitPriceVal;
			
		}
	 
	 
	 function collectAndSubmit(){
		 collectToExpendedValue();
		 
		// pincode address city state country email mobile orderDate names
		 var pincodeVal=$("#pincode").val();
		 var addressVal=$("#address").val();
	  	 var cityVal=$("#city").val();
	  	 var stateVal=$("#state").val();
	  	 var countryVal=$("#country").val();
	  	 var emailVal=$("#email").val();
	   	 var mobileVal=$("#mobile").val();
	   	 var orderDateVal=$("#orderDate").val(); 
		 var namesVal=$("#names").val();  
		/*  var productNameVal=$("productName").val();
		 var productDescriptionVal=$("productDescription").val();
		 var manufacturingDateVal=$("manufacturingDate").val();
		 var productCodeVal=$("productCode").val();
		 var quantityVal=$("quantity").val();
		 var unitPriceVal=$("unitPrice").val();
		 var amountVal= $("amount").val();	
		  */
		   var productNameVal= document.getElementById("productName").value;
		   var productDescriptionVal= document.getElementById("productDescription").value;
		   var manufacturingDateVal= document.getElementById("manufacturingDate").value;
		   var productCodeVal= document.getElementById("productCode").value;
		   var quantityVal=document.getElementById("quantity").value;
		   var unitPriceVal=document.getElementById("unitPrice").value;
		   var amountVal=document.getElementById("amount").value; 
		 
	 	 $.ajax({
	 	      type: "POST",
	 	      url: "/e-commerce/admin/savewholesale",
	 	      data:{ 
	 	      names:namesVal,
	 	      orderDate:orderDateVal, 
	 	      mobile:mobileVal, 
	 	      email:emailVal,
	 	      country:countryVal,
	 	      state:stateVal,
	 	      city:cityVal,
	 	      address:addressVal,
	 	      pincode:pincodeVal,
	 	      productName:productNameVal,
	 		  productDescription:productDescriptionVal,
	 	      manufacturingDate:manufacturingDateVal,
	 	      productCode:productCodeVal,
	 	      quantity:quantityVal,
	 	      unitPrice:unitPriceVal,
	 	      amount:amountVal
	 	      } ,
	 	      success: function(data) {
	 	    	 // alert(data);
	 	    	//  document.getElementById("subOrgClient").innerHTML = data;
	 	    	//  document.getElementById("subOrgClient").value.innerHTML=data;
	 	      }
	 	    });
	     return true;
	 } 
	 
	 
	  function isNumberKey(evt,id)
	  { 
	     var charCode = (evt.which) ? evt.which : event.keyCode;
	     if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105))
	     { 
	    alert("Please enter numeric value only !");
	     $("input#"+id).val(""); 
	     $("input#"+id).focus(); 
	      document.getElementById(id).value="";
	      document.getElementById(id).focus();
	       return false;
	     }else{
	       //  document.getElementById('message').innerHTML="";
	         return true;	
	     } 
	  }
	  
	  
		function addWholeSale($scope, $http) {  
			 
			$scope.submits = function() {
				$scope.productName=$("productName").val();
				$scope.productDescription=$("productDescription").val();
				$scope.manufacturingDate=$("manufacturingDate").val();
				$scope.productCode=$("productCode").val();
				$scope.quantity=$("quantity").val();
				$scope.unitPrice=$("unitPrice").val();
				$scope.amount= $("amount").val();
				var formData = {  
						  "names":$scope.names,
				 	      "orderDate":$scope.orderDate, 
				 	      "mobile":$scope.mobile, 
				 	      "email":$scope.email,
				 	      "country":$scope.country,
				 	      "state":$scope.state,
				 	      "city":$scope.city,
				 	      "address":$scope.address,
				 	      "pincode":$scope.pincode,
				 	      "productName":$scope.productName,
				 		  "productDescription":$scope.productDescription,
				 	      "manufacturingDate":$scope.manufacturingDate,
				 	      "productCode":$scope.productCode,
				 	      "quantity":$scope.quantity,
				 	      "unitPrice":$scope.unitPrice,
				 	      "amount":$scope.amount
				 };
	         //  alert($scope.amount +" "+$scope.unitPrice +" "+$scope.productCode);
				var response = $http.post('/e-commerce/admin/savewholesale.json', formData);
				response.success(function(data, status, headers, config) {
				 $scope.rb=data;
				});
				response.error(function(data, status, headers, config) {
					alert("Exception details: " + JSON.stringify({
						data : data
					}));
				});
	 		};
			  
		}
		
		

		
		function fillState()	
		{    var SelValue = document.getElementById("country").value;
		 //alert("Country :"+SelValue);
			 $.ajax({
			      type: "POST",
			      url: "/e-commerce/fetch/state/by/countryName",
			      data:{
			    	  countryName:SelValue
					  } ,
			      success: function(data) {
			    	  document.getElementById("state").innerHTML = data;
			    	  document.getElementById("state").value.innerHTML=data; 
			    	  
			      }
			    });
		    return true;
		}


		function fillCity()
		{
		    var stateId = document.getElementById("state").value;
			 $.ajax({
			      type: "GET",
			      url: "/e-commerce/fetch/city/by/cityid",
			      data:{
					  state:stateId
					  } ,
			      success: function(data) {
			    	  document.getElementById("city").innerHTML = data;
			    	  document.getElementById("city").value.innerHTML=data;
			      }
			    });
		    return true;
		}
		
		function fillCountry()
		{
		    $.ajax({
			      type: "POST",
			      url: "/e-commerce/fetch/country",
			      data:{
					  //state:stateId
					  } ,
			      success: function(data) {
			    	  document.getElementById("country").innerHTML = data;
			    	  document.getElementById("country").value.innerHTML=data;
			      }
			    });
		    return true;
		}
	</script>
</body>
</html>