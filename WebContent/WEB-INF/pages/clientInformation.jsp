<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>
<meta charset="ISO-8859-1">
<title></title>

<script src="/pcm/resources/js/angular.min.js"></script>
<link type="text/css" rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script type="text/javascript">
	//goople api

	var placeSearch, autocomplete, autocompleteTo, autocompleteStand;
	var componentForm = {
		street_number : 'short_name',
		route : 'long_name',
		locality : 'long_name',
		administrative_area_level_1 : 'short_name',
		country : 'long_name',
		postal_code : 'short_name'
	};

	//
	function initialize() {
		// Create the autocomplete object, restricting the search
		// to geographical location types.
		autocomplete = new google.maps.places.Autocomplete(
		/** @type {HTMLInputElement} */
		(document.getElementById('address')), {
			types : [ 'geocode' ]
		});
		// When the user selects an address from the dropdown,
		// populate the address fields in the form.
		google.maps.event.addListener(autocomplete, 'place_changed',
				function() {
					fillInAddress();
				});

	}

	// [START region_fillform]
	function fillInAddress() {
		// Get the place details from the autocomplete object.
		var place = autocomplete.getPlace();

		for ( var component in componentForm) {
			document.getElementById(component).value = '';
			document.getElementById(component).disabled = false;
		}

		// Get each component of the address from the place details
		// and fill the corresponding field on the form.
		for (var i = 0; i < place.address_components.length; i++) {
			var addressType = place.address_components[i].types[0];
			if (componentForm[addressType]) {
				var val = place.address_components[i][componentForm[addressType]];
				document.getElementById(addressType).value = val;
			}
		}
	}
	// [END region_fillform]

	// [START region_geolocation]
	// Bias the autocomplete object to the user's geographical location,
	// as supplied by the browser's 'navigator.geolocation' object.
	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = new google.maps.LatLng(
						position.coords.latitude, position.coords.longitude);
				var circle = new google.maps.Circle({
					center : geolocation,
					radius : position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}
	// [END region_geolocation]
</script>
<script type="text/javascript">
	function clInfo($scope, $http) {
		$scope.submit = function() {
			var formData = {
				"userName" : $scope.user,
				"userPassword" : $scope.password,
				"age" : $scope.age,
				"gender" : $scope.gender,
				"role" : $scope.fname,
				"fristName" : $scope.fname,
				"lastName" : $scope.lname,
				"mobile" : $scope.mobile,
				"email" : $scope.email,
				"address" : $scope.address
			};

			var response = $http.post('/pcm/signup.json', formData);
			response.success(function(data, status, headers, config) {
				//	$scope.list.push(data);
				$scope.msg = data;
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});

			$scope.list = [];

		};

		$scope.clearText = function() {
			$scope.msg.message = '';
		};

	}
</script>
</head>
<body onload="initialize();">

	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<fieldset class="borderManager table-responsive">
					<legend class="borderManager" style="font-size: 24px;">
						&nbsp;&nbsp;&nbsp;Sign Up &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
							style="color: green;"></span>
					</legend>
					<form ng-submit="submit()" ng-controller="clInfo">
						<div class="clearfix margin_05"></div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="col-lg-12 col-md-12 col-sm-12"
								style="background-color: #f1f1f1;">
								<h3>
									<input style="color: green;" value="{{msg.message}}"
										readonly="readonly">
								</h3>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12"
							style="background-color: #f1f1f1;">
							<h3>User Details</h3>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								First Name<span style="color: red;">*</span>
							</div>
							<input ng-model="fname" id="fname" ng-maxlength='100'
								class="form-control input-lg" style="width: 100%;"
								placeholder="First Name" required />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								Last Name<span style="color: red;">*</span>
							</div>
							<input ng-model="lname" id="lname" ng-maxlength='100'
								class="form-control input-lg" style="width: 100%;"
								placeholder="Last Name" required />
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								Age<span style="color: red;"></span>
							</div>
							<input type='number' ng-model='age' ng-maxlength='3'
								ng-minlength='1' maxlength="3" class="form-control input-lg"
								style="width: 100%;" placeholder="Age">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								Gender<span style="color: red;"></span>
							</div>

							<select id="gender" ng-model="gender"
								class="form-control input-lg" style="width: 100%;" required>
								<option ng-model="Male">Male</option>
								<option ng-model="Female">Female</option>
								<option ng-model="Transgender">Transgender</option>
							</select>
						</div>

						<div class="clearfix margin_05"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-12 col-md-12 col-sm-12"
							style="background-color: #f1f1f1;">
							<h3>Contact Details</h3>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								Mobile<span style="color: red;">*</span>
							</div>
							<input type="number" ng-model="mobile" id="mobile"
								class="form-control input-lg" style="width: 100%;"
								placeholder="Mobile" required />
						</div>

						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								E-Mail<span style="color: red;">*</span>
							</div>
							<input type="email" ng-model="email" id="email"
								class="form-control input-lg" style="width: 100%;"
								placeholder="E-Mail" required />
						</div>
						<div class="col-lg-6 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								Address<span style="color: red;">*</span>
							</div>
							<input ng-model="address" id="address"
								class="form-control input-lg" style="width: 100%;"
								placeholder="Address" required />
						</div>
						<div class="clearfix margin_05"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-12 col-md-12 col-sm-12"
							style="background-color: #f1f1f1;">
							<h3>Sing In Details</h3>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								User Name Or E-Mail<span style="color: red;">*</span>
							</div>
							<input ng-model="user" id="user" class="form-control input-lg"
								style="width: 100%;" placeholder="User" required />
						</div>

						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom: 6px;">
								Password<span style="color: red;">*</span>
							</div>
							<input type="password" ng-model="password" id="password"
								class="form-control input-lg" style="width: 100%;"
								placeholder="Password" required />
						</div>
						<div class="clearfix margin_05"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div
								style="margin-top: 6px; font-size: 14px; text-align: left; margin-left: 0px; margin-top: 8px;">
								<input type="submit" class="btn btn-danger btn-lg"
									value="Sign Up"> <input type="reset"
									class="btn btn-danger btn-lg" value="Reset"
									ng-click='clearText()'>
							</div>
						</div>

					</form>

					<div id="message" style="color: red;"></div>
					<div class="clearfix margin_05"></div>
					<div class="clearfix margin_10"></div>
					<div class="clearfix"></div>
					<div class="clearfix margin_10"></div>
				</fieldset>
				<div class="clearfix margin_05"></div>
				<div class="clearfix margin_10"></div>
			</div>
		</div>
	</div>
</body>
</html>