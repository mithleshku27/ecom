<%@page import="com.itech.model.UserDetails"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>
<meta charset="ISO-8859-1">
<title></title>

<link type="text/css" href="/e-commerce/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css" rel="stylesheet" media="screen" /> 
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css"	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css"	rel="stylesheet" media="screen" /> 
<link rel="icon" type="image/png" sizes="96x96" href="/e-commerce/resources/images/app-fevicon.png">
<link type="text/css" href="/e-commerce/resources/css/media.css"	rel="stylesheet" media="screen" />
<script src="/e-commerce/resources/js/angular.min.js"></script>
<script type="text/javascript">
 
</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="col-md-6 login-right">
					<fieldset class="borderManager table-responsive">

						<h3>New customers</h3>
						<p>By creating an account with our store, you will be able to
							move through the checkout process faster, store multiple shipping
							addresses, view and track your orders in your account and more.</p>
						<a class="btn btn-danger btn-lg" href="/e-commerce/consumer/signup">Create an Account</a>
						<p></p>
						<div class="clearfix"></div>
						<div class="clearfix"></div>
						<div class="clearfix"></div>
					</fieldset>
				</div>
				<fieldset class="borderManager table-responsive">
					<div class="col-md-6 login-right">
						<h3>registered</h3>
						<p>If you have an account with us, please log in.</p>
						<form action="/e-commerce/consumer/loginProceed" method="post">
							<div>
								<span> <i class="glyphicon glyphicon-user red"
									style="color: blue; width: 4%;"></i>  User E-mail Address
								</span> <input type="text" name="users" id="users" class="form-control"
									placeholder="Email Address" required>

							</div>
							<div>
								<span> <i class="glyphicon glyphicon-lock red"	style="color: blue; width: 4%;"> </i> Password
								</span> <input type="password" name="uPassword" id="uPassword"	class="form-control" placeholder="Password" required>

							</div>
                            <a class="forgot btn btn-danger btn-sm" href="javascript:void(0);">Forgot Your Password?</a>
							<input type="submit" value="Login" class="btn btn-danger btn-sm">
						</form>
						
					</div>
					<div class="clearfix"></div>
				</fieldset>

			</div>
		</div>
	</div>
</body>
</html>
 