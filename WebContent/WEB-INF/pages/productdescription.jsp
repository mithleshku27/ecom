<%@page import="com.itech.model.ProductDescription" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Description</title>
<link type="text/css" href="/e-commerce/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css" rel="stylesheet" media="screen" />
<link href="/e-commerce/resources/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<script src="/e-commerce/resources/js/validation.js"></script>
<link href="/e-commerce/resources/jtable/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery.datetimepicker.js" type="text/javascript"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script type="text/javascript">

 $(document).ready(function(){
	 var dateToday = new Date();
	 var dd = dateToday.getDate();
	 var mm = dateToday.getMonth()+1; 
	 var yyyy = dateToday.getFullYear();
	 var toYears=parseInt(yyyy);
	 $(function() {
	 $('#manufacturingdate').datetimepicker({
	 	 showOn: 'button',
	 	 buttonImage: "/trux/resources/images/calendar.png",
	     buttonImageOnly: true, 
	     timepicker:false,
	     format:'Y/m/d',
	 	 dayOfWeekStart : 1,
	 	 lang:'en',
	 	 yearRange: '1800:' + toYears + '',
	 	 startDate:	dateToday  
	 	 });
	 });
}); 

function DateValidation()
{
	var dt= $("#manufacturingdate").val();
	//alert(dt);
	//alert(checkdate(dt));
	if(checkdate(dt)==false){ $("#divError").show(); }
	
	return checkdate(dt);//false;
	}
	
	
function checkdate(input){
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(input)) { /* alert("Invalid Date Format. Please correct and submit again."); */ }
	else
	{ 
		var dayfield=input.split("/")[0];
		var monthfield=input.split("/")[1];
		var yearfield=input.split("/")[2];
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
		{ /* alert("Invalid Day, Month, or Year range detected. Please correct and submit again."); */ }
		else {  returnval=true;}
	
	}
	/* if (returnval==false)
	{ alert("go3");  }
	alert("go4");
	alert(returnval); */
	return returnval;
	}

</script> 
</head>
<body>
<div>
<div class="clearfix hs_margin_10"></div>
<div class="container">  
		
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
	 
		
						 
		</div></div>
	<div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12"> 
            <form action="productdescriptiondetails" method="post">
 				    <fieldset class="fieldset2 col-lg-12" id="personal">                                    
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Product Name/Description</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-font red" style="color: blue" style="color: blue"></i></span> 
								<input  id="productname" name="productname"  class="form-control input-sm" style="width:100%;" placeholder="Product Name"  required="required"/>
                            </div>
							</div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Input Voltage</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="inputvoltage"  name="inputvoltage" class="form-control input-sm" style="width:100%;" placeholder="Input Voltage"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Product Code</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-font red" style="color: blue"></i></span>
                                <input  id="productcode" name="productcode"  class="form-control input-sm" style="width:100%;" placeholder="Product Code"  required="required"/>
                            </div></div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Input Power /Watts</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="inputpower"  name="inputpower" class="form-control input-sm" style="width:100%;" placeholder="Input Power /Watts in Hz"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Product Type</div>
                                 <!-- <input  id="producttype" name="producttype"  class="form-control input-sm" style="width:100%;" placeholder="Name"  required="required"/> --> 
                                 <select id="producttype" name="producttype" name="producttype" class="form-control input-sm" style="width:100%;">
                                  <option value="1" selected="selected">Volvo</option>
								  <option value="23">Saab</option>
								  <option value="22">Opel</option>
								  <option value="33">Audi</option>
								</select>
                            </div>
                              
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Frequency</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="frequency"  name="frequency" class="form-control input-sm" style="width:100%;" placeholder="Frequency"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">									                                  
                                <div style="margin-bottom:6px;">Size mm</div>
                                 <!--<input  id="size" name="size"  class="form-control input-sm" style="width:100%;" placeholder="Name"  required="required"/>--> 
                                 <select id="size" name="size" class="form-control input-sm" style="width:100%;">
                                  <option value="32" selected="selected">Volvo</option>
								  <option value="33">Saab</option>
								  <option value="22">Opel</option>
								  <option value="22">Audi</option>
								</select> 
                            </div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Power Factor VAC</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  type="text" id="powerfactor"  name="powerfactor" class="form-control input-sm" style="width:100%;" placeholder="Power Factor VAC"  required="required"/>
                            </div></div>
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Weight Kg</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-plus red" style="color: blue"></i></span>
                                <input  id="weight" name="weight"   onkeypress="return isDecimalNumeric(this, event);"  class="form-control input-sm" style="width:100%;" placeholder="Weight Kg"  required="required"/>
                            </div> </div> 
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Colour Temperatures</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-cloud red" style="color: blue"></i></span>
                                <input  type="text" id="colourtempratures"  name="colourtempratures" class="form-control input-sm" style="width:100%;" placeholder="Colour Temperatures"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Energy Saving</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-flash red" style="color: blue"></i></span>
                                <input  id="energysaving" name="energysaving"  class="form-control input-sm" style="width:100%;" placeholder="Energy Saving"  required="required"/>
                            </div></div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Colour Rendering Index</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-user red" style="color: blue"></i></span>
                                <input  type="text" id="colourrenderingindex"  name="colourrenderingindex" class="form-control input-sm" style="width:100%;" placeholder="Colour Rendering Index"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Manufecturing Date</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-calendar red" style="color: blue"></i></span>
                                <input  id="manufacturingdate" name="manufacturingdate"  class="form-control input-sm" style="width:100%;" placeholder="Manufecturing Date"  required="required"/>
                            </div></div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Ingress Protection</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-plus red" style="color: blue"></i></span>
                                <input  type="text" id="ingressprotection"  name="ingressprotection" class="form-control input-sm" style="width:100%;" placeholder="Ingress Protection"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Quantity</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-plus red" style="color: blue"></i></span>
                                <input  id="quantity" name="quantity"  onkeypress="return isDecimalNumeric(this, event);"   class="form-control input-sm" style="width:100%;" placeholder="Quantity"  required="required"/>
                            </div></div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Impact Protection</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-plus red" style="color: blue"></i></span>
                                <input  type="text" id="impactprotection"  name="impactprotection" class="form-control input-sm" style="width:100%;" placeholder="Impact Protection"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Unit Price</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i class="fa fa-inr" style="color: blue"></i></span>
                                <input  id="unitprice" name="unitprice" onkeypress="return isDecimalNumeric(this, event);"  class="form-control input-sm" style="width:100%;" placeholder="Unit Price"  required="required"/>
                            </div></div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Life Span</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-plus red" style="color: blue"></i></span>
                                <input  type="text" id="lifespan"  name="lifespan" class="form-control input-sm" style="width:100%;" placeholder="Life Span"  required="required"/>
                            </div></div>
                             
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">		
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                            <div style="margin-bottom:6px;">Description</div>
                                <!-- <input  type="text" id="description"  name="description" class="form-control input-sm" style="width:100%; height: 100px;" placeholder="Description"  required="required"/> -->
                                <textarea rows="11" cols=""  id="description"  name="description" name="description" class="form-control input-sm" style="width:100%; resize:none;"  placeholder="Description"> </textarea>
                            
                            </div>
                            </div>									                                  
                                </div>  
                            <div class="col-lg-6 col-md-6 col-sm-12">	
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div style="margin-bottom:6px;">Safety Standard</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-globe red" style="color: blue"></i></span>
                                <input  type="text" id="safetystandard"  name="safetystandard" class="form-control input-sm" style="width:100%;" placeholder="Safety Standard"  required="required"/>
                            </div>
                            </div>
                            </div>
                            	<div class="clearfix margin_10"></div>  
                        
                            	<div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div style="margin-bottom:6px;">EMC/EMI Standard
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-globe red" style="color: blue"></i></span>
                                <input  type="text" id="emcemistandard"  name="emcemistandard" class="form-control input-sm" style="width:100%;" placeholder="EMC/EMI Standard"  required="required"/></div>
                           </div>
                            </div>
                            </div>
                            		
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div style="margin-bottom:6px;">Harmonics</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-globe red" style="color: blue"></i></span>
                                <input  type="text" id="harmonics"  name="harmonics" class="form-control input-sm" style="width:100%;" placeholder="Harmonics"  required="required"/>
                            </div></div>
                            </div>
                            		
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                   <div style="margin-bottom:6px;">Lumenus</div>
                             <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-globe red" style="color: blue"></i></span>
                                <input  type="text" id="lumenus"  name="lumenus" class="form-control input-sm" style="width:100%;" placeholder="Lumenus"  required="required"/>
                           </div>
                            </div>
                            </div>
                            		
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div style="margin-bottom:6px;">Incandescent</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-globe red" style="color: blue"></i></span>
                                <input  type="text" id="incandescent"  name="incandescent" class="form-control input-sm" style="width:100%;" placeholder="Incandescent"  required="required"/>
                            </div></div>
                            </div>
                           </div>
                             
                        </div>
                        <div class="row" id="divError" style="display:none;">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div style="margin-bottom:6px;" id="divMessage" style="color: red;">* Invalid Date.</div>
                            </div>
                            </div>
                             
                        <div class="clearfix margin_10"></div>  
                        
                     
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <input  type="submit" id="btnSubmit"   class="btn btn-success"  value="Submit"/>
                                
                            </div>
                        </div>
                        
                          
                    </fieldset>
</form>   
		</div>  
            
        
       </div>		
  	</div>   
</div>
<div class="clearfix margin_30"></div>
<div class="clearfix margin_30"></div>
 

</body>
</html>