<%@page import="org.apache.catalina.util.Base64" %>
<%@page import="com.itech.model.ProductDetails" %>
<%@page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="/e-commerce/resources/productlist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/font-awesome.min.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/prettyPhoto.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/price-range.css" rel="stylesheet">
    <link href="/e-commerce/resources/productlist/css/animate.css" rel="stylesheet">
	<link href="/e-commerce/resources/productlist/css/main.css" rel="stylesheet">
	<link href="/e-commerce/resources/productlist/css/responsive.css" rel="stylesheet">
          
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/e-commerce/resources/productlist/images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head><!--/head-->

<body>
	<%
			List<ProductDetails> list= (List<ProductDetails>)request.getAttribute("list");

%>
		
	
	
	<section>
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						
	<%if(list!=null && list.size()>0){  
		int i=1;
		for(ProductDetails dto:list){
			String id="id"+ Integer.toString(i);
			i = i + 1;
			String url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
			String large = "data:image/png;base64," + Base64.encode(dto.getImageFile());
	%>
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="<%=url%>" alt="" />
											<h2><i class="fa fa-inr" ></i> &nbsp; <%=dto.getUnitPrice() %></h2>
											<p><%=dto.getProductName() %></p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>View Details</a>
										</div>
										<div class="product-overlay" style="opacity: 0.5; ">
											<div class="overlay-content">
											<%-- <h2><i class="fa fa-inr" style="color: blue"></i> &nbsp;&nbsp; <%=dto.getUnitPrice() %></h2> --%>
											<p><%= dto.getProductName() %></p>
											<a href="getItemDetailsWithProductID?productID=<%=dto.getPrid() %>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>View Details</a>
											</div>
										</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
						
				<%} }%>		
						
						
					</div><!--features_items-->
					
					
					
					
				</div>
			</div>
		</div>
	</section>
	
	

  
    <script src="/e-commerce/resources/productlist/js/jquery.js"></script>
	<script src="/e-commerce/resources/productlist/js/bootstrap.min.js"></script>
	<script src="/e-commerce/resources/productlist/js/jquery.scrollUp.min.js"></script>
	<script src="/e-commerce/resources/productlist/js/price-range.js"></script>
    <script src="/e-commerce/resources/productlist/js/jquery.prettyPhoto.js"></script>
    <script src="/e-commerce/resources/productlist/js/main.js"></script>
</body>
</html>