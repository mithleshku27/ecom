<%@page import="org.apache.catalina.util.Base64"%>
<%@page import="com.itech.model.ProductDetails" %>
<%@page import="com.itech.model.Cart" %>
<%@page import="java.util.List" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | i-Tech Mass</title>
    <link href="/e-commerce/resources/productlist/css/bootstrap.min.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/font-awesome.min.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/prettyPhoto.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/price-range.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/animate.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/responsive.css" rel="stylesheet">
<script src="/e-commerce/resources/js/validation.js"></script>
<script src="/e-commerce/resources/js/jsp-script/checkout.js"></script>
<script src="/e-commerce/resources/js/jsp-script/cart-list.js"></script>
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<link rel="shortcut icon" href="images/ico/favicon.ico">

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style type="text/css">
.input-margin {
	margin: 0px 0px 10px 0px;
}

.border-radius-none {
	border-radius: 0px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		fillCountry();
		LoginStatusCheck();
		CheckCartList('cartSize');
	});

	function fillCountry() {
		$.ajax({
			type : "GET",
			url : "/e-commerce/fetch/country",
			data : {
			//state:stateId
			},
			success : function(data) {
				document.getElementById("countryname").innerHTML = data;
				document.getElementById("countryname").value.innerHTML = data;
				fillState();
			}
		});
		return true;
	}

	function fillState() {
		//alert('ss');
		var SelValue = document.getElementById("countryname").value;
		//alert(SelValue);
		$.ajax({
			type : "GET",
			url : "/e-commerce/fetch/state/by/countryName",
			data : {
				countryName : SelValue
			},
			success : function(data) {
				document.getElementById("statename").innerHTML = data;
				document.getElementById("statename").value.innerHTML = data;

			}
		});
		return true;
	}

	function LoginStatusCheck() {
		var status = $('#hdfLoginStatus').val();
		//alert(status);
		$('#hdfLoginStatus').val('Y');
		var status = $('#hdfLoginStatus').val();
		//alert(status);
		if (status != "N") {
			$("#loginDIV").hide();
			$("#billToDetailDIV").removeClass("col-sm-5");
			$("#billToDetailDIV").addClass("col-sm-8");
		}

		return false;
	}
</script>
</head>

<body>
<input type="hidden" id="hdfLoginStatus" value="Y">
	 <%-- <%	ProductDetails dtos = (ProductDetails)request.getAttribute("dtos");	%> --%>
	<%	List<Cart> list= (List<Cart>)request.getAttribute("crtList");	%> 
	
	<section id="cart_items">
		<div class="container">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody id="cartItem">
					<%
								int id=0;
													if (list != null && list.size() > 0)
													{
														int i = 1;
														String priceID = "hdfPrice" + i;
														String quantityID = "cart_quantity_input" + i;
														double ecototal=0;
														double shippingtotal =0;
														for (Cart dto : list)
														{ id++;
															String url = "";
															String CartID = String.valueOf(dto.getId());
															String aCartID = "aCartID" + String.valueOf(dto.getId());
															
															String aPlus = "aPlus" + String.valueOf(dto.getId());
															String cart_quantity_input = "cart_quantity_input" + String.valueOf(dto.getId());
															String aMinus = "aMinus" + String.valueOf(dto.getId());
															String quantityInput = "quantityInput" + String.valueOf(dto.getId());
															String priceLevel = "priceLevel" + String.valueOf(dto.getId());
															String hdfPrice = "hdfPrice" + String.valueOf(dto.getId());
															String trid = "tr" + String.valueOf(dto.getId());
															
															
															double total = dto.getProductPrice() * dto.getQuantity();
															ecototal = dto.getEcocost();
															shippingtotal = dto.getShippingcost();
															int cartid = dto.getId();
															if (dto != null && dto.getImageFile() != null)
															{
																url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
															}
															else
															{
																url = "/e-commerce/resources/productlist/images/cart/one.png";
															}
															i = i + 1;
							%>
							<tr id="<%=trid%>">
								<td class="cart_product"><a href=""><img
										src="/e-commerce/resources/productlist/images/cart/one.png"
										alt=""></a></td>

								<td class="cart_description">
									<h4>
										<a href=""><%=dto.getProductName()%></a>
									</h4>
									<p>
										Web ID: <%=dto.getProductID()%></p>
								</td>
								<td class="cart_price">
									<p><%=dto.getProductPrice()%></p> <input type="hidden"
									id="<%=hdfPrice%>" name="" Value="<%=dto.getProductPrice()%>">
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button">
										<a class="cart_quantity_up" href="" id="<%=aPlus%>"
											style="padding: 5px; text-decoration: none;"
											onclick="return QuantityPlus('<%=cartid%>','<%=cart_quantity_input%>','<%=priceLevel%>','<%=hdfPrice%>','#priceLevelsId<%=id%>');">
											+ </a> <input class="cart_quantity_input" type="text"
											name="quantity" id="<%=cart_quantity_input%>"
											name="cart_quantity_input"
											onkeypress="return IsNumeric(event)"
											value="<%=dto.getQuantity()%>" autocomplete="off" size="1">
										<a class="quantity_down" href=""
											style="padding: 5px; text-decoration: none;" id="<%=aMinus%>"
											onclick="return QuantityMinus('<%=cartid%>','<%=cart_quantity_input%>','<%=priceLevel%>','<%=hdfPrice%>','#priceLevelsId<%=id%>');">
											- </a>
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price"
										style="font-weight: normal; font-size: 19px;">
										<i class='fa fa-inr'
											style="font-weight: normal; font-size: 17px;"></i><input
											type="hidden" id="priceLevelsId<%=id%>" value="<%=total%>">
										<span id="<%=priceLevel%>"><%=total%></span>
									</p>
									<input type="hidden" value="<%=ecototal%>" id="ecototal<%=id%>">
									<input type="hidden" value="<%=shippingtotal%>" id="shippingtotal<%=id%>">
								</td>
								<td class="cart_delete"><a class="cart_quantity_delete"
									href=""
									onclick="return RemoveItemsFromCart(this.id,'<%=cartid%>');"
									id="<%=aCartID%>"> <i class="fa fa-times"></i></a> <input
									type="hidden" id="<%=CartID%>" value="<%=cartid%>"></td>
							</tr>



							<%
								}
													}
							%>
							<!-- <tfoot></tfoot> -->
							<tr>
								<td colspan="4">&nbsp;</td>
								<td colspan="2">
									<table class="table table-condensed total-result">
										<tr>
											<td>Cart Sub Total</td>
											<td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="subtotal">61</span></td>
										</tr>
										<tr>
											<td>Exo Tax</td>
											<td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="ecototalHTML">0.00</span></td>
										</tr>
										<tr class="shipping-cost">
											<td>Shipping Cost</td>
											<td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="shippingtotalHTML">0.00</span></td>
										</tr>
										<tr>
											<td>Total</td>
											<td><script type="text/javascript">
												$(document).ready(function() {
													collectToTotalValue();
												});
											</script><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="total"></span></td>
										</tr>
									</table>
								</td>
							</tr>	
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
<div class="row"> <div class="payment-options">
  <span> <a href="/e-commerce/consumer/checkout" id="btnCheckout"  class="btn btn-primary pull-right" style="margin: 10px" >Checkout</a>  </span>
  <span> <a href="/e-commerce/consumer/getItemDetailsWithCategory?productCategory=women" style="margin: 10px"  id="btnContinueShopping"  class="btn btn-primary pull-right">Continue Shopping</a>  </span>
					
				</div></div>
    <script src="/e-commerce/resources/productlist/js/jquery.js"></script>
	<script src="/e-commerce/resources/productlist/js/bootstrap.min.js"></script>
	<script src="/e-commerce/resources/productlist/js/jquery.scrollUp.min.js"></script>
    <script src="/e-commerce/resources/productlist/js/jquery.prettyPhoto.js"></script>
    <script src="/e-commerce/resources/productlist/js/main.js"></script>
</body>
</html>