<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script type="text/javascript" src="/e-commerce/resources/js/tabpenel.js"></script> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" media="screen" />
<!-- <style type="text/css">
 

/*----------tap panel style------*/
 .tab-panel {
            width: 100%;
            display: inline-block;
        }

        .tab-link:after {
            display: block;
            padding-top: 20px;
            content: '';
        }

        .tab-link li {
            margin: 0px 5px;
            float: left;
            list-style: none;
        }

        .tab-link a {
            padding: 8px 15px;
            display: inline-block;
            border-radius: 10px 3px 0px 0px;
            background: #8dc8f5;
            font-weight: bold;
            color: #4c4c4c;
        }

            .tab-link a:hover {
                background: #b7c7e5;
                text-decoration: none;
            }

        li.active a, li.active a:hover {
            background: #ccc;
            color: #0094ff;
            text-decoration:none;
        }

        .content-area {
            padding: 15px;
            border-radius: 3px;
            box-shadow: -1px 1px 1px rgba(0,0,0,0.15);
            background: #fff;
            border: 1px solid #ccc;
        }

        .inactive{
            display: none;
        }

        .active {
            display: block;
        }


</style> -->

<script type="text/javascript">
$(document).ready(function () {
	   $('.tab-panel .tab-link a').on('click', function (e) {
	        var currentAttrValue = jQuery(this).attr('href');
 
	        $('.tab-panel ' + currentAttrValue).slideDown(400).siblings().slideUp(400);
 
	        $(this).parent('li').addClass('active').siblings().removeClass('active');

	        e.preventDefault();
	    });
	});

</script>
</head>

<body>
 <h3>Tab Panel Example</h3>
    <hr />

        <div class="tab-panel">
            <ul class="tab-link">
                <li class="active"><a href="#FirstTab">First Tab</a></li>
                <li><a href="#SecondTab">Second Tab</a></li>
                <li><a href="#ThirdTab">Third Tab</a></li>
            </ul>

            <div class="content-area">
                <div id="FirstTab" class="active">
                    <p>This is sample tab area in First Tab.</p>
                </div>

                <div id="SecondTab" class="inactive">
                   <p>This is the sample tab area in Second tab.</p>
                </div>

                <div id="ThirdTab" class="inactive">
                    <p>This is the sample tab area in Third tab.</p>
                </div>

            </div>
        </div>

</body>
</html>

