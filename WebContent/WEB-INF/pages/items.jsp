<%@page import="org.apache.catalina.util.Base64"%>
<%@page import="com.itech.model.ProductDetails"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="container">
		<%
			ProductDetails dtos = (ProductDetails)request.getAttribute("dtos");
		%>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form class="form-horizontal" action="/e-commerce/admin/geristeritemDetails" method="post" enctype="multipart/form-data">
					<fieldset class="borderManager table-responsive">
						<legend class="borderManager glyphicon glyphicon-euro red" style="font-size: 24px;color: blue;"> Product Item Details
							<%
								if (dtos != null)
								{
							%>
							<%=dtos.getPrid()%>
							<%
								}
							%>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: green;"></span>
						</legend>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-gift red" style="color: blue;"></i>
								</span> <input type="text" id="productName" name="productName"
									class="form-control" placeholder="Product Name" required>
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"	style="margin-bottom: 6px; width: 10%; text-align: left;">
								<i class="glyphicon glyphicon-gift red" style="color: blue;"></i></span>
									 <input	type="text" name="productCode" id="productCode"	class="form-control" placeholder="Product Code" required>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;"><i
									class="glyphicon glyphicon-th-list red" style="color: blue;"></i></span>
								<select name="productCategory" id="productCategory"
									class="form-control">
									<option value="0">--Select Product Category--</option>
									<option value="man">Man</option>
									<option value="women">Women</option>
									<option value="kids">Kids</option>
									<option value="flower">Flower</option>
									<option value="bakery">Bakery</option>
									<option value="courrier">Courrier</option>
								</select>

							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;"><i
									class="glyphicon glyphicon-th-list red" style="color: blue;"></i></span>
								   <select name="productCategorys" id="productCategorys"	class="form-control">									
									<option value="t-shirts">--Select Item Category--</option>
									<option value="t-shirts">T-shirt</option>
									<option value="shirt">Shirt</option>
									<option value="jeans">Jeans</option>
									<option value="trousers">Trousers</option>
									<option value="sports wear">Sports wear</option>
									<option value="shorts">Shorts</option>
									<option value="innerwear">Inner wear</option>
									<option value="suits&blazzers">Suits & Blazers</option>
									<option value="socks&ties">Socks&  Ties</option>
								</select>

							</div>
						</div>
						
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;"><i
									class="glyphicon glyphicon-th-list red" style="color: blue;"></i></span> <select
									name="productSubCategory" id="productSubCategory" class="form-control">									
									<option value="t-shirts">--Select Item Category--</option>
									<option value="t-shirts">T-shirt</option>
									<option value="shirt">Shirt</option>
									<option value="jeans">Jeans</option>
									<option value="trousers">Trousers</option>
									<option value="sports wear">Sports wear</option>
									<option value="shorts">Shorts</option>
									<option value="innerwear">Inner wear</option>
									<option value="suits&blazzers">Suits & Blazers</option>
									<option value="socks&ties">Socks &  Ties</option>
								</select>

							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;"><i
									class="glyphicon glyphicon-picture red" style="color: red;"></i></span> <input
									type="file" name="fileImage" id="fileImage" placeholder="Upload Product Image"
									class="form-control" required>
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;"><i
									class="glyphicon glyphicon-road red" style="color: green;"></i></span> <input
									type="text" name="unitPrice" id="unitPrice"
									class="form-control" placeholder="Unit Price" required>
							</div>
						</div>




						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-inbox red" style="color: blue;"></i>
								</span> <input type="text" name="totalQuantity" id="totalQuantity" class="form-control"
									placeholder="Total Quantity" required>
							</div>
						</div>


						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="col-lg-12">
								<button type="submit" class="btn btn-danger btn-lg"
									id="linkLogin">Add Product</button>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
					</fieldset>
				</form>

				<div class="clearfix"></div>
				<div class="clearfix margin_10"></div>
				<div class="col-lg-3 col-md-3 col-sm-12">
					<div class="col-lg-12">
						<%
							if (dtos != null)
							{
								String url = "data:image/png;base64," + Base64.encode(dtos.getImageFile());
						%>
						<img alt="" src="<%=url%>" width="200px" height="200px">
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>