<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.itech.model.CartShipping"%>
<%@page import="com.itech.model.Cart"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/e-commerce/resources/invoice/style.css" media="all" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
    <script src="/e-commerce/resources/js/jsp-script/cart-list.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    	CheckCartList('cartSize');
    	collectToTotalValue();
    	function collectToTotalValue() {
    		//alert("collectToTotalValue");
    		var tbl = document.getElementById('cartItem');
    		var lastRow = tbl.rows.length-3;
    		var iteration = lastRow; 
    		//alert("rows.length : "+iteration);
    		var totalAmount = 0;
    		//var totalEchoAmount = 0;
    		//var totalShippingAmount = 0;
    		var idName = new Array("", "#priceLevelsId");
    		//var echoCostID = new Array("", "#ecototal");
    		//var ShippingCostID = new Array("", "#shippingtotal");

    		var priceLevel = 0;
    		//var echoPriceLevel=0;
    		//var ShippingPriceLevel=0;

    		for (var i = 1; i < iteration; i++) {
    			var priceLevelInner = 0;
    			//var echoCostInner =0;
    			//var ShippingCostInner =0;

    			for (var id = 1; id < idName.length; id++) {
			//alert("idName.length: "+idName.length);
    				if (id == 1) {
    					priceLevelInner = $(idName[id] + i).val();
    					//echoCostInner = $(echoCostID[id] + i).val();
    					//ShippingCostInner = $(ShippingCostID[id] + i).val();
    					//alert($(echoCostID[id] + i));
    				}

    				if (id == (idName.length) - 1) {
    					priceLevel = parseFloat(priceLevelInner);
    					//echoPriceLevel = parseFloat(echoCostInner);
    					//ShippingPriceLevel = parseFloat(ShippingCostInner);
    				} else {
    					priceLevel =parseFloat(priceLevelInner);
    					//echoPriceLevel = parseFloat(echoCostInner);
    					//ShippingPriceLevel = parseFloat(ShippingCostInner);
    				}
    				totalAmount+=priceLevel;
    				//alert("A:"+totalAmount);
    				//totalEchoAmount+= echoPriceLevel;
    				//alert("B:"+totalEchoAmount);
    				//totalShippingAmount+= ShippingPriceLevel;
    			} 

    		}
    		var taxAmount= totalAmount * 0.144;
    		var total= totalAmount + taxAmount;
    		document.getElementById("subtotal").innerHTML = totalAmount.toFixed(2);
    		document.getElementById("tax").innerHTML = taxAmount.toFixed(2);
    		document.getElementById("total").innerHTML = total.toFixed(2);
    		
    		$("#deliverydate").html($("#deliverydate1").val());
    		$("#duedate").html($("#billdate1").val());
    		//document.getElementById("tax").innerHTML = totalEchoAmount.toFixed(2);//$("#ecototal1").val();
    		//document.getElementById("shippingtotalHTML").innerHTML = totalShippingAmount.toFixed(2);
    		return false;
    	}

    });
    
    </script>
  </head>
  <body  id="invoice-body" >
 <% List<Cart> list = (List<Cart>)request.getAttribute("list"); 
 	CartShipping csList= (CartShipping)request.getAttribute("cslist");
 %>
  
    <div id="header" class="clearfix" style="background-color: lightgray;">
      <div id="logo">
        <img src="/e-commerce/resources/images/logo.png">
      </div>
      <div id="company">
        <h2 class="name">iTech Mass</h2>
        <div>E-53/54, Sector-3,</div>
        <div>Noida, UP, India</div>
        <div><a href="mailto:company@example.com">info@itechmass.com</a></div>
      </div>
    </div>
    
    <div id="main">
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">INVOICE TO:</div>
          <h2 class="name"><%= csList.getCustomername() %></h2>
          <div class="address"><%= csList.getAddress1() %>, <%= csList.getAddress2() %><!-- 796 Silver Harbour, TX 79273, US --></div>
          <div class="email"><a href="mailto:john@example.com"><%= csList.getCustomeremail() %></a></div>
        </div>
        <div id="invoice">
          <h1>INVOICE 3-2-1</h1>
          <div class="date">Date of Invoice: <span id="deliverydate"></span></div>
          <div class="date">Due Date: <span id="duedate"></span></div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0" id="cartItem">
        <thead>
          <tr>
            <th class="total">Sl.</th>
            <th class="desc" style="text-align: center;">DESCRIPTION</th>
            <th class="unit">UNIT PRICE</th>
            <th class="qty">QUANTITY</th>
            <th class="total">TOTAL</th>
          </tr>
        </thead>
        <tbody>
        <%if(list != null && list.size() > 0){ 
      	  int id=1;
      	  double subtotal = 0;
      	  for (Cart dto : list){
      		   
      		  Date invoiceDate = new Date();
      		  String dt= (new SimpleDateFormat("dd-MM-yyyy").format(invoiceDate)).toString();
      		  double total= dto.getProductPrice() * dto.getQuantity();
      		  subtotal = subtotal + total;
      		  //invoiceDate=dto.getBillDate();
      		  String billingDate=dto.getBillDates();
      		  String deliveryDate=dto.getDeliveryDates();
      		  
        %>
        
          <tr>
            <td class="no"><%= id %></td>
            <td class="desc"><h3><%=dto.getProductName() %></h3>Creating a recognizable design solution based on the company's existing visual identity</td>
            <td class="unit"><i class='fa fa-inr' style="font-weight: normal; font-size: 14px;">&nbsp;</i><%= dto.getProductPrice() %></td>
            <td class="qty"><%=dto.getQuantity() %></td>
            <td class="total"><i class='fa fa-inr' style="font-weight: normal; font-size: 14px;">&nbsp;</i><%= total %>
            <input type="hidden" id="priceLevelsId<%=id %>" value="<%=total %>" >
            <input type="hidden" id="deliverydate<%=id %>"  value="<%=dt %>" >
            <input type="hidden" id="billdate<%=id %>"  value="<%=billingDate %>" >
            </td>
          </tr>
          
          <% id++; } } %>
        
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">SUBTOTAL</td>
            <td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px;">&nbsp;</i><span id="subtotal"></span></td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">S.TAX &nbsp;(14.4%)</td>
            <td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px;">&nbsp;</i><span id="tax"></span></td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">GRAND TOTAL</td>
            <td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px;">&nbsp;</i><span id="total"></span></td>
          </tr>
        </tfoot>
      </table>
      <div class="row" style="text-align: center; padding: 0px 15px 0px 15px;">
      <h1 class="pull-left"><span class="label label-info label-lg" style="border-radius: 0px;">Payment Mode: COD</span></h1>
      <h1 class="pull-right"><span class="label label-warning label-lg" style="border-radius: 0px;">UNPAID</span></h1>
      </div>
      <div id="thanks">Thank you!</div>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div>
    </div>
    <div id="footer">
     
    </div>
    
  </body>
</html>