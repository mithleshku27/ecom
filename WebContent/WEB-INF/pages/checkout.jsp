<%@page import="com.itech.model.UserDetails"%>
<%@page import="org.apache.catalina.util.Base64"%>
<%@page import="com.itech.model.ProductDetails"%>
<%@page import="com.itech.model.Cart"%>
<%@page import="com.itech.model.CartShipping"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Cart | i-Tech Mass</title>
<link href="/e-commerce/resources/productlist/css/bootstrap.min.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/font-awesome.min.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/prettyPhoto.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/price-range.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/animate.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/productlist/css/responsive.css" rel="stylesheet">
<script src="/e-commerce/resources/js/validation.js"></script>
<script src="/e-commerce/resources/js/jsp-script/checkout.js"></script>
<script src="/e-commerce/resources/js/jsp-script/cart-list.js"></script>
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style type="text/css">
.input-margin {
	margin: 0px 0px 10px 0px;
}

.border-radius-none {
	border-radius: 0px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		fillCountry();
		LoginStatusCheck();
		CheckCartList('cartSize');
	});

	function fillCountry() {
		$.ajax({
			type : "GET",
			url : "/e-commerce/fetch/country",
			data : {
			//state:stateId
			},
			success : function(data) {
				document.getElementById("countryname").innerHTML = data;
				document.getElementById("countryname").value.innerHTML = data;
				fillState();
			}
		});
		return true;
	}

	function fillState() {
		//alert('ss');
		var SelValue = document.getElementById("countryname").value;
		//alert(SelValue);
		$.ajax({
			type : "GET",
			url : "/e-commerce/fetch/state/by/countryName",
			data : {
				countryName : SelValue
			},
			success : function(data) {
				document.getElementById("statename").innerHTML = data;
				document.getElementById("statename").value.innerHTML = data;

			}
		});
		return true;
	}

	function LoginStatusCheck() {
		var status = $('#hdfLoginStatus').val();
		//alert(status);
		$('#hdfLoginStatus').val('Y');
		var status = $('#hdfLoginStatus').val();
		//alert(status);
		if (status != "N") {
			$("#loginDIV").hide();
			$("#billToDetailDIV").removeClass("col-sm-5");
			$("#billToDetailDIV").addClass("col-sm-8");
		}

		return false;
	}
</script>
</head>

<body>
	`
	<%
		List<Cart> list = (List<Cart>)request.getAttribute("list");
		
	%>

	<section id="cart_items">
		<form action="/e-commerce/consumer/proceedCheckoutNow" method="post">
			<div class="container">
				<div class="step-one">
					<h2 class="heading" style="width: 100%;">Billing Detail</h2>
				</div>
				<!--<div class="checkout-options">
					<h3>New User</h3>
					<p>Checkout options</p>
					<ul class="nav">
						<li><label><input type="checkbox"> Register
								Account</label></li>
						<li><label><input type="checkbox"> Guest
								Checkout</label></li>
						<li><a href=""><i class="fa fa-times"></i>Cancel</a></li>
					</ul>
				</div>
				/checkout-options

				<div class="register-req">
					<p>Please use Register And Checkout to easily get access to
						your order history, or use Checkout as Guest</p>
				</div>
				<!--/register-req-->

				<div class="shopper-informations">
					<div class="row">
						<div class="col-sm-3" id="loginDIV">
							<div class="shopper-info">
								<p>Shopper Information</p>
								<input type="text" placeholder="User Name"
									class="input-margin form-control input-sm"> <input
									type="password" placeholder="Password"
									class="input-margin form-control input-sm"> <input
									type="button" id="btnLohin" Value="Sign In"
									class="btn btn-success btn-sm pull-right">
								<!-- <a class="btn btn-primary" href="">Get Quotes</a>
							<a class="btn btn-primary" href="">Continue</a> -->
							</div>
						</div>
						<div class="col-sm-5 clearfix" id="billToDetailDIV">
							<div class="bill-to">
								<p>Bill To</p>
								<div class="form-one">

									<input type="text" id="customername" name="customername"
										placeholder="Name *" required="required"  	
										class="input-margin form-control input-sm"> 
										<input required="required"	
										type="email" id="customeremail" name="customeremail"
										placeholder="Email*" 
										class="input-margin form-control input-sm"> 
										<input required="required"	
										type="text" id="address1" name="address1"
										placeholder="Address 1 *"
										class="input-margin form-control input-sm"> 
										<input required="required"	type="text" id="address2" name="address2" 
										placeholder="Address 2" class="input-margin form-control input-sm" >

								</div>
								<div class="form-two ">

									<input type="text" id="pincode" name="pincode"
										 maxlength="6" onkeypress="return IsNumeric(event)"
										placeholder="Zip / Postal Code *" required="required"	
										class="input-margin form-control input-sm"> <select
										id="countryname" name="countryname"
										class="input-margin form-control input-sm">
										<option>-- Country --</option>
										
									</select> 
									<select id="statename" name="statename"
										class="input-margin form-control input-sm" onchange="return fillCity('statename', 'cityname');">
										<option>-- State State --</option>
										
									</select>
									<select id="cityname" name="cityname"
										class="input-margin form-control input-sm">
										<option>-- State City --</option>
										
									</select>  
									<input type="text" id="mobileno" name="mobileno"
										placeholder="Mobile Phone" required="required"	
										class="input-margin form-control input-sm" maxlength="10" onkeypress="return IsNumeric(event)">


								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="order-messages">
								<p>Shipping Order</p>
								<textarea name="shippingnote" id="shippingnote" name="shippingnote"
									class="input-margin form-control input-sm"
									placeholder="Notes about your order, Special Notes for Delivery"
									rows="9"></textarea>
								<label style="margin-top: 10px;"><input type="checkbox">
									Shipping to bill address</label>
							</div>
						</div>
					</div>
				</div>
				<div class="review-payment">
					<h2>Review & Payment</h2>
				</div>

				<div class="table-responsive cart_info">
					<table class="table table-condensed" id="test">
						<thead>
							<tr class="cart_menu">
								<td class="image">Item</td>
								<td class="description"></td>
								<td class="price">Price</td>
								<td class="quantity">Quantity</td>
								<td class="total">Total</td>
								<td></td>
							</tr>
						</thead>
						<tbody id="cartItem">
							<%
								int id=0;
													if (list != null && list.size() > 0)
													{
														int i = 1;
														String priceID = "hdfPrice" + i;
														String quantityID = "cart_quantity_input" + i;
														double ecototal=0;
														double shippingtotal =0;
														for (Cart dto : list)
														{ id++;
															String url = "";
															String CartID = String.valueOf(dto.getId());
															String aCartID = "aCartID" + String.valueOf(dto.getId());
															
															String aPlus = "aPlus" + String.valueOf(dto.getId());
															String cart_quantity_input = "cart_quantity_input" + String.valueOf(dto.getId());
															String aMinus = "aMinus" + String.valueOf(dto.getId());
															String quantityInput = "quantityInput" + String.valueOf(dto.getId());
															String priceLevel = "priceLevel" + String.valueOf(dto.getId());
															String hdfPrice = "hdfPrice" + String.valueOf(dto.getId());
															String trid = "tr" + String.valueOf(dto.getId());
															
															
															double total = dto.getProductPrice() * dto.getQuantity();
															ecototal = dto.getEcocost();
															shippingtotal = dto.getShippingcost();
															int cartid = dto.getId();
															if (dto != null && dto.getImageFile() != null)
															{
																url = "data:image/png;base64," + Base64.encode(dto.getImageFile());
															}
															else
															{
																url = "/e-commerce/resources/productlist/images/cart/one.png";
															}
															i = i + 1;
							%>
							<tr id="<%=trid%>">
								<td class="cart_product"><a href=""><img
										src="/e-commerce/resources/productlist/images/cart/one.png"
										alt=""></a></td>

								<td class="cart_description">
									<h4>
										<a href=""><%=dto.getProductName()%></a>
									</h4>
									<p>
										Web ID: <%=dto.getProductID()%></p>
								</td>
								<td class="cart_price">
									<p><%=dto.getProductPrice()%></p> <input type="hidden"
									id="<%=hdfPrice%>" name="" Value="<%=dto.getProductPrice()%>">
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button">
										<a class="cart_quantity_up" href="" id="<%=aPlus%>"
											style="padding: 5px; text-decoration: none;"
											onclick="return QuantityPlus('<%=cartid%>','<%=cart_quantity_input%>','<%=priceLevel%>','<%=hdfPrice%>','#priceLevelsId<%=id%>');">
											+ </a> <input class="cart_quantity_input" type="text"
											name="quantity" id="<%=cart_quantity_input%>"
											name="cart_quantity_input"
											onkeypress="return IsNumeric(event)"
											value="<%=dto.getQuantity()%>" autocomplete="off" size="1">
										<a class="quantity_down" href=""
											style="padding: 5px; text-decoration: none;" id="<%=aMinus%>"
											onclick="return QuantityMinus('<%=cartid%>','<%=cart_quantity_input%>','<%=priceLevel%>','<%=hdfPrice%>','#priceLevelsId<%=id%>');">
											- </a>
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price"
										style="font-weight: normal; font-size: 19px;">
										<i class='fa fa-inr'
											style="font-weight: normal; font-size: 17px;"></i><input
											type="hidden" id="priceLevelsId<%=id%>" value="<%=total%>">
										<span id="<%=priceLevel%>"><%=total%></span>
									</p>
									<input type="hidden" value="<%=ecototal%>" id="ecototal<%=id%>">
									<input type="hidden" value="<%=shippingtotal%>" id="shippingtotal<%=id%>">
								</td>
								<td class="cart_delete"><a class="cart_quantity_delete"
									href=""
									onclick="return RemoveItemsFromCart(this.id,'<%=cartid%>');"
									id="<%=aCartID%>"> <i class="fa fa-times"></i></a> <input
									type="hidden" id="<%=CartID%>" value="<%=cartid%>"></td>
							</tr>



							<%
								}
													}
							%>
							<!-- <tfoot></tfoot> -->
							<tr>
								<td colspan="4">&nbsp;</td>
								<td colspan="2">
									<table class="table table-condensed total-result">
										<tr>
											<td>Cart Sub Total</td>
											<td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="subtotal">61</span></td>
										</tr>
										<tr>
											<td>Exo Tax</td>
											<td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="ecototalHTML">0.00</span></td>
										</tr>
										<tr class="shipping-cost">
											<td>Shipping Cost</td>
											<td><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="shippingtotalHTML">0.00</span></td>
										</tr>
										<tr>
											<td>Total</td>
											<td><script type="text/javascript">
												$(document).ready(function() {
													collectToTotalValue();
												});
											</script><i class='fa fa-inr' style="font-weight: normal; font-size: 14px; color: orange;"></i>&nbsp;<span id="total"></span></td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="payment-options">
					<span> <label><input type="checkbox"> Direct
							Bank Transfer</label>
					</span> <span> <label><input type="checkbox"> Check
							Payment</label>
					</span> <span> <label><input type="checkbox">
							Paypal</label>
					</span> <span> <input type="submit" id="btnSubmit" value="Continue"
						class="btn btn-primary pull-right"> <!-- <a class="btn btn-primary pull-right" style="border: 1px solid red;" href="">Continue</a> -->
					</span>
				</div>
			</div>
		</form>
	</section>
	<!--/#cart_items-->

	<script type="text/javascript">
		/* function QuantityPlus(cartID, value, pricelevel, hdfPrice,id) {
			//alert(value + "  --"+pricelevel+"-  " + hdfPrice);
			var val = parseInt(document.getElementById(value).value);
			var productPrice = parseFloat(document.getElementById(hdfPrice).value);
			val = val + 1;
			var total = productPrice * val;
			//alert(val + "  ---  " + total);
			//var Cart=[{ 'id':'1', 'quantity': val, 'productPrice':productPrice }];
			$.ajax({
				type : "POST",
				url : "/e-commerce/consumer/proceedCartUpdate",
				//dataType: 'JSON',
				data : {
					'id' : cartID,
					'quantity' : val
				},
				success : function(data) {
					//alert("success");
					//alert(data);
				},
				error : function(data) {
					alert("error");
				}
			});
			document.getElementById(value).value = val;
			$('#' + pricelevel).html(total);
			$(id).val(total);
			collectToTotalValue();
			//document.getElementById('priceLevel').value=val;
			return false;
		}

		function QuantityMinus(cartID, value, pricelevel, hdfPrice,id) {
			var val = parseInt(document.getElementById(value).value);
			if (val != 0) {

				val = val - 1;
				var productPrice = parseFloat(document.getElementById(hdfPrice).value);
				var total = productPrice * val;
				
				//alert(val + "  ---  " + total);
				//var Cart=[{ 'id':'1', 'quantity': val, 'productPrice':productPrice }];
				$.ajax({
					type : "POST",
					url : "/e-commerce/consumer/proceedCartUpdate",
					// dataType: 'JSON',
					data : {
						'id' : cartID,
						'quantity' : val
					},
					success : function(data) {
						//alert("success");
						//alert(data);
					},
					error : function(data) {
						alert("error");
					}
				});
				document.getElementById(value).value = val;
				$('#' + pricelevel).html(total);
				$(id).val(total);
				collectToTotalValue(); 
			}
			return false;
		}

		function collectToTotalValue() {
			var tbl = document.getElementById('cartItem');
			var lastRow = tbl.rows.length;
			var iteration = lastRow; 
			var totalAmount = 0;
			var idName = new Array("", "#priceLevelsId");

			var priceLevel=0;

			for (var i = 1; i < iteration; i++) {
				var priceLevelInner =0;

				for (var id = 1; id < idName.length; id++) {

					if (id == 1) {
						priceLevelInner = $(idName[id] + i).val();
					}

					if (id == (idName.length) - 1) {
						priceLevel = parseFloat(priceLevelInner);
					} else {
						priceLevel =parseFloat(priceLevelInner);
					}
					totalAmount+= priceLevel;
				} 

			}
			document.getElementById("subtotal").innerHTML = totalAmount;
			return totalAmount;
		} */
	</script>

	<script src="/e-commerce/resources/productlist/js/jquery.js"></script>
	<script src="/e-commerce/resources/productlist/js/bootstrap.min.js"></script>
	<script src="/e-commerce/resources/productlist/js/jquery.scrollUp.min.js"></script>
	<script src="/e-commerce/resources/productlist/js/jquery.prettyPhoto.js"></script>
	<script src="/e-commerce/resources/productlist/js/main.js"></script>
</body>
</html>