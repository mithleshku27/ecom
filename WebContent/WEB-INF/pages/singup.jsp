<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<title>Welcome Itech Mass Pvt. Ltd.</title>
<head>
<link type="text/css" href="/e-commerce/resources/css/bootstrap.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css"
	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css"
	rel="stylesheet" media="screen" />
<link href="/e-commerce/resources/css/main.css" rel="stylesheet">
<link href="/e-commerce/resources/css/timepicki.css" rel="stylesheet">
<script src="/e-commerce/resources/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/js/jquery-ui.js"></script>
<script src="/e-commerce/resources/js/angular.min.js"></script>
<link href="/e-commerce/resources/jtable/css/jquery.datetimepicker.css"
	rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery.datetimepicker.js"
	type="text/javascript"></script>
<link rel="icon" type="image/png" sizes="96x96"
	href="/e-commerce/resources/images/app-fevicon.png">
</head>
<body>

	<!-- favicon links -->

	<script type="text/javascript">
		function bookRequest($scope, $http) {
			$scope.submits = function() {
				var uPassword1 =$scope.uPassword1
				var uPassword = $scope.uPassword;
				if (uPassword1 != uPassword) {
					alert("Please enter the valide Retype-Password.");
				} else {
					var formData = {
						"email" : $scope.email,
						"fristName" : $scope.fristName,
						"mobile" : $scope.mobile,
						"contactDetails" : $scope.contactDetails,
						"users" :$scope.fristName,
						"uPassword" : $scope.uPassword
					};

					var response = $http
							.post('/e-commerce/consumer/signupproceed.json',
									formData);
					response.success(function(data, status, headers, config) {
						$scope.rb = data;
					});
					response.error(function(data, status, headers, config) {
						alert("Exception details: " + JSON.stringify({
							data : data
						}));
					});

				};

			}

		}
	</script>
</head>
<body>

	<div style="width: 100%; float: left; margin: auto;">
		<div class="hs_page_title">
			<div class="container">
				<div class="breadcrumb2 flat" style="color: black;">
					<h3>Sing UP Details</h3>

				</div>
			</div>
		</div>
	</div>
	<div class="clearfix hs_margin_10"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form ng-submit="submits()" ng-controller="bookRequest">
					<div class="hs_tab">
						<div class="breadcrumb2 flat">
							<!--  <span class="active" id="travellerDetailsLabel"> </span> -->
							<span id="travellerDetailsLabel"> Personal's
								Details/Review Details </span>
						</div>
						<!--////////////////Email Address start div/////////////////////////-->
						<fieldset class="fieldset2 col-lg-12" id="emaildiv">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12">
									<div style="margin-bottom: 6px;">Email Address</div>
									<input id="Eail" ng-model="email" class="form-control input-sm"
										style="width: 100%;" placeholder="Email Address"
										required="required" /><br />

								</div>
								<div class="clearfix margin_10"></div>
						</fieldset>

						<!--////////////////Email Address end div/////////////////////////-->
						<!--////////////////Traveller's details start div/////////////////////////-->

						<fieldset class="fieldset2 col-lg-12" id="personal">
							<div class="col-lg-12 col-md-12 col-sm-12"
								style="background-color: #f1f1f1;">
								<h3>Personal's details</h3>
							</div>
							<div class="clearfix margin_10"></div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12">
									<div style="margin-bottom: 6px;">Name</div>
									<input id="fristName" ng-model="fristName"
										class="form-control input-sm" style="width: 100%;"
										placeholder="Name" required="required" />
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<div style="margin-bottom: 6px;">Phone</div>
									<input type="number" id="mobile" ng-model="mobile"
										class="form-control input-sm" style="width: 100%;"
										placeholder="Phone" required="required" />
								</div>

							</div>
							<div class="clearfix margin_10"></div>
							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-12">
									<div style="margin-bottom: 6px; margin-left: 4px;">Address</div>
									<textarea ng-model="contactDetails" id="contactDetails"
										class="form-control input-sm" placeholder="Address"
										style="margin-left: 4px; height: 45px; width: 100%;"
										required="required"></textarea>
								</div>
							</div>
							<div class="clearfix margin_10"></div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12">
									<div style="margin-bottom: 6px;">Password</div>
									<input  type="password"  id="uPassword1" ng-model="uPassword1" class="form-control input-sm" style="width: 100%;" placeholder="User Name" required="required" />
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<div style="margin-bottom: 6px;">Retype-password</div>
									<input type="password" id=uPassword ng-model="uPassword" class="form-control input-sm" style="width: 100%;" placeholder="password" required="required" />
								</div>

							</div>
							<div class="clearfix margin_10"></div>

							<div class="clearfix margin_10"></div>
							<div class="col-lg-4 col-md-4 col-sm-12">
								<div
									style="margin-top: 25px; font-size: 14px; text-align: left; margin-left: 0px;">
									<div class="col-lg-4 col-md-4 col-sm-12">
										<div
											style="margin-top: 25px; font-size: 14px; text-align: left; margin-left: 0px;">
											<input type="submit" class="btn btn-danger btn-sm"
												value="Sing Up">
										</div>
									</div>
									<!--  <button class="btn btn-danger btn-lg"  ng-click='travellerContinue()'></button>  -->
								</div>
							</div>
					</div>
					</fieldset>

					<!--////////////////Traveller's details end div//////////////////////-->
					<!--////////////////Review Details start div/////////////////////////-->

					<fieldset class="fieldset2 col-lg-12" id="reviewDiv">
						<div class="col-lg-12 col-md-12 col-sm-12"
							style="background-color: #f1f1f1;">
							<h3>Review Details</h3>
							<h4>Sing up {{rb.id}}</h4>
						</div>
						<div class="clearfix margin_10"></div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12">
								Email: <label id="travellerName">{{rb.email}}</label>
							</div>
							<div class="clearfix margin_10"></div>
							<div class="col-lg-10 col-md-11 col-sm-12">
								First Name: <label id="travellerName">{{rb.firstName}}</label>
							</div>
							<div class="clearfix margin_10"></div>
							<div class="col-lg-2 col-md-2 col-sm-12">Mobile:</div>
							<div class="col-lg-10 col-md-11 col-sm-12">
								<label id="fromLocationLbl">{{rb.mobile}} </label>
							</div>
							<div class="clearfix margin_10"></div>
							<div class="col-lg-2 col-md-2 col-sm-12">Address:</div>
							<div class="col-lg-10 col-md-11 col-sm-12">
								<label id="rideTime"> {{rb.contactDetails}} </label>
							</div>


							<div class="clearfix margin_10"></div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div style="margin-top: 10px;">Coupon Code:</div>
							</div>
							<div class="col-lg-10 col-md-11 col-sm-12">
								<div class="row" style="margin-left: -12px;">
									<div class="col-lg-3 col-md-3 col-sm-12">
										<input type='text' id="" class="form-control input-lg"
											style="width: 100%;" placeholder="Coupon code" />
									</div>
									<div class="col-lg-2 col-md-2 col-sm-12">
										<button class="btn btn-danger btn-lg">Verify</button>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div style="margin-top: 4px; text-align: left;">
											<input name="" class="" type="checkbox" value="">
											&nbsp;I've read and I accept the <a
												href="javascript:void(0);">terms and conditions.</a>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="clearfix margin_10"></div>
					</fieldset>
					<!--////////////////Review Details end div/////////////////////////-->
			</div>
			</form>

		</div>
	</div>
	</div>
	<div class="clearfix margin_30"></div>
	<div class="clearfix margin_30"></div>




</body>
</html>