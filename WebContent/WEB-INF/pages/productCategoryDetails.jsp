
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link
	href="/e-commerce/resources/jtable/css/jquery-ui-1.10.3.custom.css"
	rel="stylesheet" type="text/css" />
<script src="/e-commerce/resources/jtable/js/jquery-1.8.2.js"
	type="text/javascript"></script>
<script src="/e-commerce/resources/jtable/js/jquery-ui-1.10.3.custom.js"
	type="text/javascript"></script>
<link rel="icon" type="image/png" sizes="96x96"
	href="/e-commerce/resources/images/app-fevicon.png">
</head>
<body>

	<script type="text/javascript">
		function addProductCategory($scope, $http) {
			$scope.submits = function() {
				var formData = {
					"productName" : $scope.productName,
					"cotegoryName" : $scope.cotegoryName,
					"cotegoryDetials" : $scope.cotegoryDetials,
					"productDescripion" : $scope.productDescripion
				};
				alert($scope.productName + " " + $scope.cotegoryName + " "
						+ $scope.cotegoryDetials);
				var response = $http.post('/e-commerce/admin/savecd.json',
						formData);
				response.success(function(data, status, headers, config) {
					$scope.rb = data;
				});
				response.error(function(data, status, headers, config) {
					alert("Exception details: " + JSON.stringify({
						data : data
					}));
				});
			};

		}
	</script>
	<div class="container">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form ng-submit="submits()" ng-controller="addProductCategory">
					<fieldset class="borderManager table-responsive">
						<legend class="borderManager glyphicon glyphicon-euro red"
							style="font-size: 24px; color: blue;">
							Category Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
								style="color: green;"></span>
						</legend>


						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-gift" style="color: blue;"></i>
								</span> <input type="text" id="productName" ng-model="productName"
									class="form-control" placeholder="Product Name" required>
							</div>
						</div>


						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;"><i
									class="glyphicon glyphicon-gift red" style="color: blue;"></i></span>
								<input ng-model="productDescripion" id="productDescripion"
									class="form-control"   placeholder="Product Description as Size,Length,Quanity"
									required>
							</div>
						</div>


						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;">
									<i class="glyphicon glyphicon-list-alt red"
									style="color: blue;"></i>
								</span> <input type="text" id="cotegoryName" ng-model="cotegoryName"
									class="form-control" placeholder="Category" required>
							</div>
						</div>
						<!-- Category Name Category Details -->

						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="input-group input-group-lg">
								<span class="input-group-addon"
									style="margin-bottom: 6px; width: 10%; text-align: left;"><i
									class="glyphicon glyphicon-comment red" style="color: green;"></i></span>
								<input ng-model="cotegoryDetials" id="cotegoryDetials"
									class="form-control" placeholder="Category Details" required>
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="col-lg-12">
								<button type="submit" class="btn btn-danger btn-lg"
									id="linkLogin">Add Category</button>
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="clearfix margin_10"></div>
						<!-- <div ng-show="user.age != ''">
							<h1>{{ user.name | capitalize }}</h1>
							<span class="age">Age: {{ user.age }}</span> <span
								class="location">Location: {{ user.location }}</span>
						</div>
 -->
						<div class="col-lg-10 col-md-11 col-sm-12" ng-show="rb.ctId !=0">
							<label id="fromLocationLbl" >{{rb.productName}}</label>
						</div>
					</fieldset>
				</form>

				<div class="clearfix"></div>
				<div class="clearfix margin_10"></div>
				<div class="col-lg-3 col-md-3 col-sm-12">
					<div class="col-lg-12"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>