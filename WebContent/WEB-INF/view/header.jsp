
<%@page import="com.itech.model.ClientSignUp"%>
<%@page import="com.itech.model.UserDetails"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<header id="header" class="gradient4Color"
	style="background-color: #f7f7f7;">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-12">
				<div id="hs_logo">
					<a href="#"> <img src="/e-commerce/resources/images/logo.png"
						alt="">
					</a>

				</div>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-12">
 				<% ClientSignUp ud = (ClientSignUp) session.getAttribute("ClientSignUp");%>
					<nav>
					<ul class="hs_menu collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-top: 8px;">
					<%if(ud!=null){ %>
						 
						<li><a href="javascript:void(0);" class="linkmenu">Welcome, <%=ud.getFirstname() %></a></li>
						<li><a href="/e-commerce/admin/logout" class="linkmenu">Logout</a></li> 
						<%} %>
					 
					</ul>
				</nav>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<div class="hs_social">
					<ul>
						<li><a href="javascript:void(0);"
							style="color: #ffffff; background-color: #314987"><i
								class="fa fa-facebook"></i></a></li>
						<li><a href="javascript:void(0);"
							style="color: #ffffff; background-color: #00b2e4"><i
								class="fa fa-twitter"></i></a></li>
						<li><a href="javascript:void(0);"
							style="color: #ffffff; background-color: #d01820"><i
								class="fa fa-google-plus"></i></a></li>
						<li><a href="javascript:void(0);"
							style="color: #ffffff; background-color: #329edc"><i
								class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 nav_crm">

				<nav id="nav" role="navigation">
					<a href="#nav" title="Show navigation"><i class="fa fa-bars"></i></a>
					<a href="#" title="Hide navigation"><i class="fa fa-bars"></i></a>
					<ul class="hs_menu" id="bs-example-navbar-collapse-1">
						<li class="glyphicon glyphicon-home red"><a
							href="/e-commerce/admin/login">Home</a></li>

						<li><a href="javascript:void(0);" class="drop-arrow">Store Setting</a>
							<ul>
								<li><a href="itemDetails">Stock</a>
									<ul style="width: 200%;">
										<li>Manufacturing</li>
										<li>Packaging</li> 
										<li><a href="/e-commerce/admin/supplierdetail">Supplier</a></li>
										<li>delivery</li>
										<li>Pricing</li>
										<li><a href="openstock">Product In Stock</a></li>
									</ul></li>
								<li><a href="itemDetails">Catalogs</a>
									<ul style="width: 200%;">
										<li>Product Listing Pricing</li>
										<li>Packaging Info</li>
										<li>Supplier Info</li>
										<li>Delivery Info</li>
									</ul></li>
								<li><a href="itemDetails">Contents</a>
									<ul style="width: 200%;">
										<li> <a href="openpd">Product Details content</a></li>
										<li>Packaging contents</li>
										<li>Supplier Contents</li>
									</ul></li>
								<li><a href="itemDetails">Facility</a>
									<ul style="width: 200%;">
										<li>Product Warranty facility</li>
										<li>Product Replacement facility</li>

									</ul></li>
								<li><a href="getItemDetails">Order Details</a></li>
								<li><a href="javascript:void(0);">Order Details</a></li>
								<li><a href="javascript:void(0);">Party Details</a></li>
								<li><a href="javascript:void(0);">Accounting</a></li>
								<li><a href="#"> </a></li>
								</ul>
								<li><a href=javascript:void(0); class="drop-arrow">Product for sale</a>
									<ul>
										<li><a href="opencd">Category</a></li>
										<li><a href="subcd">Sub Category</a></li>
										<li><a href="itemDetails">Add Product </a></li>
										<li><a href="getItemDetails" class="drop-arrow">Product	Re-View </a></li>
									</ul></li>
								<li><a href="javascript:void(0);" class="drop-arrow">Wholesale</a>
									<ul>
										<li><a href="productHoleSales">Sale</a></li>
										<li><a href="pincodedetail">Pin code/Area Allocation</a></li>
										 
									</ul></li>


							</ul>
				</nav>
			</div>

		</div>
	</div>




</header>
