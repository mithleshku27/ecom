 
<%@page import="com.itech.model.UserDetails"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
 <div style="width: 100%; float: left; margin: auto; position: relative; z-index: 999999;">
		<header id="header" class="gradient4Color"
			style="background-color: #f7f7f7;">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-12">
					<div id="hs_logo">
						<a href="javascript:void(0);"> <img src="/e-commerce/resources/images/logo.png"
							alt="">
						</a>
					</div>
				</div>

				<div class="col-lg-9 col-md-9 col-sm-12">
				<% UserDetails ud = (UserDetails) session.getAttribute("userDetails");%>
					<nav>
					<ul class="hs_menu collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-top: 6px;">
					<%if(ud==null){ %>
						<li><a href="/e-commerce/consumer/signin" class="linkmenu">Sign In</a></li>
						<li><a href="/e-commerce/consumer/signup" class="linkmenu">Sign Up</a></li>
						<%}else{ %>
						<li><a href="javascript:void(0);" class="linkmenu">Welcome, <%=ud.getFristName() %></a></li>
						<li><a href="/e-commerce/consumer/logout" class="linkmenu">Logout</a></li>
						<li><a href="/e-commerce/consumer/openCart" class="linkmenu" id="cartSize">Cart : 0</a></li> 
						<%} %>
					</ul>
					</nav>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="hs_social">
						<ul style="margin-top: 17px;">
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #314987"><i
									class="fa fa-facebook"></i></a></li>
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #00b2e4"><i
									class="fa fa-twitter"></i></a></li>
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #d01820"><i
									class="fa fa-google-plus"></i></a></li>
							<li><a href="javascript:void(0);"
								style="color: #ffffff; background-color: #329edc"><i
									class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 nav_crm">

					<nav id="nav" role="navigation"> <a href="#nav"
						title="Show navigation"><i class="fa fa-bars"></i></a> <a href="javascript:void(0);"
						title="Hide navigation"><i class="fa fa-bars"></i></a>

					<ul class="hs_menu " id="bs-example-navbar-collapse-1">
						<li class="glyphicon glyphicon-home red"><a
							href="javascript:void(0);">Home</a></li>


						<li><a href="javascript:void(0);"
							class="drop-arrow glyphicon glyphicon-shopping-cart red">
								Women,Baby & Kids</a>

							<ul>
								<li><a href="javascript:void(0);" class="drop-arrow">Women</a>

									<ul>
										<li> <a href="/e-commerce/consumer/view?productCategory=women">Dresses</a>
										
										<li><a href="javascript:void(0);" class="drop-arrow">Top & Tees</a></li>

										<li><a href="javascript:void(0);" class="drop-arrow">Saari </a></li>
										<li><a href="/e-commerce/consumer/orderdetails" class="drop-arrow">Order details </a></li>
  
										<li><a href="javascript:void(0);">Sweaters</a></li>
										<li><a href="javascript:void(0);">Fashion Hoodies & Sweetshirt</a></li>

										<li><a href="javascript:void(0);">Jeans</a></li>
										<li><a href="javascript:void(0);">pants</a></li>
										<li><a href="javascript:void(0);">skirts </a></li>

										<li><a href="javascript:void(0);">shorts</a></li>
										<li><a href="javascript:void(0);">Leggins</a></li>
										<li><a href="javascript:void(0);">Active</a></li>
										<li><a href="javascript:void(0);">Coats,Jackets & Vests </a></li>
										<li><a href="javascript:void(0);">suiting & Blazers</a></li>
										<li><a href="javascript:void(0);">Socks & Hosiery</a></li>

									</ul></li>
								<li><a href="javascript:void(0);"> Baby & kids clothing</a>

									<ul>

										<li><a href="javascript:void(0);">Boys clothing</a></li>


										<li><a href="javascript:void(0);">Girls Clothing</a></li>



										<li><a href="javascript:void(0);">Baby Boys Clothing</a></li>
										<li><a href="javascript:void(0);">Baby Girls Clothing</a></li>

										<li><a href="javascript:void(0);">Combo Pack</a></li>
										<li><a href="javascript:void(0);">Character Clothing</a></li>
									</ul></li>


								<li><a href="javascript:void(0);" class="drop-arrow">Shop Clothing by Age</a>

									<ul>
										<li><a href="javascript:void(0);">0-2 Years</a></li>
										<li><a href="javascript:void(0);">2-6 Years</a></li>
										<li><a href="javascript:void(0);">6-10 Years</a></li>
										<li><a href="javascript:void(0);">10+ Years</a></li>
									</ul></li>


								<li><a href="javascript:void(0);" class="drop-arrow">Kids and Baby
										Footwear</a>

									<ul>
										<li><a href="javascript:void(0);">Boys Footwear</a></li>
										<li><a href="javascript:void(0);">Girls Footwear</a></li>
										<li><a href="javascript:void(0);">Baby Footwear</a></li>

									</ul></li>


							</ul></li>

						<!-- <li><a href="javascript:void(0);" class="drop-arrow">Lingerie
								& Sleepwear</a>

							<ul>
								<li><a href="javascript:void(0);" class="drop-arrow">MEN</a>

									<ul>

										<li><a href="/e-commerce/booking/consumerBookrideV2V">Underwear</a></li>


										<li><a href="javascript:void(0);" class="drop-arrow">Vests</a></li>



										<li><a href="javascript:void(0);">Sleepwear</a></li>
										<li><a href="javascript:void(0);">Hipsters</a></li>

										<li><a href="javascript:void(0);">Trunks</a></li>
										<li><a href="javascript:void(0);">Boxers</a></li>

										<li><a href="javascript:void(0);">Pyjama</a></li>
										<li><a href="javascript:void(0);">Nightwear Set</a></li>

										<li><a href="javascript:void(0);">Bathrobe</a></li>
										<li><a href="javascript:void(0);">Bermuda</a></li>

										<li><a href="javascript:void(0);">Capri</a></li>

									</ul></li>
								<li><a href="javascript:void(0);" class="drop-arrow">Cloths</a>
									<ul>

										<li><a href="javascript:void(0);">T-Shirt </a></li>
										<li><a href="javascript:void(0);" class="drop-arrow">Shirts</a></li>
										<li><a href="javascript:void(0);">Jeens</a></li>
										<li><a href="javascript:void(0);">Traogar</a></li>
										<li><a href="javascript:void(0);">Sports Bear</a></li>
										<li><a href="javascript:void(0);">Sharee</a></li>

									</ul>
							</ul></li>
							 -->
							
							<li><a href="javascript:void(0);" class="drop-arrow">Flowers</a>
                          	<ul>
								<li><a href="/e-commerce/consumer/view?productCategory=Flower" class="drop-arrow">Roses</a>
								
									<ul>
										<li><a href="/#">Carnations</a></li>
										<li><a href="javascript:void(0);" class="drop-arrow">Gerberas</a></li>
										<li><a href="javascript:void(0);">Lillies and Orchids</a></li>
										<li><a href="javascript:void(0);">Mixed Flowers</a></li>
										<li><a href="javascript:void(0);">Good Luck Plants</a></li>

									</ul></li>
								<li><a href="javascript:void(0);" class="drop-arrow">Combos</a>
									<ul>
										<li><a href="javascript:void(0);">Flowers with Cakes</a></li>
										<li><a href="javascript:void(0);">Flowers with Chocolates</a></li>
										<li><a href="javascript:void(0);">Flowers with Sweets</a></li>
										<li><a href="javascript:void(0);">Flowers with Dryfruits</a></li>
										<li><a href="javascript:void(0);">Flowers with Teddy</a></li>
									</ul>
							</ul></li>
					</ul>

					</nav>
				</div>

			</div>
		</div>




		</header>
	</div>
	
</body>
</html>
