<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- All CSS Styles --> 


<link type="text/css" href="/e-commerce/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/animate.css" rel="stylesheet" media="screen" /> 
<link type="text/css" href="/e-commerce/resources/css/owl.carousel.css" rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/owl.theme.css"	rel="stylesheet" media="screen" />
<link type="text/css" href="/e-commerce/resources/css/style.css"	rel="stylesheet" media="screen" /> 
<link rel="icon" type="image/png" sizes="96x96" href="/e-commerce/resources/images/app-fevicon.png">
<link type="text/css" href="/e-commerce/resources/css/media.css"	rel="stylesheet" media="screen" />
<title><tiles:insertAttribute name="title" ignore="true"></tiles:insertAttribute>
</title>
</head>
<body>
	<div style="width: 100%; float: left; margin: auto;">
		<header id="header" class="gradient4Color"	style="background-color: #f7f7f7;"> 
		<tiles:insertAttribute name="header"></tiles:insertAttribute>
		<div class="clearfix margin_1"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12"
					style="margin-buttom: 20px;">
					<tiles:insertAttribute name="body"></tiles:insertAttribute>
				</div>
			</div>
		</div>
		</header>
		<div class="clearfix margin_05"></div>
		<tiles:insertAttribute name="footer"></tiles:insertAttribute>
	</div>
<script type="text/javascript" src="/e-commerce/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/e-commerce/resources/js/jquery.scrollUp.min.js"></script>
<script type="text/javascript" src='/e-commerce/resources/js/scripts-bottom.js'></script> 
</body>
</html>