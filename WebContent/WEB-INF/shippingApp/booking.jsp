<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Itech Mass Pvt. LTd.</title>
<link href="/e-commerce/resources/admin/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="/e-commerce/resources/admin/css/style.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="e-commerce Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<script src="/e-commerce/resources/admin/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/admin/js/bootstrap.js"></script>

<link rel="icon" type="image/png" sizes="96x96"
	href="/e-commerce/resources/images/app-fevicon.png">
<!---fonts-->
<link href='//fonts.googleapis.com/css?family=Voltaire' rel='stylesheet'
	type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!---fonts-->
<!--script-->
<link rel="stylesheet"
	href="/e-commerce/resources/admin/css/swipebox.css">
<script src="/e-commerce/resources/admin/js/jquery.swipebox.min.js"></script>

<!-- 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
    rel="stylesheet" type="text/css" /> - 
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"  type="text/javascript"></script> 
 -->
 
 <script type="text/javascript" src="/e-commerce/resources/dropdown/js/ui.dropdownchecklist-1.4-min.js"></script> 
<script type="text/javascript">
    $(function () {
    	 $("#name").dropdownchecklist( { maxDropHeight: 150 } );
        $('#lstFruits').multiselect({
            includeSelectAllOption: true
        });
    });
</script>


<script type="text/javascript">
	jQuery(function($) {
		$(".swipebox").swipebox();
	});
</script>
<!--script-->
</head>
<body>
	<!---header--->
	<div class="header">
		<div class="container">
			<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse"
						data-target="javascript:void(0);bs-example-navbar-collapse-1"
						aria-expanded="false">
						<i class="sr-only">Toggle navigation</i> <i class="icon-bar"></i>
						<i class="icon-bar"></i> <i class="icon-bar"></i>
					</button>
					<div class="navbar-brand">
						<h1>
							<a href="javascript:void(0);"> <img
								src="/e-commerce/resources/images/logo.png" alt=""></a>
						</h1>
					</div>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">

						<li><a href="/e-commerce/admin/login"
							class="glyphicon glyphicon-home red">Home</a></li>
						<li><a href="javascript:void(0);">About</a></li>
						<li class="dropdown active"><a href="javascript:void(0);"
							class="dropdown-toggle" data-toggle="dropdown" role="button"
							aria-haspopup="true" aria-expanded="false">Services<i
								class="caret"></i></a>
							<ul class="dropdown-menu">
								<li><a href="javascript:void(0);">Blog</a></li>
								<li><a href="javascript:void(0);">Pricing</a></li>
								<li><a href="javascript:void(0);">FAQ's</a></li>
								<li><a href="javascript:void(0);">History</a></li>
								<li><a href="javascript:void(0);">Support</a></li>
								<li><a href="javascript:void(0);">Template setting</a></li>
								<li><a href="javascript:void(0);">Login</a></li>
								<li><a href="javascript:void(0);">Portfolio</a></li>
							</ul></li>
						<li><a href="javascript:void(0);">Product</a></li>
						<li class="dropdown"><a href="javascript:void(0);"
							class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">AIMS<i
								class="caret"></i></a>
							<ul class="dropdown-menu" >
								<li><a href="javascript:void(0);">A</a></li>
								<li><a href="javascript:void(0);">B</a></li>
								<li><a href=javascript:void(0);">C</a></li>
								<li><a href="javascript:void(0);">D</a></li>
							</ul></li>

						<li><a href="javascript:void(0);">Contact</a></li>
					</ul>

				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid --> </nav>
		</div>
	</div>
	<!---header--->
	<!---login--->
	<div class="content">
		<div class="main-1">
			<div class="container">
				<div class="login-page">
					<div class="account_grid">
						<div class="col-md-6 login-left">
							<form action="ebookingcontinue" method="post">
							
							
							<div><h3>E-Booking  registered</h3>
							<p>please book now. Please fill needed request</p>
							 <span> <i style="color: blue; width: 2%;"></i> Services
								</span> 
							 <select name="name" id="name" class="form-control"	placeholder="Name" required multiple="multiple">
							 <option value="1">Bakery</option>
							 <option value="2">Courrier</option>
							 <option value="3">Delivery Boy</option>
							 </select>
							</div>
							<div>
								<span> <i style="color: blue; width: 2%;"></i> Name
								</span> <input type="text" name="name" id="name" class="form-control"	placeholder="Name" required>

							</div>
							<div>
								<span> <i style="color: blue; width: 2%;"></i> E-mail Address
								</span> <input type="text" name="email" id="email" class="form-control"	placeholder="Email Address" required>

							</div>

							<div>
								<span> <i style="color: blue; width: 2%;"></i> Mobile
								</span> <input type="text" name="mobile" id="mobile" class="form-control"	placeholder="Mobile" required>
<input type="submit" value="Continue">
							</div>
  
							<!-- <div>
								<span> <i style="color: blue; width: 2%;"></i>Shipping Address
								</span> <textarea  name="address" id="address" class="form-control"	placeholder="Address" required></textarea>

							</div> -->
							 
						</div>
						<div class="col-md-6 login-right">
							<h3>&nbsp;</h3>
							<p>&nbsp;</p>
						 </div>
						</form>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- login -->
	<!---footer--->

	<div class="footer-section">
		<div class="container">
			<div class="footer-grids">
				<!-- <div class="col-md-3 footer-grid">
								<h4>flickr widget</h4>
								<div class="footer-top">
									<div class="col-md-4 foot-top">
										<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f2.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f3.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="footer-top second">
									<div class="col-md-4 foot-top">
									<img src="images/f4.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f1.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="col-md-4 foot-top">
									<img src="images/f2.jpg" class="img-responsive" alt=""/>
									</div>
									<div class="clearfix"></div>
								</div> -->
			</div>

			<div class="col-md-3 footer-grid">

				<div class="clearfix"></div>
			</div>

			<div class="col-md-3 footer-grid">
				<h4>Get in touch</h4>
				<p>E-53/54</p>
				<p>Sector-3,Noida,Uttar Pradesh ,India</p>
				<p>Telephone : +</p>
				<p>Telephone : +</p>
				<p>FAX : +</p>
				<p>
					E-mail : <a href="itechmasssecommerce@gmail.com">
						itechmasssecommerce@gmail.com</a>
				</p>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="copy-section">
			<p>
				&copy; 2016 Itech Mass Pvt. Ltd. All rights reserved | Design by <a
					href="http://itechmass.com/"> ITECH MASS</a>
			</p>
		</div>
	</div>

	<!---footer--->
</body>
</html>