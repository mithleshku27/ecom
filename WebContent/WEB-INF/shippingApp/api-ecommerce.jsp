<%@page import="com.itech.model.Country"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Itech Mass Pvt. LTd.</title>
<link href="/e-commerce/resources/admin/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="/e-commerce/resources/admin/css/style.css" rel="stylesheet"
	type="text/css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="e-commerce Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<script src="/e-commerce/resources/admin/js/jquery-1.11.1.min.js"></script>
<script src="/e-commerce/resources/admin/js/bootstrap.js"></script>

<link rel="icon" type="image/png" sizes="96x96"
	href="/e-commerce/resources/images/app-fevicon.png">
<!---fonts-->
<link href='//fonts.googleapis.com/css?family=Voltaire' rel='stylesheet'
	type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!---fonts-->
<!--script-->
<link rel="stylesheet"
	href="/e-commerce/resources/admin/css/swipebox.css">
<script src="/e-commerce/resources/admin/js/jquery.swipebox.min.js"></script>

<!-- 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
    rel="stylesheet" type="text/css" /> - 
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"  type="text/javascript"></script> 
 -->
 
 <script type="text/javascript" src="/e-commerce/resources/dropdown/js/ui.dropdownchecklist-1.4-min.js"></script> 
<script type="text/javascript">
    $(function () {
    	 $("#name").dropdownchecklist( { maxDropHeight: 150 } );
        $('#lstFruits').multiselect({
            includeSelectAllOption: true
        });
    });
</script>


<script type="text/javascript">
	jQuery(function($) {
		$(".swipebox").swipebox();
	});
	
	
	
	function fillState()	
	{    var SelValue = document.getElementById("country").value;
	 
		 $.ajax({
		      type: "POST",
		      url: "/trux/reg/getCountryStates",
		      data:{
				  country:SelValue
				  } ,
		      success: function(data) {
		    	  document.getElementById("state").innerHTML = data;
		    	  document.getElementById("state").value.innerHTML=data;
		    	  document.getElementById("stateTo").innerHTML = data;
		    	  document.getElementById("stateTo").value.innerHTML=data;
		    	  
		      }
		    });
	    return true;
	}


	function fillCity()
	{
	    var stateId = document.getElementById("state").value;
		 $.ajax({
		      type: "POST",
		      url: "/fetch/city/by/cityid",
		      data:{
				  state:stateId
				  } ,
		      success: function(data) {
		    	  document.getElementById("city").innerHTML = data;
		    	  document.getElementById("city").value.innerHTML=data;
		      }
		    });
	    return true;
	}
</script>
<!--script-->
</head>
<body>
	<!---header--->
	<div class="header">
		<div class="container">
			<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse"
						data-target="javascript:void(0);bs-example-navbar-collapse-1"
						aria-expanded="false">
						<i class="sr-only">Toggle navigation</i> <i class="icon-bar"></i>
						<i class="icon-bar"></i> <i class="icon-bar"></i>
					</button>
					<div class="navbar-brand">
						<h1>
							<a href="javascript:void(0);"> <img
								src="/e-commerce/resources/images/logo.png" alt=""></a>
						</h1>
					</div>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">

						<li><a href="/e-commerce/admin/login"
							class="glyphicon glyphicon-home red">Home</a></li>
						<li><a href="javascript:void(0);">About</a></li>
						<li class="dropdown active"><a href="javascript:void(0);"
							class="dropdown-toggle" data-toggle="dropdown" role="button"
							aria-haspopup="true" aria-expanded="false">Services<i
								class="caret"></i></a>
							<ul class="dropdown-menu">
								<li><a href="javascript:void(0);">Blog</a></li>
								<li><a href="javascript:void(0);">Pricing</a></li>
								<li><a href="javascript:void(0);">FAQ's</a></li>
								<li><a href="javascript:void(0);">History</a></li>
								<li><a href="javascript:void(0);">Support</a></li>
								<li><a href="javascript:void(0);">Template setting</a></li>
								<li><a href="javascript:void(0);">Login</a></li>
								<li><a href="javascript:void(0);">Portfolio</a></li>
							</ul></li>
						<li><a href="javascript:void(0);">Product</a></li>
						<li class="dropdown"><a href="javascript:void(0);"
							class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">AIMS<i
								class="caret"></i></a>
							<ul class="dropdown-menu" >
								<li><a href="javascript:void(0);">A</a></li>
								<li><a href="javascript:void(0);">B</a></li>
								<li><a href=javascript:void(0);">C</a></li>
								<li><a href="javascript:void(0);">D</a></li>
							</ul></li>

						<li><a href="javascript:void(0);">Contact</a></li>
					</ul>

				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid --> </nav>
		</div>
	</div>
	<!---header--->
<div class="content">
		<div class="main-1">
			<div class="container">
				<div class="login-page" style="padding: 0px;">
					<div class="account_grid">
						<div class="col-md-6 login-left">
						<div><h3>Shipping Details</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix hs_margin_10"></div>
<div class="container">
<form action="proceed-api-ecommerce" method="POST"> 
	<div class="row">
    	<div class="col-lg-6 col-md-6 col-sm-12" > <!-- Column start -->
           				 
                        <div class="row">
                            	<div class="login-left" style="font-size: 8px; margin: 0px; padding-left: 35px; color: red;">
								<h3 style="color: green; padding: 0px;">From Address</h3>
								</div>										                                  
                                <!-- <div style="font-size: 20px; color: black; padding-left: 25px;">From Address</div> -->
                                
                        </div>
           				 
                        <div class="row"  style=" margin: 0px; padding: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Your Name</div>
                                <input  type="text"  id="nameFrom" name="nameFrom"  class="form-control input-sm" 
                                style="width:100%; " placeholder="Your Name"  required="required"/>
                            </div>  
                        </div>
                         
                        
                         <script>  
                             $(document).ready(function(){    																		 
									fillState();
    						}); 
    				        </script>
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Country/Location</div>
                                <!-- <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-user red" style="color: blue"></i></span> -->
                                <select class="form-control input-sm"  style="width: 100%;" id="countryFrom" name="countryFrom" style="width: 100%;" >
                                 <option value="">--Select Country--</option>
                                 <%List<Country> countriesList = (List<Country>)session.getAttribute("countryList");
									if(countriesList != null && !countriesList.isEmpty()){
									for(int i=0; i<countriesList.size();i++){ 										
									%>	
								<%if(countriesList.get(i).getValue().equals("India")){ %>
									 <option value="<%=countriesList.get(i).getValue() %>" selected="selected"><%=countriesList.get(i).getValue() %></option>
								<%}else{ %>
								 <option value="<%=countriesList.get(i).getValue() %>"><%=countriesList.get(i).getValue() %></option>
                                 <%	}}}%>
                                </select>
                            <!-- </div> --></div>  
                        </div>
                         
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Address</div>
                                
                                <textarea rows="4" cols="12"  id="address1From" name="address1From"  class="form-control input-sm" 
                                style="width:100%; resize: none;" placeholder="Name"  required="required"></textarea>
                            </div>  
                        </div>
                         
                         
                         
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">City</div>
                                <input  type="text"  id="cityFrom" name="cityFrom"  class="form-control input-sm" style="width:100%;" placeholder="City"  required="required"/>
                            </div>  
                        </div>
                         
                       <!--  <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">State</div>
                                <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-user red" style="color: blue"></i></span>
                                <select  class="form-control input-sm"  id="stateFrom" name="stateFrom" style="width: 100%;"  id="ddlState">
                                <option value="1">Bihar</option>
                                <option value="2">Goa</option>
                                <option value="3">UP</option>
                                </select>
                            </div></div>  
                        </div> -->
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                            <div class="row" style="margin: 0px;">
                            <div class="col-lg-8 col-md-6 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">State</div>
                                <!-- <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-user red" style="color: blue"></i></span> -->
                                <select  class="form-control input-sm"  id="stateFrom" name="stateFrom" style="width: 100%;"  id="ddlState">
                                <option value="1">Bihar</option>
                                <option value="2">Goa</option>
                                <option value="3">UP</option>
                                </select>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">&nbsp;</div>
                                &nbsp;
                            </div>
                            <div class="col-lg-3 col-md-5 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">Pin</div>
                                <input  type="text"  id="pinFrom" name="pinFrom"  class="form-control input-sm" style="width:100%;" placeholder="Pin"  required="required"/>
                            </div> 
                        </div>
                            </div> 
                        </div>
                         
                        <!-- <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Phone</div>
                                <input  type="text"  id="phoneFrom" name="phoneFrom"  class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div> 
                        </div> -->
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                            <div class="row" style="margin: 0px;">
                            <div class="col-lg-8 col-md-6 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">Phone</div>
                                <input  type="text"  id="phoneFrom" name="phoneFrom"  class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">&nbsp;</div>
                                &nbsp;
                            </div>
                            <div class="col-lg-3 col-md-5 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">Ext.</div>
                                <input  type="text"  id="extFrom" name="extFrom"  class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div> 
                        </div>
                            </div> 
                        </div>
                         
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Email notification</div>
                                <input  type="text"  id="emailFrom" name="emailFrom"  class="form-control input-sm" style="width:100%;" placeholder="Email notification(optional)"  required="required"/>
                            </div>
                        </div>
                         
                        <div class="clearfix margin_10"></div>
                         
                         
                       
        </div><!-- Column end -->
    <div class="col-lg-6 col-md-6 col-sm-12" > <!-- Column start -->
            
                        <div class="row" style="margin: 0px;">
                            											                                  
                              <div class="login-left" style="font-size: 8px; margin: 0px; padding-left: 15px; color: red;">
								<h3 style="color: green; padding: 0px;">To Address</h3>
								</div>	 
                        </div>
           				 
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Recipient Name</div>
                                 <input  type="text"  id="nameTo" name="nameTo"  class="form-control input-sm" style="width:100%;" placeholder="Recipient Name"  required="required"/>
                            </div>  
                        </div>
                          
                         
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Country/Location</div>
                                <!-- <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-user red" style="color: blue"></i></span> -->
                                <select class="form-control input-sm"  style="width: 100%;" id="countryTo" name="countryTo" style="width: 100%;" >
                                <% 
									if(countriesList != null && !countriesList.isEmpty()){
									for(int i=0; i<countriesList.size();i++){ 										
									%>	
								<%if(countriesList.get(i).getValue().equals("India")){ %>
									 <option value="<%=countriesList.get(i).getValue() %>" selected="selected"><%=countriesList.get(i).getValue() %></option>
								<%}else{ %>
								 <option value="<%=countriesList.get(i).getValue() %>"><%=countriesList.get(i).getValue() %></option>
                                 <%	}}}%>
                                </select>
                            <!-- </div> --></div>  
                        </div>
                         
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Address</div>
                                <textarea rows="4" cols="12"  id="address1To" name="address1To"  class="form-control input-sm" 
                                style="width:100%; resize: none;" placeholder="Address"  required="required"></textarea>
                            </div> 
                        </div>
                         
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">City</div>
                                <input  type="text"  id="cityTo" name="cityTo"  class="form-control input-sm" style="width:100%;" placeholder="City"  required="required"/>
                            </div> 
                        </div>
                        
                         <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                            <div class="row" style="margin: 0px;">
                            <div class="col-lg-8 col-md-6 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">State</div>
                                <!-- <div class="input-group input-group-sm">
								<span class="input-group-addon"><i	class="glyphicon glyphicon-user red" style="color: blue"></i></span> -->
                                <select  class="form-control input-sm"  id="stateTo" name="stateTo" style="width: 100%;"  id="ddlState">
                               
                                </select>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">&nbsp;</div>
                                &nbsp;
                            </div>
                            <div class="col-lg-3 col-md-5 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">Pin</div>
                                <input  type="text"  id="pinTo" name="pinTo"  class="form-control input-sm" style="width:100%;" placeholder="Pin"  required="required"/>
                            </div> 
                        </div>
                            </div> 
                        </div>
                        <!-- <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Phone</div>
                                <input  type="text"  id="phoneFrom" name="phoneFrom"  class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div> 
                        </div> -->
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                            <div class="row" style="margin: 0px;">
                            <div class="col-lg-8 col-md-6 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">Phone</div>
                                <input  type="text"  id="phoneFrom" name="phoneFrom"  class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">&nbsp;</div>
                                &nbsp;
                            </div>
                            <div class="col-lg-3 col-md-5 col-sm-12" style="margin: 0px; padding: 0px;">											                                  
                                <div style="margin-bottom:6px;">Ext.</div>
                                <input  type="text"  id="extTo" name="extTo"  class="form-control input-sm" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div> 
                        </div>
                            </div> 
                        </div>
                         
                        <div class="row" style="margin: 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Email notification</div>
                                <input  type="text"  id="emailTo" name="emailTo"  class="form-control input-sm" style="width:100%;" placeholder="Email notification(optional)"  required="required"/>
                            </div>  
                        </div>
                       
                        <div class="row" style="margin: 0px;">
                           <div class="col-lg-12 col-md-12 col-sm-12" >
                           <div style="margin-bottom:6px;">&nbsp;</div>
                           <a href="checkout">checkout</a>
    <input type="submit" class="btn btn-danger btn-sm  pull-right" value="Sing Up">
     </div> 
                        </div>
        </div><!-- Column end -->
     
</div>

</form> 
</div>
</body>
</html>