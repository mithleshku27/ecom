<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app> 
<title>Pickup It Now</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<head>


<link href="/pcm/resources/jtable/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="/pcm/resources/jtable/js/jquery.datetimepicker.js" type="text/javascript"></script> 
</head>
<!-- favicon links --> 
<script src="/pcm/resources/js/angular.min.js"></script>
 
<script type="text/javascript">
	function bookRequest($scope, $http) {
		$scope.list = []; 
		
		$scope.travellerContinue = function() {
			var formData = {  
					"email": $scope.email,
					 "username": $scope.username,
					 "phonenumber": $scope.phonenumber,
					 "vehicleType": $scope.vehicleType,
					 "bookingDate": $scope.bookingDate,
					 "bookingTime": $scope.bookingTime,
					  "pickupLocation": $scope.pickupLocation,
					  "fromLocation": $scope.fromLocation,
					  "dropLocation": $scope.dropLocation,
					  "toLocation": $scope.toLocation 
			 };

			var response = $http.post('/pcm/userrequest.json', formData);
			response.success(function(data, status, headers, config) {
			//	$scope.list.push(data);
				$scope.rb=data;
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});

			$scope.list = [];

		};
		  $scope.submits = function() {
			var formData = {  
					"email": $scope.email,
					 "username": $scope.username,
					 "phonenumber": $scope.phonenumber,
					 "vehicleType": $scope.vehicleType,
					 "bookingDate": $scope.bookingDate,
					 "bookingTime": $scope.bookingTime,					   
					  "pickupLocation": $scope.pickupLocation,
					  "fromLocation": $scope.fromLocation,
					  "dropLocation": $scope.dropLocation,
					  "toLocation": $scope.toLocation 
			 };

			var response = $http.post('/pcm/userbookingrequest.json', formData);
			response.success(function(data, status, headers, config) {
			//	$scope.list.push(data);
				$scope.rb=data;
			});
			response.error(function(data, status, headers, config) {
				alert("Exception details: " + JSON.stringify({
					data : data
				}));
			});

			$scope.list = [];

		};
 
		/* $scope.clearText = function() {
			$scope.msg.message = ''; 
		}; */

	}
</script>
</head>
<body>
  
<div style="width:100%; float:left; margin:auto;"> 
<div class="hs_page_title">
	<div class="container">
		<!-- <div class="row" style="margin-left:0px; margin-right:0; margin-top:5px;"> -->
		<div class="breadcrumb2 flat" style="color:black;">
    		<h3>Easiest way to book a Vehicle</h3>
    		 
		</div>
  	</div>
</div> 
</div>
<div class="clearfix hs_margin_10"></div>
<div class="container">  
	<div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12"> 
            <form ng-submit="submits()" ng-controller="bookRequest">
 				   <div class="hs_tab">
					  <div class="breadcrumb2 flat">
                        <span class="active" id="emailLabel">  Email</span>
                        <span id="travellerDetailsLabel">  Traveller Details</span>
                        <span id="reviewLabel"> Review Details</span>    
						<span></span>                    </div>                     
								     			
  <!--////////////////Email Address start div/////////////////////////-->					
                    <fieldset class="fieldset2 col-lg-12" id="emaildiv"> 						
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Email Address</div>
                                <input  id="Email" ng-model="email"  class="form-control input-lg" style="width:100%;" placeholder="Email Address" required="required" /><br/>
                               
                        </div>
                        <div class="clearfix margin_10"></div>   
                    </fieldset>

                    <!--////////////////Email Address end div/////////////////////////-->	
					<!--////////////////Traveller's details start div/////////////////////////-->	
  							
                    <fieldset class="fieldset2 col-lg-12" id="travellerDetail">                                    
                        <div class="col-lg-12 col-md-12 col-sm-12" style="background-color:#f1f1f1;">
                            <h3>Traveller's details</h3> 
                        </div>  
                        <div class="clearfix margin_10"></div> 
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Name</div>
                                <input  id="username" ng-model="username"  class="form-control input-lg" style="width:100%;" placeholder="Name"  required="required"/>
                            </div>  
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Phone</div>
                                <input  type="number" id="phonenumber"  ng-model="phonenumber" class="form-control input-lg" style="width:100%;" placeholder="Phone"  required="required"/>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Vehicle Category</div>
                                <select ng-model="vehicleType" id="vehicleType" class="input-lg" style="width:100%;"  required="required">
                                   <option value="Mahindra Champion">Mahindra Champion</option>
                                   <option value="Tata Ace">Tata Ace</option>   
                                   <option value="Maximo Pickup">Maximo Pickup</option>   
                                   <option value="Tata 407">Tata 407</option>   
                                   <option value="Tata 709">Tata 709</option>                                                                            
                                </select>
                             </div> 
                        </div>
                        <div class="clearfix margin_10"></div>  
                        
                        <div class="col-lg-12 col-md-12 col-sm-12" style="background-color:#f1f1f1;">
                            <h3>Pickup details</h3> 
                        </div>  
                        <div class="row">
                            <div class="clearfix margin_10"></div>  
                        <div class="col-lg-4 col-md-4 col-sm-12">                               
                                <div style="margin-bottom:6px;">Pickup Date</div>
                                <input  id="bookingDate" ng-model="bookingDate" class="form-control input-lg"  placeholder="YY-MM-DD"/>                               
							 </div>
							<div class="col-lg-4 col-md-4 col-sm-12">                               
                                <div style="margin-bottom:6px;">Pickup Time</div>
	    							<input id="bookingTime"  ng-model="bookingTime" class="form-control input-lg" style="width:100%;"placeholder="00/00/00" />
							  </div>  
                            </div>
                            
                        <div class="row">
                            <div class="clearfix margin_10"></div>                                       
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Pickup Locality</div>
                                <input ng-model="pickupLocation" id="pickupLocation" class="form-control input-lg" style="width:100%;" placeholder="Pickup Locality"  required="required"/>											
                            </div>  
                            <div class="col-lg-8 col-md-8 col-sm-12">											                                  
                                <div style="margin-bottom:6px; margin-left:4px;">Pickup Address</div>
                                <textarea ng-model="fromLocation" id="fromLocation" class="form-control input-lg" placeholder="Pickup Address" style="margin-left:4px;height:45px;width:100%;"  required="required"></textarea>
                            </div>
                        </div>                                        
                        <div class="clearfix margin_10"></div>  
                       <div class="col-lg-12 col-md-12 col-sm-12" style="background-color:#f1f1f1;">
                            <h3>Drop details</h3> 
                        </div>  
                        <div class="row">
                            <div class="clearfix margin_10"></div>                                   
                            <div class="col-lg-4 col-md-4 col-sm-12">											                                  
                                <div style="margin-bottom:6px;">Drop Locality</div>
                                <input ng-model="dropLocation" id="dropLocation"  class="form-control input-lg" style="width:100%;" placeholder="Drop Locality"  required="required"/>											
                            </div>  
                            <div class="col-lg-8 col-md-8 col-sm-12">											                                  
                                <div style="margin-bottom:6px; margin-left:4px;">Drop Address</div>
                                <textarea ng-model="toLocation" id="toLocation" class="form-control input-lg" placeholder="Drop Address" style="margin-left:4px;height:45px;width:100%;"  required="required"></textarea>
                             </div>                                            
                            <div class="clearfix margin_10"></div>   
                            <div class="col-lg-4 col-md-4 col-sm-12">     
                                <div style="margin-top:25px; font-size:14px; text-align:left; margin-left:0px;">
                                    <button class="btn btn-danger btn-lg"  ng-click='travellerContinue()'>Book</button>                               
                                </div>
                            </div>
                        </div>
                    </fieldset>

					<!--////////////////Traveller's details end div//////////////////////-->
					<!--////////////////Review Details start div/////////////////////////-->
                       							
                    <fieldset class="fieldset2 col-lg-12" id="reviewDiv"> 
                        <div class="col-lg-12 col-md-12 col-sm-12" style="background-color:#f1f1f1;">
                            <h3>Review Details</h3>  <h4>Booking  {{rb.rquestId}}</h4>
                        </div>
                        <div class="clearfix margin_10"></div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                Traveller 
                            </div> 
                            <div class="col-lg-10 col-md-11 col-sm-12">											                                  
                                <label id="travellerName">{{rb.username}}</label> 
                            </div> 
                            <div class="clearfix margin_10"></div>
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                Pickup From
                            </div>   <!--  username phonenumber vehicleType pickupLocation fromLocation dropLocation toLocation -->
                            <div class="col-lg-10 col-md-11 col-sm-12">											                                  
                                <label id="fromLocationLbl">{{rb.fromLocation}} </label>
                            </div>
                            <div class="clearfix margin_10"></div>
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                Pickup On
                            </div> 
                            <div class="col-lg-10 col-md-11 col-sm-12">											                                  
                                <label id="rideTime">
								 {{rb.bookingDate}} {{rb.bookingTime}}
							</label>
                            </div>
                            <div class="clearfix margin_10"></div>
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                Car Category
                            </div> 
                            <div class="col-lg-10 col-md-11 col-sm-12">											                                  
                                <label id="vehicleCat">{{rb.vehicleType}}</label>
                            </div>
                            <div class="clearfix margin_10"></div>
                            <div class="col-lg-2 col-md-2 col-sm-12">											                                  
                                <div style="margin-top:10px;">Coupon Code:</div>
                            </div> 
                            <div class="col-lg-10 col-md-11 col-sm-12">
                                <div class="row" style="margin-left:-12px;">
                                    <div class="col-lg-3 col-md-3 col-sm-12">	
                                        <input type='text' id=""  class="form-control input-lg" style="width:100%;" placeholder="Coupon code" />													
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12">	
                                        <button class="btn btn-danger btn-lg">Verify</button> 
                                    </div>		
                                    <div class="col-lg-12 col-md-12 col-sm-12">	
                                        <div style="margin-top:4px; text-align:left;"><input name="" class="" type="checkbox" value="">
                                            &nbsp;I've read and I accept the <a href="javascript:void(0);">terms and conditions.</a>
                                        </div>	
                                    </div>										
                                </div>	
                            </div>
                           <!--  <div class="col-lg-4 col-md-4 col-sm-12">     
                                <div style="margin-top:25px; font-size:14px; text-align:left; margin-left:0px;">
                                    <input type="submit" class="btn btn-danger btn-lg" value="Book">                               
                                </div>
                            </div> -->
                        </div>
                        <div class="clearfix margin_10"></div>   
                    </fieldset>
                    <!--////////////////Review Details end div/////////////////////////-->									
             	</div>  
            </form> 
        
       </div>		
  	</div>   
</div>
<div class="clearfix margin_30"></div>
<div class="clearfix margin_30"></div>
 <script src="/pcm/resources/js/timepicki.js"></script>
<script>
	$('#bookingTime').timepicki();
</script>
<script type="text/javascript">
$(function() {
  $('#bookingDate').datepicker({
          showOn: 'both',
          buttonImage:"/pcm/resources/images/calendar.png", 
          buttonImageOnly: true,
          dateFormat: "yy/dd/mm", 
          beforeShow: customRange,
          mandatory: true
  });
});
function onchangeCheckInDate() {
	if ($('#bookingDate').val() != "Check-in Date" && $("#bookingDate").datepicker("getDate") > $("#bookingDate").datepicker("getDate")) {
		$('#bookingDate').val($('#bookingDate').val());
	}
}
function onchangeCheckOutDate() {
	if ($('#bookingDate').val() != "Check-out Date" && $("#bookingDate").datepicker("getDate") > $("#bookingDate").datepicker("getDate")) {
		$('#bookingDate').val($('#bookingDate').val());
	}
}
function customRange(input) { 
	var date  = new  Date();
	var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();           
	return { 
	minDate: (input.id == "#bookingDate" ?
		$("#bookingDate").datepicker("getDate") : 
		new Date(y, m, d)), 
	maxDate: (input.id == "star.datepickerer" ? 
		$("#bookingDate").datepicker("getDate") : 
		null)
	 };
}
function blank(a) {
	if (a.value == a.defaultValue) {
		a.value="";
	}
}
function unblank(a) {
	if (a.value === "") {
		a.value = a.defaultValue;
	}
}
</script>
  
</body>
</html>
