<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PCM</title>
<style type="text/css">
.error {
	background-color: red;
}

.warning {
	background-color: yellow;
}
</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script> 
 http://localhost:8080/pcm/springcontent.json
 -->
<script src="/pcm/resources/js/angular.min.js"></script>
<script>
	function Hello($scope, $http) {
		$http.get('/pcm/springcontent.json').success(function(data) {
			$scope.user = data;
		});

		/* $http.get('api/user', {params: {id: '5'}
		}).success(function(data, status, headers, config) {
		// Do something successful.
		}).error(function(data, status, headers, config) {
		// Handle the error
		}); */
	}

	function HelloController($scope) {
		$scope.greeting = {
			text : 'Hello'
		};
	}

	function CartController($scope) {
		$scope.items = [ {
			title : 'Paint pots',
			quantity : 8,
			price : 3.95
		}, {
			title : 'Polka dots',
			quantity : 17,
			price : 12.95
		}, {
			title : 'Pebbles',
			quantity : 5,
			price : 6.95
		} ];
		$scope.remove = function(index) {
			$scope.items.splice(index, 1);
		}
	}
</script>
</head>
<body>
	<div ng-controller="Hello">
		<h2>Spring MVC + AngularJS Demo</h2>
		<p>EMail Id : {{user.emailId}}</p>
		<p>User Name : {{user.userName}}</p>
	</div>
	<div ng-controller='CartController'>
		<h1>Your Order</h1>
		<div ng-repeat='item in items'>
			<span>{{item.title}}</span> <input ng-model='item.quantity'>
			<span>{{item.price | currency}}</span> <span>{{item.price *
				item.quantity | currency}}</span>
			<button ng-click="remove($index)">Remove</button>
		</div>
	</div>


	<div ng-controller='HelloController'>
		<input ng-model='greeting.text'>
		<p>{{greeting.text}}, World</p>
	</div>

	<form ng-controller="StartUpControllera">
		<input type="checkbox" ng-model="youCheckedIt">
	</form>


	<form ng-controller="StartUpController" ng-submit="requestFunding()">
		Starting: <input ng-change="computeNeeded()"
			ng-model="startingEstimate"> Recommendation: {{needed}}
		<button>Fund my startup!</button>
		<button ng-click="reset()">Reset</button>
	</form>
	<script type="text/javascript">
		function StartUpController($scope) {
			$scope.computeNeeded = function() {
				$scope.needed = $scope.startingEstimate * 10;
			};
			$scope.requestFunding = function() {
				window.alert("Sorry, please get more customers first.");
			};
		}

		function StartUpControllera($scope) {

		}
		var album = [ {
			name : 'Southwest Serenade',
			duration : '2:34'
		}, {
			name : 'Northern Light Waltz',
			duration : '3:21'
		}, {
			name : 'Eastern Tango',
			duration : '17:45'
		} ];
		function AlbumController($scope) {
			$scope.album = album;
		}

		var students = [ {
			name : 'Mary Contrary',
			id : '1'
		}, {
			name : 'Jack Sprat',
			id : '2'
		}, {
			name : 'Jill Hill',
			id : '3'
		} ];
		function StudentListController($scope) {
			$scope.students = students;
			/*  $scope.insertTom = function () {
			// window.alert("Sorry, please get more customers first.");
			 $scope.students.splice(1, 0, {name:'Tom Thumb', id:'4'});
			 }; */

			$scope.insertTom = function() {
				$scope.students.splice(1, 0, {
					name : 'Tom Thumb',
					id : '4'
				});
			};
		}
	</script>

	<table ng-controller='AlbumController'>
		<tr ng-repeat='track in album'>
			<td>{{$index + 1}}</td>
			<td>{{track.name}}</td>
			<td>{{track.duration}}</td>
		</tr>
	</table>

	<!-- <ul ng-controller='StudentListController'>
		<li ng-repeat='student in students'><a
			href='/pcm/view/{{student.id}}'>{{student.name}}</a></li>
	</ul> -->

	<ul ng-controller='StudentListController'>
		<li ng-repeat='student in students'><a	href='/student/view/{{student.id}}'>{{student.name}}</a>
		<a	href='./api/logs'>Login</a>
		</li>
	</ul>
	<button ng-click="insertTom()">Insert</button>


	<div ng-controller='DeathrayMenuController'>
		<button ng-click='toggleMenu()'>Toggle Menu</button>
		<ul ng-show='menuState'>
			<li ng-click='stun()'>Stun</li>
			<li ng-click='disintegrate()'>Disintegrate</li>
			<li ng-click='erase()'>Erase from history</li>
		</ul>
		<div />

		<script type="text/javascript">
			function DeathrayMenuController($scope) {
				$scope.menuState = true;
				$scope.toggleMenu = function() {
					$scope.menuState = !$scope.menuState;
				};
				// death ray functions left as exercise to reader
			}

			function HeaderController($scope) {
				$scope.isError = false;
				$scope.isWarning = false;
				$scope.showError = function() {
					$scope.messageText = 'This is an error!';
					$scope.isError = true;
					$scope.isWarning = false;
				};
				$scope.showWarning = function() {
					$scope.messageText = 'Just a warning. Please carry on.';
					$scope.isWarning = true;
					$scope.isError = false;
				};
			}

			function RestaurantTableController($scope) {
				$scope.directory = [ {
					name : 'The Handsome Heifer',
					cuisine : 'BBQ'
				}, {
					name : 'Greens Green Greens',
					cuisine : 'Salads'
				}, {
					name : 'House of Fine Fish',
					cuisine : 'Seafood'
				} ];
				$scope.selectRestaurant = function(row) {
					$scope.selectedRow = row;
				};
			}

			function AddUserController($scope) {
				$scope.message = '';
				$scope.addUser = function() {
					// TODO for the reader: actually save user to database...
					$scope.message = 'Thanks, ' + $scope.user.first
							+ ', we added you!';
				};
			}
		</script>



		<div ng-controller='HeaderController'>
			<div ng-class='{error: isError, warning: isWarning}'>{{messageText}}</div>
			<button ng-click='showError()'>Simulate Error</button>
			<button ng-click='showWarning()'>Simulate Warning</button>
		</div>


		<table ng-controller='RestaurantTableController'>
			<tr ng-repeat='restaurant in directory'
				ng-click='selectRestaurant($index)'
				ng-class='{selected: $index==selectedRow}'>
				<td>{{restaurant.name}}</td>
				<td>{{restaurant.cuisine}}</td>
			</tr>
		</table>


		<h1>Sign Up</h1>
		<form name='addUserForm'>
			<div>
				First name: <input ng-model='user.first' required>
			</div>
			<div>
				Last name: <input ng-model='user.last' required>
			</div>
			<div>
				Email: <input type='email' ng-model='user.email' required>
			</div>
			<div>
				Age: <input type='number' ng-model='user.age' ng-maxlength='3' ng-minlength='1'>
			</div>
			<div>
				<button>Submit</button>
			</div>
		</form>
		
<tabset>
<tab title='Home'>
<p>Welcome home!</p>
</tab>
<tab title='Preferences'>
<!-- preferences UI goes here -->
</tab>
</tabset>






<script src="/pcm/resources/js/angular.min.js"></script>
<script type="text/javascript">
	 function FormSubmitControllerd($scope, $http) {			
		$scope.list = [];
			$scope.headerText = 'AngularJS Post Form Spring MVC example: Submit below form';
			$scope.submit = function() {
		    var formData = {
						"name" : $scope.name,
						"location" : $scope.location,
						"phone" : $scope.phone	
				};
				
				var response = $http.post('PostFormData', formData);
				response.success(function(data, status, headers, config) {
					$scope.list.push(data);
				});
				response.error(function(data, status, headers, config) {
					alert( "Exception details: " + JSON.stringify({data: data}));
				});
				
				//Empty list data after process
				$scope.list = [];
				
			};
		}

	 function Hello($scope, $http) {
 $http.get('/pcm/springcontent.json').success(function(data) {
				$scope.user = data;
			});

			/* $http.get('api/user', {params: {id: '5'}
			}).success(function(data, status, headers, config) {
			// Do something successful.
			}).error(function(data, status, headers, config) {
			// Handle the error
			}); */
		}

	 function Hello1($scope, $http) {
		 $scope.submit = function() {
			    var formData = {
							"name" : $scope.name,
							"location" : $scope.location,
							"phone" : $scope.phone	
					};
					
					var response = $http.post('/pcm/getUser1.json', formData);
					response.success(function(data, status, headers, config) {
						$scope.list.push(data);
						$scope.u=data;
					});
					response.error(function(data, status, headers, config) {
						alert( "Exception details: " + JSON.stringify({data: data}));
					});
					
					 	$scope.list = [];
					
				};
			  
		}
</script>
</head>
<body>
<div class="container">  
	<div class="row">     
    	<div class="col-lg-12 col-md-12 col-sm-12">   	
    	<fieldset class="fieldset2 borderManager table-responsive">  
    	<legend class="borderManager"> <span style="color: green;"></span></legend>
					<form  action="clusterRegister" method="post" class="form-inline" onsubmit="return validateForm();">				    
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div style="margin-bottom:6px;">Hub<span style="color: red;">*</span></div>

</body>
</html>


